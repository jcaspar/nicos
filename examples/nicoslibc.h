#ifndef NICOSLIBC
#define NICOSLIBC

#include <stdint.h>

#define SYS_READ 0
#define SYS_WRITE 1
#define SYS_OPEN 2
#define SYS_CLOSE 3
#define SYS_STAT 4
#define SYS_FSTAT 5
#define SYS_LSTAT 6
#define SYS_POLL 7
#define SYS_LSEEK 8
#define SYS_MMAP 9
#define SYS_MPROTECT 10
#define SYS_MUNMAP 11
#define SYS_BRK 12
#define SYS_IOCTL 16
#define SYS_GETPID 39
#define SYS_FORK 57
#define SYS_EXECVE 59
#define SYS_EXIT 60
#define SYS_KILL 62
#define SYS_GETPPID 110
#define SYS_DEBUG 250
#define SYS_HINT_DO_NOTHING 251
#define SYS_HALT 252
#define SYS_FLUSH_VFS 253

#define STDOUT 1

// The user must define a `main` function, that does *not* have access to command line arguments,
// because such a thing does not exist (yet) in NicOS.
int main(void);

/**************
 ** Syscalls **
 **************/

int read(int fd, char *buffer, int size) {
  int retcode;

  asm("mov %1, %%rax\n\t"
      "mov %2, %%rdi\n\t"
      "mov %3, %%rsi\n\t"
      "mov %4, %%rdx\n\t"
      "syscall\n\t"
      "mov %%eax, %0"
      : "=r" (retcode)
      : "r" ((uint64_t) SYS_READ),
	"r" ((uint64_t) fd),
	"r" ((uint64_t) buffer),
	"r" ((uint64_t) size)
      : "%rax", "%rdi", "%rsi", "%rdx");

  return retcode;
}

int write(char *message, int size, int fd) {
  int retcode;

  asm("mov %1, %%rax\n\t"
      "mov %2, %%rdi\n\t"
      "mov %3, %%rsi\n\t"
      "mov %4, %%rdx\n\t"
      "syscall\n\t"
      "mov %%eax, %0"
      : "=r" (retcode)
      : "r" ((uint64_t) SYS_WRITE),
	"r" ((uint64_t) fd),
	"r" ((uint64_t) message),
	"r" ((uint64_t) size)
      : "%rax", "%rdi", "%rsi", "%rdx");
  
  return retcode;
}

int open(char *path) {
  int retcode;

  asm("mov %1, %%rax\n\t"
      "mov %2, %%rdi\n\t"
      "mov 0, %%rsi\n\t"
      "mov 0, %%rdx\n\t"
      "syscall\n\t"
      : "=r" (retcode)
      : "r" ((uint64_t) SYS_OPEN),
	"r" ((uint64_t) path)
      : "%rax", "%rdi", "%rsi", "%rdx");

  return retcode;
}

void *brk(int incr) {
  uint64_t retcode;

  asm("mov %1, %%rax\n\t"
      "mov %2, %%rdi\n\t"
      "syscall\n\t"
      : "=r" (retcode)
      : "r" ((uint64_t) SYS_BRK),
	"r" ((uint64_t) incr)
      : "%rax", "%rdi");
  
  return (void*) retcode;
}

int getpid(void) {
  int retcode;

  asm("mov %1, %%rax\n\t"
      "syscall\n\t"
      : "=r" (retcode)
      : "r" ((uint64_t) SYS_GETPID)
      : "%rax");

  return retcode;
}

int fork() {
  int retcode;

  asm("mov %1, %%rax\n\t"
      "syscall\n\t"
      : "=r" (retcode)
      : "r" ((uint64_t) SYS_FORK)
      : "%rax");

  return retcode;
}

int execve(char *path, char **argv, char **envp) {
  int retcode;

  asm("mov %1, %%rax\n\t"
      "mov %2, %%rdi\n\t"
      "mov %3, %%rsi\n\t"
      "mov %4, %%rdx\n\t"
      "syscall\n\t"
      "mov %%eax, %0"
      : "=r" (retcode)
      : "r" ((uint64_t) SYS_EXECVE),
	"r" ((uint64_t) path),
	"r" ((uint64_t) argv),
	"r" ((uint64_t) envp)
      : "%rax", "%rdi", "%rsi", "%rdx");
  
  return retcode;
}

_Noreturn void exit(int code) {
  while(1) {
    asm("mov %0, %%rax\n\t"
	"mov %1, %%rdi\n\t"
	"syscall\n\t"
	:
	: "r" ((uint64_t) SYS_EXIT),
	  "r" ((uint64_t) code)
	: "%rax", "%rdi");
  }
}

int getppid(void) {
  int retcode;

  asm("mov %1, %%rax\n\t"
      "syscall\n\t"
      : "=r" (retcode)
      : "r" ((uint64_t) SYS_GETPPID)
      : "%rax");

  return retcode;
}

void debug(void) {
  asm("mov %0, %%rax\n\t"
      "syscall\n\t"
      :
      : "r" ((uint64_t) SYS_DEBUG)
      : "%rax");
}

void hint_do_nothing(void) {
  asm("mov %0, %%rax\n\t"
      "syscall\n\t"
      :
      : "r" ((uint64_t) SYS_HINT_DO_NOTHING)
      : "%rax");
}

void flush_vfs(void) {
  asm("mov %0, %%rax\n\t"
      "syscall\n\t"
      :
      : "r" ((uint64_t) SYS_FLUSH_VFS)
      : "%rax");
}

_Noreturn void halt(void) {
  flush_vfs();
  while(1) {
    asm("mov %0, %%rax\n\t"
	"syscall\n\t"
	:
	: "r" ((uint64_t) SYS_HALT)
	: "%rax");
  }
}

/***********
 ** Other **
 ***********/

int print(char *message) {
  char *end = message;
  for (; *end; end++);
  int size = end-message;
  
  return write(message, size, STDOUT);
}

int println(char *message) {
  int retcode;
  if (!(retcode = print(message)))
    return retcode;
  retcode = print("\n");
  return retcode;
}

void _start() {
  int retcode = main();
  exit(retcode);
}

#endif
