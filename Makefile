all:
	@echo "Maybe you meant `make cleandisk`?"
.PHONY: all

lol:
	@mkdir lol

cleandisk: | lol
	dd if=/dev/zero of=harddisk.img bs=1024 count=1024
	mkfs.fat -F 32 harddisk.img
	sudo mount -o loop,umask=000,gid=100,uid=1000 harddisk.img lol
	@echo Populating harddisk...
	@cd lol; \
	 echo hello >thisfile; \
	 mkdir subdir; \
	 cd subdir; \
	 echo good bye >file.you
	sudo umount lol
.PHONY: cleandisk

mount:
	sudo mount -o loop,umask=000,gid=100,uid=1000 harddisk.img lol
.PHONY: mount

umount:
	sudo umount lol
.PHONY: umount

ex1: | lol
	dd if=/dev/zero of=harddisk.img bs=1024 count=1024
	mkfs.fat -F 32 harddisk.img
	sudo mount -o loop,umask=000,gid=100,uid=1000 harddisk.img lol
	@echo Populating harddisk...
	@cd examples/ex1; \
		nasm -f elf64 INIT.S -o INIT.O; \
		ld INIT.O -o INIT
	mkdir lol/bin
	cp examples/ex1/INIT lol/bin/init
	cp examples/ex1/to_read lol/to_read
	sudo umount lol
.PHONY: ex1

ex2: | lol
	dd if=/dev/zero of=harddisk.img bs=1024 count=1024
	mkfs.fat -F 32 harddisk.img
	sudo mount -o loop,umask=000,gid=100,uid=1000 harddisk.img lol
	@echo Populating harddisk...
	@cd examples/ex2; \
		nasm -f elf64 INIT.S -o INIT.O; \
		ld INIT.O -o INIT
	mkdir lol/bin
	cp examples/ex2/INIT lol/bin/init
	sudo umount lol
.PHONY: ex2

examples/ex%/init: examples/ex%/init.c examples/nicoslibc.h
	gcc -Wall -nostdlib -I examples/ -o $@ $<

ex3: examples/ex3/init | lol
	dd if=/dev/zero of=harddisk.img bs=1024 count=1024
	mkfs.fat -F 32 harddisk.img
	sudo mount -o loop,umask=000,gid=100,uid=1000 harddisk.img lol
	@echo Populating harddisk...
	mkdir lol/bin
	cp examples/ex3/init lol/bin/
	sudo umount lol
.PHONY: ex3
