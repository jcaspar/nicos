#![feature(custom_test_frameworks)]
#![test_runner(crate::runner)]

use ovmf_prebuilt::ovmf_pure_efi;
use std::process::Command;

// Use this option to choose between UEFI and BIOS boot
const USE_UEFI: bool = false;

fn main() {
    let uefi_path = env!("UEFI_PATH");
    let bios_path = env!("BIOS_PATH");

    let mut cmd = Command::new("qemu-system-x86_64");
    let path = if USE_UEFI {
        cmd.arg("-bios").arg(ovmf_pure_efi());
        uefi_path
    } else {
        bios_path
    };

    cmd.arg("-drive").arg(format!(
        "id=bootdisk,media=disk,format=raw,file={path},index=0"
    ));

    cmd.arg("-serial").arg("stdio");

    cmd.arg("-drive")
        .arg("id=disk,media=disk,format=raw,file=./harddisk.img,index=1");

    // GDB setup
    if let Some(1) = std::env::var("GDB")
        .ok()
        .and_then(|s| s.parse::<u32>().ok())
    {
        cmd.arg("-s").arg("-S");
    }
    // log interrupt and exception
    if let Some(1) = std::env::var("LOG")
        .ok()
        .and_then(|s| s.parse::<u32>().ok())
    {
        cmd.arg("-d").arg("int");
    }
    println!("running {cmd:?}");
    let mut child = cmd.spawn().unwrap();
    child.wait().unwrap();
}

#[cfg(test)]
fn runner(_: &[&dyn Fn()]) {
    let uefi_path = env!("TEST_UEFI_PATH");
    let bios_path = env!("TEST_BIOS_PATH");

    let mut cmd = Command::new("qemu-system-x86_64");
    let path = if USE_UEFI {
        cmd.arg("-bios").arg(ovmf_pure_efi());
        uefi_path
    } else {
        bios_path
    };
    cmd.arg("-drive").arg(format!("format=raw,file={path}"));
    // GDB setup
    if let Some(1) = std::env::var("GDB")
        .ok()
        .and_then(|s| s.parse::<u32>().ok())
    {
        cmd.arg("-s").arg("-S");
    }
    // log interrupt and exception
    // cmd.arg("-d").arg("int");
    println!("running {cmd:?}");
    let mut child = cmd.spawn().unwrap();
    child.wait().unwrap();
}
