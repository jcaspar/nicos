{
  description = "nicos";
  inputs = {
    nixpkgs.url = github:NixOS/nixpkgs/nixpkgs-unstable;
    utils.url = github:numtide/flake-utils;
    rust-overlay = {
      url = github:oxalica/rust-overlay;
      inputs = {
        nixpkgs.follows = "nixpkgs";
        flake-utils.follows = "utils";
      };
    };
    naersk.url = github:nix-community/naersk;
  };

  outputs = { self, nixpkgs, utils, naersk, rust-overlay }:
    utils.lib.eachDefaultSystem (system:
      let
        pkgs = import nixpkgs {
          inherit system;
          overlays = [
            rust-overlay.overlays.default
          ];
        };
        rust = pkgs.rust-bin.nightly.latest.default.override {
          extensions = [ "rust-src" "clippy" "llvm-tools-preview" ];
          targets = [ "x86_64-unknown-linux-gnu" "x86_64-unknown-none" ];
        };
        naerskLib = naersk.lib.${system}.override {
          cargo = rust;
          rustc = rust;
        };
        build_deps = with pkgs; [
          cargo-bootimage
          rust
          gcc
          llvm
        ];
      in
      {
        defaultPackage = naerskLib.buildPackage {
          pname = "nicos";
          root = ./.;
          buildInputs = build_deps;
        };
        defaultApp = utils.lib.mkApp {
          drv = self.defaultPackage.${system};
        };
        devShells = with pkgs; {
          default = mkShell {
            packages = [
              cargo
              cargo-edit
              rustfmt
              rustPackages.clippy
              rust-analyzer
              gdb
              binutils
              nasm
              qemu
            ] ++ build_deps;
          };

          cross = mkShell {
            packages = [
              gcc
              binutils
              nasm
              musl
            ];
          };
        };
      });
}
