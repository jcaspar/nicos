#![no_std]
#![no_main]

use bootloader_api::{entry_point, BootInfo};
use kernel::{hardware, primitives::halt, println, vga};

use core::assert;

entry_point!(main);
fn main(boot_info: &'static mut BootInfo) -> ! {
    assert!(boot_info.framebuffer.as_ref().unwrap().info().height >= 16);
    unsafe {
        vga::init(boot_info.framebuffer.as_mut().unwrap());
    }

    hardware::init();
    println!("Hello, tests!");
    halt();
}
