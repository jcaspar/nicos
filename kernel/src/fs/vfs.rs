use core::fmt::{self, Display, Formatter};

use alloc::{sync::Arc, vec::Vec};
use bitflags::bitflags;
use crossbeam_queue::SegQueue;
use futures_util::task::AtomicWaker;
use lazy_static::lazy_static;
use spin::Mutex;

use crate::{
    error::{err, Result},
    hardware::{
        DecodedKey, EventModifiers, HardDiskDriver, KeyEvent, Peekable, Readable, Skippable,
        Writable,
    },
    parallelism::scheduler::{register, unregister, yield_now},
    prelude::*,
};

use super::{
    fat::{CachedFileSystem, Node, OpenFileCursor},
    raw_fat::Name,
    FsError,
};

lazy_static! {
    pub static ref KEYCODES: SegQueue<KeyEvent> = SegQueue::default();
}

static CONSOLE_WAKER: AtomicWaker = AtomicWaker::new();

bitflags! {
    pub struct Permissions: u8 {
    const WRITE = 0b01;
    const READ = 0b010;
    }
}

pub struct Vfs {
    raw_fs: Arc<Mutex<CachedFileSystem<HardDiskDriver>>>,
}

impl Vfs {
    pub fn new(drive: u8) -> Result<Self> {
        let mut disk = HardDiskDriver::new(drive);
        // SAFETY: We have already initialized the IDE because... yes?
        unsafe {
            disk.init();
        }
        let fs = CachedFileSystem::new(disk)?;
        Ok(Self {
            raw_fs: Arc::new(Mutex::new(fs)),
        })
    }

    pub fn open(&self, path: Path, permissions: Permissions) -> Result<FileDescriptor> {
        if path == Path::new(b"/dev/console")? {
            return Ok(FileDescriptor::Console(Console));
        }
        let fs = self.raw_fs.lock();
        let mut current_node = fs.root();
        'unroll: for segment in path.path.iter() {
            match fs.get_node(current_node)? {
                Node::Directory { children, .. } => {
                    for (id, name) in children {
                        if name == segment {
                            current_node = *id;
                            continue 'unroll;
                        }
                    }
                    return err!(FsError::FileNotFound { path: path.clone() });
                }
                Node::File { metadata, .. } => {
                    return err!(FsError::IsAFile {
                        id: current_node,
                        name: metadata.name().clone()
                    })
                }
            }
        }
        let cursor = fs.open(current_node)?;
        drop(fs);
        Ok(FileDescriptor::RealFile(OpenFileHandle {
            permissions,
            cursor,
            raw_fs: self.raw_fs.clone(),
            path,
        }))
    }

    pub fn flush(&self) -> Result<()> {
        self.raw_fs.lock().flush()
    }
}

#[derive(Debug, Clone)]
pub enum FileDescriptor {
    RealFile(OpenFileHandle),
    Console(Console),
}

impl Drop for FileDescriptor {
    fn drop(&mut self) {
        self.flush().unwrap();
    }
}

impl Skippable for FileDescriptor {
    fn skip_forward(&mut self, offset: usize) {
        match self {
            Self::RealFile(ofh) => ofh.skip_forward(offset),
            Self::Console(console) => console.skip_forward(offset),
        }
    }
}

impl Peekable for FileDescriptor {
    fn skip_backward(&mut self, offset: usize) {
        match self {
            Self::RealFile(ofh) => ofh.skip_backward(offset),
            Self::Console(console) => console.skip_backward(offset),
        }
    }

    fn go_to(&mut self, head: usize) {
        match self {
            Self::RealFile(ofh) => ofh.go_to(head),
            Self::Console(console) => console.go_to(head),
        }
    }
}

impl Readable for FileDescriptor {
    fn read(&mut self) -> Result<Option<u8>> {
        match self {
            Self::RealFile(ofh) => ofh.read(),
            Self::Console(console) => console.read(),
        }
    }

    fn read_to_end(&mut self, buffer: &mut Vec<u8>) -> Result<()> {
        match self {
            Self::RealFile(ofh) => ofh.read_to_end(buffer),
            Self::Console(console) => console.read_to_end(buffer),
        }
    }
}

impl Writable for FileDescriptor {
    fn write(&mut self, byte: u8) -> Result<()> {
        match self {
            Self::RealFile(ofh) => ofh.write(byte),
            Self::Console(console) => console.write(byte),
        }
    }

    fn flush(&mut self) -> Result<()> {
        match self {
            Self::RealFile(ofh) => ofh.flush(),
            Self::Console(console) => console.flush(),
        }
    }

    fn write_all(&mut self, bytes: &[u8]) -> Result<()> {
        match self {
            Self::Console(console) => console.write_all(bytes),
            Self::RealFile(ofh) => ofh.write_all(bytes),
        }
    }
}

#[derive(Debug, Clone)]
pub struct Console;

impl Skippable for Console {
    fn skip_forward(&mut self, _: usize) {}
}

impl Peekable for Console {
    fn go_to(&mut self, _: usize) {}
    fn skip_backward(&mut self, _: usize) {}
}

impl Readable for Console {
    fn read(&mut self) -> Result<Option<u8>> {
        if let Some(KeyEvent {
            key: DecodedKey::Decoded(chr),
            is_pressed: true,
            modifiers:
                EventModifiers {
                    control: false,
                    alt: false,
                    ..
                },
        }) = KEYCODES.pop()
        {
            Ok(Some(chr))
        } else {
            Ok(None)
        }
    }

    fn read_to_end(&mut self, buffer: &mut Vec<u8>) -> Result<()> {
        loop {
            if let Some(KeyEvent {
                key: DecodedKey::Decoded(chr),
                is_pressed: true,
                modifiers:
                    EventModifiers {
                        control: false,
                        alt: false,
                        ..
                    },
            }) = KEYCODES.pop()
            {
                buffer.push(chr);
                if chr == '\n' as u8 {
                    return Ok(());
                }
            }

            register(&CONSOLE_WAKER)?;

            if let Some(KeyEvent {
                key: DecodedKey::Decoded(chr),
                is_pressed: true,
                modifiers:
                    EventModifiers {
                        control: false,
                        alt: false,
                        ..
                    },
            }) = KEYCODES.pop()
            {
                unregister(&CONSOLE_WAKER);

                buffer.push(chr);
                if chr == '\n' as u8 {
                    CONSOLE_WAKER.take();
                    return Ok(());
                }
            } else {
                yield_now()
            }
        }
    }
}

impl Writable for Console {
    fn write(&mut self, byte: u8) -> Result<()> {
        print!("{}", byte as char);
        Ok(())
    }

    fn flush(&mut self) -> Result<()> {
        Ok(())
    }
}

#[derive(Debug, Clone)]
pub struct OpenFileHandle {
    permissions: Permissions,
    raw_fs: Arc<Mutex<CachedFileSystem<HardDiskDriver>>>,
    cursor: OpenFileCursor,
    path: Path,
}

impl Skippable for OpenFileHandle {
    fn skip_forward(&mut self, offset: usize) {
        self.cursor.skip_forward(offset, &*self.raw_fs.lock())
    }
}

impl Peekable for OpenFileHandle {
    fn go_to(&mut self, head: usize) {
        self.cursor.go_to(head, &mut *self.raw_fs.lock())
    }

    fn skip_backward(&mut self, offset: usize) {
        self.cursor
            .skip_backward(offset, self.raw_fs.lock().boot_record())
    }
}

impl Readable for OpenFileHandle {
    fn read(&mut self) -> Result<Option<u8>> {
        if !self.permissions.contains(Permissions::READ) {
            return err!(FsError::PermissionError {
                path: self.path.clone(),
                permission: "read",
            });
        }

        self.cursor.read(&mut *self.raw_fs.lock())
    }

    fn read_to_end(&mut self, buffer: &mut Vec<u8>) -> Result<()> {
        if !self.permissions.contains(Permissions::READ) {
            return err!(FsError::PermissionError {
                path: self.path.clone(),
                permission: "read",
            });
        }

        let mut fs = self.raw_fs.lock();
        while let Some(byte) = self.cursor.read(&mut *fs)? {
            buffer.push(byte);
        }

        Ok(())
    }
}

impl Writable for OpenFileHandle {
    fn write(&mut self, byte: u8) -> Result<()> {
        if !self.permissions.contains(Permissions::WRITE) {
            return err!(FsError::PermissionError {
                path: self.path.clone(),
                permission: "write",
            });
        }

        self.cursor.write(byte, &mut *self.raw_fs.lock())
    }

    fn write_all(&mut self, bytes: &[u8]) -> Result<()> {
        if !self.permissions.contains(Permissions::WRITE) {
            return err!(FsError::PermissionError {
                path: self.path.clone(),
                permission: "write",
            });
        }

        let mut fs = self.raw_fs.lock();
        for byte in bytes {
            self.cursor.write(*byte, &mut *fs)?;
        }
        Ok(())
    }

    fn flush(&mut self) -> Result<()> {
        self.cursor.flush(&mut *self.raw_fs.lock())
    }
}

#[derive(Debug, Clone, Eq, PartialEq)]
pub struct Path {
    path: Vec<Name>,
}

impl Path {
    /// Beware, relative paths are not supported yet.
    pub fn new(path: &[u8]) -> Result<Self> {
        if path.is_empty() {
            return err!(FsError::EmptyPath);
        }

        if path[0] != '/' as u8 {
            return err!(FsError::RelativePath {
                path: AsciiString::from(path),
            });
        }

        if path.len() == 1 {
            return Ok(Path { path: Vec::new() });
        }

        let end = if *path.last().unwrap() == '/' as u8 {
            path.len() - 1
        } else {
            path.len()
        };

        let path = path[1..end]
            .split(|c| *c == '/' as u8)
            .map(|segment| Name::new(segment))
            .collect::<Result<Vec<_>>>()?;
        Ok(Path { path })
    }
}

impl Display for Path {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        write!(f, "/")?;
        match &self.path[..] {
            [] => {}
            [first, others @ ..] => {
                write!(f, "{first}")?;
                for other in others {
                    write!(f, "/{other}")?;
                }
            }
        }
        Ok(())
    }
}
