use core::fmt::{self, Debug, Display, Formatter, Write};

use alloc::string::String;
use bitflags::bitflags;

use super::FsError;
use crate::{
    error::{err, Result},
    hardware::{Readable, Writable},
    prelude::*,
    serde::{Deserialize, Serialize},
    string::to_lower,
};

/// Flag to indicate a free cluster. See the documentation of [`ClusterKind`].
const FREE_CLUSTER_FLAG: u32 = 0x00000000;

/// Flag to indicate a reserved cluster. See the documentation of [`ClusterKind`].
const RESERVED_CLUSTER_FLAG: u32 = 0x00000001;

/// Start of range of used cluster with next cluster in chain. See the documentation of
/// [`ClusterKind`].
const START_USED_CLUSTER_FLAG: u32 = 0x00000002;

/// End of range of used cluster with next cluster in chain. See the documentation of
/// [`ClusterKind`].
const END_USED_CLUSTER_FLAG: u32 = 0x0FFFFFF6;

/// Flag to indicate a corrupted cluster. See the documentation of [`ClusterKind`].
const BAD_CLUSTER_FLAG: u32 = 0x0FFFFFF7;

/// Start of range of used cluster, end of cluster chain. See the documentation of
/// [`ClusterKind`].
const START_EOC_CLUSTER_FLAG: u32 = 0x0FFFFFF8;

/// End of range of used cluster, end of cluster chain. See the documentation of [`ClusterKind`].
const END_EOC_CLUSTER_FLAG: u32 = 0x0FFFFFFF;

#[derive(Debug)]
pub enum ClusterKind {
    /// This cluster is available for use.
    Free,
    /// This cluster cannot be used.
    Reserved,
    /// This cluster is currently used. The value correspond to next cluster of the chain, or
    /// `None` if this cluster is the end of the chain.
    Used(Option<u32>),
    /// Some external tool detected, at some point, that a sector in this cluster is unusable
    /// for some reasons, and therefore this cluster should be avoided (neither written to nor
    /// read from).
    /// Most notably, a bad cluster is tagged as if it was a used cluster with a very big next cluster in the
    /// chain, and therefore:
    ///  * if too many clusters are needed, one of them will be considered corrupted;
    ///  * this confuses certain utilities
    BadCluster,
}

impl ClusterKind {
    /// The size of a serialized `ClusterKind`, in bytes.
    pub const SIZE: usize = 4;

    pub fn is_used(&self) -> bool {
        matches!(self, Self::Used(_))
    }
}

impl Deserialize for ClusterKind {
    fn deserialize(reader: &mut impl Readable) -> Result<Self> {
        Ok(reader.parse::<u32>()?.into())
    }
}

impl Serialize for ClusterKind {
    fn serialize(&self, writer: &mut impl Writable) -> Result<()> {
        match self {
            Self::Free => writer.encode(&FREE_CLUSTER_FLAG),
            Self::Reserved => writer.encode(&RESERVED_CLUSTER_FLAG),
            Self::Used(Some(next)) => writer.encode(next),
            Self::Used(None) => writer.encode(&START_EOC_CLUSTER_FLAG),
            Self::BadCluster => writer.encode(&BAD_CLUSTER_FLAG),
        }
    }
}

impl From<u32> for ClusterKind {
    fn from(value: u32) -> Self {
        // The 4 high bits are reserved, and should be ignored.
        match value & 0x0FFFFFFF {
            FREE_CLUSTER_FLAG => Self::Free,
            RESERVED_CLUSTER_FLAG => Self::Reserved,
            START_USED_CLUSTER_FLAG..=END_USED_CLUSTER_FLAG => Self::Used(Some(value)),
            BAD_CLUSTER_FLAG => Self::BadCluster,
            START_EOC_CLUSTER_FLAG..=END_EOC_CLUSTER_FLAG => Self::Used(None),
            _ => unreachable!(),
        }
    }
}

impl From<ClusterKind> for u32 {
    fn from(ck: ClusterKind) -> Self {
        match ck {
            ClusterKind::Free => FREE_CLUSTER_FLAG,
            ClusterKind::Reserved => RESERVED_CLUSTER_FLAG,
            ClusterKind::Used(Some(next)) => next,
            ClusterKind::Used(None) => START_EOC_CLUSTER_FLAG,
            ClusterKind::BadCluster => BAD_CLUSTER_FLAG,
        }
    }
}

/// # Synopsis
///
/// The Bios Parameter Block (BPB) describes the way the data is actually layed out in the hard disk.
///
/// This is the filesystem metadata structure. It will never be modified, so it does not implement
/// [`Serialize`].
///
/// # References
///
/// Wiki OSdev <https://wiki.osdev.org/FAT#Implementation_Details>
/// A ~nice~ useless tutorial <https://www.sobyte.net/post/2022-01/rust-fat32/>
/// Wikipedia <https://en.wikipedia.org/wiki/BIOS_parameter_block>
#[allow(dead_code)]
#[derive(Debug, Clone, Copy)]
pub struct BiosParameterBlock {
    pub(crate) bytes_per_sector: u16,
    pub(crate) sectors_per_cluster: u8,
    /// Number of reserved sectors in the reserved area starting from the first sector of the volume.
    /// This is usually 32.
    pub(crate) reserved_sectors: u16,
    /// Sometimes, people include the FAT twice for rendudency, so this number will usually be either 1 or 2.
    /// However, all the FATs beyond the first one are useless for our purpose (we do not check
    /// redundency).
    pub(crate) number_of_fats: u8,
    /// This is only useful to FAT12 and FAT16, it should be 0 on FAT32.
    pub(crate) root_entries: u16,
    /// If this is 0, it means there are more than u16::MAX sectors, and so the actual value is in a
    /// Large Sector Count entry, at 0x20 (which correspond to the field [`total_sectors_large`] of this
    /// struct
    pub(crate) total_sectors: u16,
    /// This says what the media actually is (ie. what kind of disk it is). See
    /// <https://en.wikipedia.org/wiki/Design_of_the_FAT_file_system#BPB20_OFS_0Ah>
    pub(crate) media_descriptor_type: u8,
    /// This is a compatibility field that is useless, I think. I'll leave it here just because I might be
    /// wrong, but I don't think you should use it. Yes, I'm talking to you. Don't use this.
    pub(crate) old_sectors_per_fat: u16,
    pub(crate) sectors_per_track: u16,
    /// This is the number of sides on the storage media.
    pub(crate) heads: u16,
    pub(crate) hidden_sectors: u32,
    /// If
    pub(crate) total_sectors_large: u32,
    //* Extended Boot Record *//
    pub(crate) sectors_per_fat: u32,
    pub(crate) flags: u16,
    /// Version of the filesystem. The high byte is the major version number, the low byte is the minor
    /// version.
    ///
    /// The specs says we should "refuse to mount volues with version numbers unknown by [us]".
    pub(crate) version: u16,
    /// "Often this field is set to 2."
    pub(crate) root_dir_first_cluster: u32,
    pub(crate) fs_info_sector: u16,
    pub(crate) backup_boot_sector: u16,
    /// Number of the physical drive, as given by the BIOS. This is useless for us I think.
    pub(crate) drive_number: u8,
    /// A serial number "xxxx-xxxx". A timestamp can be computed for readability.
    pub(crate) volume_id: u32,
    /// The label of this partition volume. Padded with whitespace (`0x20`).
    /// Where there is no volume label, this should be `"No Name"`.
    pub(crate) volume_label: [u8; 11],
    /// Type of filesystem. This is for display purposes only (ie. it's a label, not an ID) and
    /// should be right-padded with whitespace (`0x20`).
    pub(crate) fs_type_label: [u8; 8],
}

impl Deserialize for BiosParameterBlock {
    fn deserialize(reader: &mut impl Readable) -> Result<Self> {
        // The first three bytes correspond to the assembly instruction "jump over the table", so that if
        // someone tries to execute a parameter block, it will skip it...
        reader.skip_forward(3); // 0x00
                                // OEM identifier, it's meaningless.
        reader.skip_forward(8); // 0x03
        let bytes_per_sector = reader.parse()?; // 0x0B
        let sectors_per_cluster = reader.parse()?; // 0x0D
        let reserved_sectors = reader.parse()?; // 0x0E
        let number_of_fats = reader.parse()?; // 0x10
        let root_entries = reader.parse()?; // 0x11
        let total_sectors = reader.parse()?; // 0x13
        let media_descriptor_type = reader.parse()?; // 0x15
        let old_sectors_per_fat = reader.parse()?; // 0x16
        let sectors_per_track = reader.parse()?; // 0x18
        let heads = reader.parse()?; // 0x1A
        let hidden_sectors = reader.parse()?; // 0x1C
        let total_sectors_large = reader.parse()?; // 0x20
        let sectors_per_fat = reader.parse()?; // 0x24
        let flags = reader.parse()?; // 0x28
        let version = reader.parse()?; // 0x2A
        let root_dir_first_cluster = reader.parse()?; // 0x2C
        let fs_info_sector = reader.parse()?; // 0x30
        let backup_boot_sector = reader.parse()?; // 0x32
                                                  // Reserved.
        reader.skip_forward(12); // 0x34
        let drive_number = reader.parse()?; // 0x40
                                            // Reserved (flags for Windows NT).
        reader.skip_forward(1); // 0x41
                                // Signature (must be 0x28 or 0x29).
        reader.skip_forward(1); // 0x42
        let volume_id = reader.parse()?; // 0x43
        let volume_label = reader.parse()?; // 0x47
        let fs_type_label = reader.parse()?; // 0x52
        Ok(Self {
            bytes_per_sector,
            sectors_per_cluster,
            reserved_sectors,
            number_of_fats,
            root_entries,
            total_sectors,
            media_descriptor_type,
            old_sectors_per_fat,
            sectors_per_track,
            heads,
            hidden_sectors,
            total_sectors_large,
            sectors_per_fat,
            flags,
            version,
            root_dir_first_cluster,
            fs_info_sector,
            backup_boot_sector,
            drive_number,
            volume_id,
            volume_label,
            fs_type_label,
        })
    }
}

impl BiosParameterBlock {
    /// Retrieve the offset of the cluster `cluster_nb` from the start of the volume. Note that the
    /// cluster numbers 0 and 1 are reserved, and therefore the first cluster is actually cluster
    /// number 2.
    #[inline]
    pub fn offset(&self, cluster_nb: u32) -> usize {
        if cluster_nb == 0 {
            // Some `..` entries refer to the root cluster as `0` rather than its actuall cluster.
            return self.offset(self.root_dir_first_cluster);
        }

        (self.reserved_sectors as usize
            + self.number_of_fats as usize * self.sectors_per_fat as usize
            + (cluster_nb - 2) as usize * self.sectors_per_cluster as usize)
            * self.bytes_per_sector as usize
    }

    /// Return the size of each cluster, in bytes.
    #[inline]
    pub fn bytes_per_cluster(&self) -> u32 {
        self.bytes_per_sector as u32 * self.sectors_per_cluster as u32
    }

    /// Return the size of each FAT, in bytes.
    ///
    /// This does not take into account the fact that there may be multiple FATs in a single
    /// filesystem.
    #[inline]
    pub fn size_of_fat(&self) -> u32 {
        self.sectors_per_fat * self.bytes_per_sector as u32
    }

    /// Return the byte at which the first FAT starts.
    #[inline]
    pub fn fat_starts_at(&self) -> usize {
        (self.reserved_sectors * self.bytes_per_sector) as _
    }
}

/// I haven't (yet?) understood what the purpose of this thing was. This exists in the specs.
/// It can be serialized. And deserialized. However, according to the specs, the information
/// contained in this struct is not reliable, so if we want to use it, we have to check it first.
///
/// Also, this takes 512B of storage, of which
///  * 492B are reserved;
///  * 12B are magic numbers;
///  * 8B are *actual* information (although I don't know how to use it).
#[allow(dead_code)]
struct FileSystemInfo {
    /// Last known number of free clusters available. If `u32::MAX`, it means no hint available.
    /// This value might be incorrect, and *should* be checked.
    ///
    /// Hence my question: what is the purpose of this thing, if it is not reliable and must be
    /// checked???
    free_clusters: u32,
    /// Clusters before this are likely not free, so when looking for free clusters, we should
    /// start here.
    /// Again (see [`free_clusters`]), this is just a *hint*, and if equal to `u32::MAX`, no
    /// hint is available...
    free_clusters_start_at: u32,
}

impl Deserialize for FileSystemInfo {
    fn deserialize(reader: &mut impl Readable) -> Result<Self> {
        let lead_signature: u32 = reader.parse()?; // 0x000
                                                   // Reserved.
        reader.skip_forward(480); // 0x004
        let another_signature: u32 = reader.parse()?; // 0x1E4
        let free_clusters = reader.parse()?; // 0x1E8
        let free_clusters_start_at = reader.parse()?; // 0x1EC
                                                      // Reserved.
        reader.skip_forward(12); // 0x1F0
        let trail_signature: u32 = reader.parse()?; // 0x1FC

        debug_assert_eq!(lead_signature, Self::LEAD_SIGNATURE);
        debug_assert_eq!(another_signature, Self::ANOTHER_SIGNATURE);
        debug_assert_eq!(trail_signature, Self::TRAIL_SIGNATURE);

        Ok(Self {
            free_clusters,
            free_clusters_start_at,
        })
    }
}

impl FileSystemInfo {
    const LEAD_SIGNATURE: u32 = 0x41615252;
    const ANOTHER_SIGNATURE: u32 = 0x61417272;
    const TRAIL_SIGNATURE: u32 = 0xAA550000;
}

/// We don't handle timestamps further than acknowledging they exist.
#[derive(Debug, Default, Clone)]
#[allow(dead_code)]
pub struct Timestamp {
    deci_sec: u8,
    second: u8,
    minute: u8,
    hour: u8,
}

impl Deserialize for Timestamp {
    fn deserialize(device: &mut impl Readable) -> Result<Self> {
        let time: u16 = device.parse()?;
        let second = (time & 0b0000000000011111) as u8 * 2; // yes, multipled by 2, because Microsoft
        let minute = ((time & 0b0000011111100000) >> 5) as u8;
        let hour = ((time & 0b1111100000000000) >> 11) as u8;
        Ok(Self {
            deci_sec: 0,
            second,
            minute,
            hour,
        })
    }
}

/// We don't handle dates further than acknowledging they exist.
#[derive(Debug, Default, Clone)]
#[allow(dead_code)]
pub struct Date {
    year: u8,
    month: u8,
    day: u8,
}

impl Deserialize for Date {
    fn deserialize(device: &mut impl Readable) -> Result<Self> {
        let date: u16 = device.parse()?;
        let day = (date & 0b0000000000011111) as u8;
        let month = ((date & 0b0000000111100000) >> 5) as u8;
        let year = ((date & 0b1111111000000000) >> 5) as u8;
        Ok(Self { day, month, year })
    }
}

bitflags! {
    pub struct FileFlags: u8 {
    const READ_ONLY = 0x01;
    const HIDDEN = 0x02;
    const SYSTEM = 0x04;
    const VOLUME_ID = 0x08;
    const DIRECTORY = 0x10;
    const ARCHIVE = 0x20;
    const LFN = Self::READ_ONLY.bits() | Self::HIDDEN.bits() | Self::SYSTEM.bits() | Self::VOLUME_ID.bits();
    }
}

impl Deserialize for FileFlags {
    fn deserialize(reader: &mut impl Readable) -> Result<Self> {
        Ok(Self::from_bits_truncate(reader.read()?.unwrap())) // TODO: remove unwrap
    }
}

impl Serialize for FileFlags {
    fn serialize(&self, writer: &mut impl Writable) -> Result<()> {
        Ok(writer.write(self.bits)?)
    }
}

#[derive(Debug, Clone)]
pub struct Metadata {
    pub(super) name: Name,
    pub(super) flags: FileFlags,
    #[allow(dead_code)]
    pub(super) creation_time: Timestamp,
    #[allow(dead_code)]
    pub(super) creation_date: Date,
    #[allow(dead_code)]
    pub(super) access_date: Date,
    pub(super) chain_head: u32,
    #[allow(dead_code)]
    pub(super) modification_date: Date,
    #[allow(dead_code)]
    pub(super) modification_time: Timestamp,
    pub(super) size: u32,
}

impl Metadata {
    /// Size of serialized metadata, in bytes.
    pub const SIZE: usize = 32;

    /// End Of Directory marker. Directories should end with this.
    pub const EOD: [u8; Self::SIZE] = [0; Self::SIZE];

    /// Construct the metadata of the root directory, which is not stored anywhere in the
    /// filesystem.
    pub fn root(chain_head: u32, size: u32) -> Self {
        Self::new(Name::ROOT, FileFlags::DIRECTORY, chain_head, size)
    }

    pub fn new(name: Name, flags: FileFlags, chain_head: u32, size: u32) -> Self {
        Self {
            name,
            flags,
            chain_head,
            size,
            creation_date: Default::default(),
            creation_time: Default::default(),
            access_date: Default::default(),
            modification_date: Default::default(),
            modification_time: Default::default(),
        }
    }

    pub fn name(&self) -> &Name {
        &self.name
    }

    pub fn size(&self) -> u32 {
        self.size
    }
}

impl Deserialize for Metadata {
    fn deserialize(device: &mut impl Readable) -> Result<Self> {
        let name = device.parse()?; // 0x00
        let flags: FileFlags = device.parse()?; // 0x0B
                                                // Reserved for Windows NT...
        device.skip_forward(1); // 0x0C
        device.skip_forward(1); // 0x0D
        let creation_time = device.parse()?; // 0x0E
        let creation_date = device.parse()?; // 0x10
        let access_date = device.parse()?; // 0x12
        let [b3, b4]: [u8; 2] = device.parse()?; // 0x14
        let modification_time = device.parse()?; // 0x16
        let modification_date = device.parse()?; // 0x18
        let [b1, b2]: [u8; 2] = device.parse()?; // 0x1A
        let size = device.parse()?; // 0x1C
        let chain_head = u32::from_le_bytes([b1, b2, b3, b4]);
        Ok(Self {
            name,
            flags,
            creation_date,
            creation_time,
            access_date,
            chain_head,
            modification_date,
            modification_time,
            size,
        })
    }
}

impl Serialize for Metadata {
    fn serialize(&self, writer: &mut impl Writable) -> Result<()> {
        let [b1, b2, b3, b4] = self.chain_head.to_le_bytes();
        self.name.serialize(writer)?;
        self.flags.serialize(writer)?;
        writer.skip_forward(8);
        [b3, b4].serialize(writer)?;
        writer.skip_forward(4);
        [b1, b2].serialize(writer)?;
        self.size.serialize(writer)?;
        Ok(())
    }
}

// Invariant to be upheld: self.0[8] = 46 [character `.`], what comes before is the name (right
// padded with spaces), what comes after is the extension (right padded with spaces). An empty
// extension (ie. an extension entierly made have spaces) is equivalent to no extension at all,
// so `file.`, `file.  ` and `file` will all have the same name, and therefore will be rendered as
// `file`.
#[derive(PartialEq, Eq, PartialOrd, Ord, Clone)]
pub struct Name([u8; 12]);

impl Name {
    pub const ROOT: Self = {
        let mut buffer = [' ' as u32 as u8; 12];
        buffer[8] = '.' as u32 as u8;
        buffer[0] = '/' as u32 as u8;
        Self(buffer)
    };

    pub const DOT: Self = {
        let mut buffer = [' ' as u32 as u8; 12];
        buffer[8] = '.' as u32 as u8;
        buffer[0] = '.' as u32 as u8;
        Self(buffer)
    };

    pub const DOTDOT: Self = {
        let mut buffer = [' ' as u32 as u8; 12];
        buffer[8] = '.' as u32 as u8;
        buffer[0] = '.' as u32 as u8;
        buffer[1] = '.' as u32 as u8;
        Self(buffer)
    };

    pub const fn whole(&self) -> &[u8] {
        &self.0
    }

    pub fn content(&self) -> &[u8] {
        &self.0[..8]
    }

    pub fn extension(&self) -> &[u8] {
        &self.0[9..]
    }

    pub fn new(name: &[u8]) -> Result<Self> {
        let mut buffer = [' ' as u32 as u8; 12];
        buffer[8] = '.' as u32 as u8;
        let mut i = None;
        for j in (0..name.len()).rev() {
            if name[j] == '.' as u32 as u8 {
                i = Some(j);
                break;
            }
        }
        let content = if let Some(ext_i) = i {
            &name[..ext_i]
        } else {
            name
        };
        let extension = if let Some(ext_i) = i {
            Some(&name[ext_i + 1..])
        } else {
            None
        };
        if content.len() > 8 {
            return err!(FsError::FileNameTooLong {
                content: AsciiString::from(content),
            });
        }
        for i in 0..content.len() {
            buffer[i] = to_lower(content[i]);
        }
        if let Some(ext) = extension {
            if ext.len() > 3 {
                return err!(FsError::ExtensionTooLong {
                    extension: AsciiString::from(ext),
                });
            }
            for i in 0..ext.len() {
                buffer[9 + i] = to_lower(ext[i]);
            }
        }
        Ok(Self(buffer))
    }
}

impl Deserialize for Name {
    fn deserialize(reader: &mut impl Readable) -> Result<Self> {
        let mut buffer = [46; 12];
        let name: [_; 8] = reader.parse()?;
        for (i, c) in name.into_iter().enumerate() {
            buffer[i] = to_lower(c);
        }
        let extension: [u8; 3] = reader.parse()?;
        for (i, c) in extension.into_iter().enumerate() {
            buffer[9 + i] = to_lower(c);
        }
        Ok(Self(buffer))
    }
}

impl Serialize for Name {
    fn serialize(&self, writer: &mut impl Writable) -> Result<()> {
        for i in (0..8).chain(9..12) {
            writer.write(self.0[i])?;
        }
        Ok(())
    }
}

impl Debug for Name {
    fn fmt(&self, formatter: &mut Formatter<'_>) -> fmt::Result {
        formatter
            .debug_tuple("Name")
            .field(&&*String::from_utf8_lossy(&self.0))
            .finish()
    }
}

impl Display for Name {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        let mut i = 0;
        while i < 8 && self.0[i] != 32 {
            f.write_char(char::from_u32(self.0[i] as u32).unwrap_or('?'))?;
            i += 1;
        }
        if self.0[9] != 32 {
            f.write_char('.')?;
            let mut i = 9;
            while i < 12 && self.0[i] != 32 {
                f.write_char(char::from_u32(self.0[i] as u32).unwrap_or('?'))?;
                i += 1;
            }
        }
        Ok(())
    }
}
