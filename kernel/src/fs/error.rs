use core::fmt::{self, Display};

use crate::string::AsciiString;

use super::{fat::NodeId, raw_fat::Name, vfs::Path};

#[derive(Debug)]
pub enum Error {
    CannotDeleteRoot,
    InvalidNodeId(NodeId),
    CreateNodeInFile {
        parent_id: NodeId,
        name: Name,
    },
    DirectoryNonNullEnded {
        id: NodeId,
        name: Name,
    },
    CorruptedCluster {
        cluster: u32,
        name: Name,
        id: NodeId,
    },
    InvalidChainCluster {
        cluster: u32,
        name: Name,
        id: NodeId,
    },
    IsAFile {
        id: NodeId,
        name: Name,
    },
    CannotOpenDir {
        id: NodeId,
        name: Name,
    },
    FileNameTooLong {
        content: AsciiString,
    },
    ExtensionTooLong {
        extension: AsciiString,
    },
    EmptyPath,
    RelativePath {
        path: AsciiString,
    },
    FileNotFound {
        path: Path,
    },
    PermissionError {
        path: Path,
        permission: &'static str,
    },
}

impl Display for Error {
    fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Self::CannotDeleteRoot => write!(f, "The root directory cannot be deleted."),
            Self::InvalidNodeId(id) => {
                write!(f, "Invalid node id {id}. Maybe the node has been deleted?")
            }
            Self::CreateNodeInFile { parent_id, name } => write!(
                f,
                "Cannot create node {name} in a file with id {parent_id}."
            ),
            Self::DirectoryNonNullEnded { id, name } => write!(
                f,
                "While reading directory {name} ({id}), found EOC instead of EOD"
            ),
            Self::CorruptedCluster { cluster, name, id } => write!(
                f,
                "Cluster {cluster}, in node {name} ({id}), is corrupted, check your drive."
            ),
            Self::InvalidChainCluster { cluster, name, id } => write!(
                f,
                "Cluster {cluster}, in node {name} ({id}), is either free or reservered."
            ),
            Self::IsAFile { id, name } => write!(f, "Node {name} ({id}) is a file.",),
            Self::CannotOpenDir { id, name } => {
                write!(f, "Cannot open node {name} ({id}), which is a directory.",)
            }
            Self::FileNameTooLong { content } => write!(
                f,
                "The file name `{content}` is too long ({}/8 bytes).",
                content.len(),
            ),
            Self::ExtensionTooLong { extension } => write!(
                f,
                "The extension `{extension}` is too long ({}/3 bytes).",
                extension.len(),
            ),
            Self::EmptyPath => write!(f, "An empty path is invalid. Try using `/` instead.",),
            Self::RelativePath { path } => write!(
                f,
                "The path `{path}` is relative, which is not supported (yet?).",
            ),
            Self::FileNotFound { path } => write!(f, "The path `{path}` does not exist.",),
            Self::PermissionError { path, permission } => {
                write!(
		    f,
		    "The file `{path}` was opened without permission {permission}, and yet a {permission} operation was performed.",
		)
            }
        }
    }
}
