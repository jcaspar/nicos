use core::fmt::{Display, Formatter};

use alloc::{
    collections::{BTreeMap, BTreeSet},
    fmt,
    vec::Vec,
};

use crate::{
    error::{err, error, Result},
    hardware::{Device, Peekable, Readable, Writable},
};

use super::{
    raw_fat::{BiosParameterBlock, ClusterKind, FileFlags, Metadata, Name},
    FsError,
};

#[derive(Debug, PartialEq, Eq, PartialOrd, Ord, Default, Clone, Copy)]
pub struct NodeId(pub u32);

impl NodeId {
    pub fn next(&mut self) -> Self {
        let next = Self(self.0);
        self.0 += 1;
        next
    }

    pub fn new() -> Self {
        Self::default()
    }
}

impl Display for NodeId {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        write!(f, "{}", self.0)
    }
}

#[derive(Debug)]
pub enum Node {
    File {
        parent: NodeId,
        metadata: Metadata,
    },
    Directory {
        parent: Option<NodeId>,
        metadata: Metadata,
        offset_of_child: BTreeMap<NodeId, usize>,
        children: Vec<(NodeId, Name)>,
    },
}

impl Node {
    pub fn name(&self) -> &Name {
        &self.metadata().name
    }

    pub fn metadata(&self) -> &Metadata {
        match self {
            Self::File { metadata, .. } => metadata,
            Self::Directory { metadata, .. } => metadata,
        }
    }

    pub fn metadata_mut(&mut self) -> &mut Metadata {
        match self {
            Self::File { metadata, .. } => metadata,
            Self::Directory { metadata, .. } => metadata,
        }
    }

    pub fn parent(&self) -> Option<NodeId> {
        match self {
            Self::Directory { parent, .. } => parent.clone(),
            Self::File { parent, .. } => Some(parent.clone()),
        }
    }

    pub fn is_file(&self) -> bool {
        matches!(self, Self::File { .. })
    }

    pub fn is_dir(&self) -> bool {
        !self.is_file()
    }
}

type Pool = BTreeMap<NodeId, Node>;

pub struct CachedFileSystem<T> {
    pub fat: Vec<ClusterKind>,
    fat_dirty: bool,
    available_clusters: BTreeSet<u32>,
    boot_record: BiosParameterBlock,
    pool: Pool,
    id: NodeId,
    device: T,
}

impl<T: Device> CachedFileSystem<T> {
    pub fn new(mut device: T) -> Result<Self> {
        let boot_record: BiosParameterBlock = device.reader_at(0).parse()?;
        let (mut fat, mut available_clusters) = {
            let mut reader = device.reader_at(boot_record.fat_starts_at());
            let mut fat = Vec::new();
            let mut ac = BTreeSet::new();
            for address in 0..(boot_record.size_of_fat() / ClusterKind::SIZE as u32) {
                let cluster = reader.parse()?;
                if matches!(cluster, ClusterKind::Free) {
                    ac.insert(address);
                }
                fat.push(cluster);
            }
            (fat, ac)
        };
        let mut pool = Pool::new();
        let mut id = NodeId::new();
        Self::populate(
            &mut pool,
            &mut id,
            &mut device,
            &boot_record,
            &mut fat,
            &mut available_clusters,
        )?;
        Ok(Self {
            pool,
            id,
            boot_record,
            device,
            fat,
            fat_dirty: true, // we might have modified the FAT to purge LFN entries
            available_clusters,
        })
    }

    /// Make sure that everything is properly written to disk. This will flush every cache, at
    /// every underlying level.
    /// Every write operation is subject to caching.
    pub fn flush(&mut self) -> Result<()> {
        if self.fat_dirty {
            self.device
                .writer_at(self.boot_record.fat_starts_at())
                .encode(&*self.fat)?;
        }
        self.device.flush()?;
        Ok(())
    }

    pub fn get_cluster(&self, cluster: u32) -> &ClusterKind {
        &self.fat[cluster as usize]
    }

    pub fn get_cluster_mut(&mut self, cluster: u32) -> &mut ClusterKind {
        &mut self.fat[cluster as usize]
    }

    pub fn root(&self) -> NodeId {
        NodeId(0)
    }

    pub fn get_node(&self, id: NodeId) -> Result<&Node> {
        self.pool.get(&id).ok_or(error!(FsError::InvalidNodeId(id)))
    }

    pub fn get_node_mut(&mut self, id: NodeId) -> Result<&mut Node> {
        self.pool
            .get_mut(&id)
            .ok_or(error!(FsError::InvalidNodeId(id)))
    }

    fn populate(
        pool: &mut Pool,
        id: &mut NodeId,
        device: &mut T,
        boot_record: &BiosParameterBlock,
        fat: &mut [ClusterKind],
        available_clusters: &mut BTreeSet<u32>,
    ) -> Result<()> {
        let root_id = id.next();
        let metadata = Metadata::root(boot_record.root_dir_first_cluster, 0);
        let mut id_of_head = BTreeMap::new();
        id_of_head.insert(boot_record.root_dir_first_cluster, root_id);
        Self::populate_node(
            None,
            root_id,
            metadata,
            pool,
            id,
            device,
            boot_record,
            fat,
            available_clusters,
            &mut id_of_head,
        )?;
        Ok(())
    }

    fn populate_node(
        parent_id: Option<NodeId>,
        node_id: NodeId,
        mut metadata: Metadata,
        pool: &mut Pool,
        id: &mut NodeId,
        device: &mut T,
        boot_record: &BiosParameterBlock,
        fat: &mut [ClusterKind],
        available_clusters: &mut BTreeSet<u32>,
        id_of_head: &mut BTreeMap<u32, NodeId>,
    ) -> Result<bool> {
        if pool.contains_key(&node_id) {
            return Ok(false);
        }
        id_of_head.insert(metadata.chain_head, node_id);

        if metadata.flags.contains(FileFlags::DIRECTORY) {
            let mut current_cluster = metadata.chain_head;
            let mut reader = device.reader_at(boot_record.offset(current_cluster));
            let mut to_process = Vec::new();

            let mut children = Vec::new();
            let mut offset_of_child = BTreeMap::new();
            let mut size = 0;
            let mut current_offset = 0;

            while reader.read()? != 0 {
                reader.skip_backward(1);
                let mut child_metadata: Metadata = reader.parse()?;
                if child_metadata.chain_head == 0 {
                    // The `..` entry of any subdirectory of the root directory will have `0`
                    // as their chain head...
                    child_metadata.chain_head = boot_record.root_dir_first_cluster;
                }
                if !child_metadata.flags.contains(FileFlags::LFN)
		    // According to Linux' driver, 0xe5 tags the entry as "unused", which is a
		    // way of saying "deleted but we were to lazy to clean it up".
		    && child_metadata.name.content()[0] != 0xe5
                {
                    let child_id = id_of_head
                        .get(&child_metadata.chain_head)
                        .copied()
                        .unwrap_or_else(|| id.next());
                    children.push((child_id, child_metadata.name.clone()));
                    offset_of_child.insert(child_id, size);
                    let moved = size != current_offset;
                    to_process.push((child_id, child_metadata, size, moved, current_offset));

                    size += Metadata::SIZE;
                }

                current_offset += Metadata::SIZE;

                if current_offset % boot_record.bytes_per_cluster() as usize == 0 {
                    // We have reached the end of the current cluster, but there are still some
                    // children in the directory to be parsed.
                    match fat[current_cluster as usize] {
                        ClusterKind::Used(Some(next)) => {
                            current_cluster = next;
                            reader.go_to(boot_record.offset(current_cluster));
                        }
                        ClusterKind::Used(None) => {
                            return err!(FsError::DirectoryNonNullEnded {
                                id: node_id,
                                name: metadata.name,
                            })
                        }
                        ClusterKind::BadCluster => {
                            return err!(FsError::CorruptedCluster {
                                cluster: current_cluster,
                                name: metadata.name,
                                id: node_id,
                            })
                        }
                        ClusterKind::Free | ClusterKind::Reserved => {
                            return err!(FsError::InvalidChainCluster {
                                cluster: current_cluster,
                                name: metadata.name,
                                id: node_id,
                            })
                        }
                    }
                }
            }

            let modified = current_offset != size;
            metadata.size = size as u32;
            if modified {
                let (cluster, address) =
                    Self::addr_of_offset(boot_record, fat, metadata.chain_head, size);
                device.writer_at(address).encode(&Metadata::EOD)?;

                if let ClusterKind::Used(Some(mut current)) = fat[cluster as usize] {
                    fat[cluster as usize] = ClusterKind::Used(None);
                    while let ClusterKind::Used(mnext) = fat[current as usize] {
                        fat[current as usize] = ClusterKind::Free;
                        available_clusters.insert(current);
                        if let Some(next) = mnext {
                            current = next;
                        }
                    }
                }
            }

            let node = Node::Directory {
                parent: parent_id,
                metadata: metadata.clone(),
                children,
                offset_of_child,
            };
            pool.insert(node_id, node);
            for (child_id, child_metadata, offset, offset_changed, old_location) in to_process {
                let modified = Self::populate_node(
                    Some(node_id),
                    child_id,
                    child_metadata,
                    pool,
                    id,
                    device,
                    boot_record,
                    fat,
                    available_clusters,
                    id_of_head,
                )?;

                if modified || offset_changed {
                    let (_, address) =
                        Self::addr_of_offset(boot_record, fat, metadata.chain_head, offset);
                    let new_metadata = match &pool[&child_id] {
                        Node::File {
                            metadata: new_child_metadata,
                            ..
                        }
                        | Node::Directory {
                            metadata: new_child_metadata,
                            ..
                        } => new_child_metadata,
                    };
                    device.writer_at(address).encode(new_metadata)?;
                }

                if offset_changed {
                    let (_, address) =
                        Self::addr_of_offset(boot_record, fat, metadata.chain_head, old_location);
                    // Linux' driver doesn't seem to respect the specs. It will read any entry in
                    // the directory that is not null, so we must erase everything.
                    device.writer_at(address).encode(&Metadata::EOD)?;
                }
            }
            Ok(modified)
        } else {
            let node = Node::File {
                parent: parent_id.unwrap(),
                metadata,
            };
            pool.insert(node_id, node);
            Ok(false)
        }
    }

    fn append_cluster(&mut self, cluster: u32) -> u32 {
        let next = self.allocate_cluster();
        self.fat[cluster as usize] = ClusterKind::Used(Some(next));
        next
    }

    fn allocate_cluster(&mut self) -> u32 {
        self.fat_dirty = true;
        let new = self
            .available_clusters
            .pop_first()
            .expect("The filesystem is full.");
        self.fat[new as usize] = ClusterKind::Used(None);
        new
    }

    /// Given the head cluster of a node, and the offset of a location in that node, find the
    /// the cluster that location is in, and the address of that location.
    fn address_of_offset(&self, cluster: u32, offset: usize) -> (u32, usize) {
        Self::addr_of_offset(&self.boot_record, &self.fat, cluster, offset)
    }

    fn addr_of_offset(
        boot_record: &BiosParameterBlock,
        fat: &[ClusterKind],
        mut cluster: u32,
        mut offset: usize,
    ) -> (u32, usize) {
        while offset >= boot_record.bytes_per_cluster() as usize {
            offset -= boot_record.bytes_per_cluster() as usize;
            let ClusterKind::Used(Some(next_cluster)) = fat[cluster as usize] else {
		panic!("Offset greater than size of node.");
	    };
            cluster = next_cluster;
        }
        (cluster, boot_record.offset(cluster) + offset)
    }

    /// Clear all the content of a node.
    ///
    /// For a file, this means removing all of its content. For a directory, this means deleting
    /// all of its children recursively.
    fn clear_node(&mut self, id: NodeId) -> Result<()> {
        // The following trickery (binding `node` again and again) is required because we of
        // exclusive borrow rules.
        // We could be tempted to take the node instead, but the recursive calls to the children
        // need to be able to access their parent in the pool.
        // Note that at least two binds are morally required, because deleting children may alter
        // the pool, making the pointer invalid.

        match self.get_node(id)? {
            Node::Directory { ref children, .. } => {
                for (child_id, child_name) in children.clone() {
                    if child_name == Name::DOT || child_name == Name::DOTDOT {
                        continue;
                    }
                    self.delete_node(child_id)?;
                }
            }
            Node::File { .. } => {}
        }

        // This is equivalent to `self.get_node_mut(...)`, but it *has* to be inlined as such
        // because otherwise Rust doesn't understand that `node` only requires a mutable borrow
        // of `self.pool`, and not to the whole `self`.
        // This pattern happens quite a few other times, as below.
        let node = self
            .pool
            .get_mut(&id)
            .ok_or(error!(FsError::InvalidNodeId(id)))?;

        node.metadata_mut().size = 0;
        if node.is_dir() {
            self.device
                .writer_at(self.boot_record.offset(node.metadata().chain_head))
                .encode(&Metadata::EOD)?;
        }

        let node = self
            .pool
            .get(&id)
            .ok_or(error!(FsError::InvalidNodeId(id)))?;
        if let Some(parent) = node.parent() {
            let parent_node = self
                .pool
                .get(&parent)
                .ok_or(error!(FsError::InvalidNodeId(parent)))?;
            let offset = match parent_node {
                Node::Directory {
                    offset_of_child, ..
                } => offset_of_child[&id],
                Node::File { .. } => {
                    unreachable!("a file node cannot be parent to an other node")
                }
            };
            let (_, address) = self.address_of_offset(node.metadata().chain_head, offset);
            self.device.writer_at(address).encode(node.metadata())?;
        }

        let mut current = node.metadata().chain_head;

        while let ClusterKind::Used(mnext) = self.fat[current as usize] {
            self.fat_dirty = true;
            self.fat[current as usize] = ClusterKind::Free;
            if let Some(next) = mnext {
                current = next;
            }
        }

        Ok(())
    }

    /// Update the metadata of the given node in the parent storage.
    fn update_metadata(&mut self, node_id: NodeId) -> Result<()> {
        let (metadata, parent_id) = {
            let node = self.get_node(node_id)?;
            let Some(parent) = node.parent() else {
		return Ok(())
	    };
            (node.metadata().clone(), parent)
        };

        let (cluster, offset) = match self.get_node(parent_id)? {
            Node::Directory {
                metadata,
                offset_of_child,
                ..
            } => (metadata.chain_head, offset_of_child[&node_id]),
            Node::File { .. } => unreachable!("parent cannot be a file"),
        };

        let (_, address) = self.address_of_offset(cluster, offset);
        self.device.writer_at(address).encode(&metadata)?;
        Ok(())
    }

    /// Delete a node.
    ///
    /// For a directory, this will recursively delete every child in the subtree.
    pub fn delete_node(&mut self, id: NodeId) -> Result<()> {
        self.clear_node(id)?;
        let node = self
            .pool
            .remove(&id)
            .ok_or(error!(FsError::InvalidNodeId(id)))?;
        let parent_id = match node {
            Node::File { parent, .. }
            | Node::Directory {
                parent: Some(parent),
                ..
            } => parent,
            Node::Directory { parent: None, .. } => return err!(FsError::CannotDeleteRoot),
        };
        let parent = self
            .pool
            .get_mut(&parent_id)
            .ok_or(error!(FsError::InvalidNodeId(parent_id)))?;
        match parent {
            Node::Directory {
                metadata,
                offset_of_child,
                children,
                ..
            } => {
                metadata.size -= Metadata::SIZE as u32;
                let offset = offset_of_child.remove(&id).unwrap();
                let (last_child, last_child_name) = children.pop().unwrap();
                if last_child != id {
                    children[offset] = (last_child, last_child_name);

                    let last_child_offset = offset_of_child[&last_child];
                    offset_of_child.insert(last_child, offset);
                    let (_, last_child_address) = Self::addr_of_offset(
                        &self.boot_record,
                        &self.fat,
                        metadata.chain_head,
                        last_child_offset,
                    );
                    // Rust bug: if there is no type hint, it looks like Rust will default to
                    // typing this variable as `()`, then complains that it doesn't implement
                    // the required traits...
                    // I suspect this is due to error conversion constraints in the `try!` macro
                    // which are wrong.
                    let metadata_last_child: Metadata =
                        self.device.reader_at(last_child_address).parse()?;

                    let (_, deleted_child_address) = Self::addr_of_offset(
                        &self.boot_record,
                        &self.fat,
                        metadata.chain_head,
                        offset,
                    );
                    self.device
                        .writer_at(deleted_child_address)
                        .encode(&metadata_last_child)?;
                }
                let (_, address) = Self::addr_of_offset(
                    &self.boot_record,
                    &self.fat,
                    metadata.chain_head,
                    metadata.size as usize,
                );
                self.device.writer_at(address).encode(&Metadata::EOD)?;
            }
            Node::File { .. } => unreachable!("parent cannot be a file"),
        }
        self.update_metadata(parent_id)?;
        Ok(())
    }

    fn create_node_in(&mut self, id: NodeId, metadata: &Metadata) -> Result<NodeId> {
        let node = self
            .pool
            .get_mut(&id)
            .ok_or(error!(FsError::InvalidNodeId(id)))?;
        match node {
            Node::File { .. } => err!(FsError::CreateNodeInFile {
                parent_id: id,
                name: metadata.name.clone(),
            }),
            Node::Directory {
                children,
                offset_of_child,
                metadata,
                ..
            } => {
                let child_id = self.id.next();
                children.push((child_id, metadata.name.clone()));
                offset_of_child.insert(child_id, metadata.size as usize);
                let (cluster, mut address) = Self::addr_of_offset(
                    &self.boot_record,
                    &self.fat,
                    metadata.chain_head,
                    metadata.size as usize,
                );
                self.device.writer_at(address).encode(metadata)?;
                metadata.size += Metadata::SIZE as u32;
                if metadata.size % self.boot_record.bytes_per_cluster() == 0 {
                    self.fat_dirty = true;
                    let next = self
                        .available_clusters
                        .pop_last()
                        .expect("The filesystem is full.");
                    self.fat[next as usize] = ClusterKind::Used(None);
                    self.fat[cluster as usize] = ClusterKind::Used(Some(next));
                    address = self.boot_record.offset(next);
                } else {
                    address += Metadata::SIZE;
                }
                self.device.writer_at(address).encode(&Metadata::EOD)?;
                self.update_metadata(id)?;
                Ok(child_id)
            }
        }
    }

    /// Create a new, empty directory with the given `name` in the given directory.
    pub fn create_directory_in(&mut self, id: NodeId, name: Name) -> Result<NodeId> {
        let cluster = self.allocate_cluster();
        let metadata = Metadata::new(name, FileFlags::DIRECTORY, cluster, 0);
        let child_id = self.create_node_in(id, &metadata)?;
        let node = Node::Directory {
            parent: Some(id),
            metadata,
            offset_of_child: BTreeMap::new(),
            children: Vec::new(),
        };
        self.pool.insert(child_id, node);
        self.device
            .writer_at(self.boot_record.offset(cluster))
            .encode(&Metadata::EOD)?;
        Ok(child_id)
    }

    /// Create a new, empty file with the given `name` in the given directory.
    pub fn create_file_in(&mut self, id: NodeId, name: Name) -> Result<NodeId> {
        let metadata = Metadata::new(name, FileFlags::empty(), self.allocate_cluster(), 0);
        let child_id = self.create_node_in(id, &metadata)?;
        let node = Node::File {
            parent: id,
            metadata,
        };
        self.pool.insert(child_id, node);
        Ok(child_id)
    }

    fn append_byte_to_file(
        &mut self,
        size: &mut u32,
        cluster: &mut u32,
        address: &mut usize,
        byte: u8,
    ) -> Result<()> {
        self.device.write_at(*address, byte)?;
        *size += 1;
        if *size % self.boot_record.bytes_per_cluster() == 0 {
            *cluster = self.append_cluster(*cluster);
            *address = self.boot_record.offset(*cluster);
        } else {
            *address += 1;
        }
        Ok(())
    }

    pub fn append_to_file(&mut self, id: NodeId, bytes: &[u8]) -> Result<()> {
        let node = self.get_node(id)?;
        let mut size = node.metadata().size;
        let mut chain_head = node.metadata().chain_head;
        if !self.fat[chain_head as usize].is_used() {
            chain_head = self.allocate_cluster();
        }
        let (mut cluster, mut address) = self.address_of_offset(chain_head, size as usize);
        for byte in bytes.iter().copied() {
            self.append_byte_to_file(&mut size, &mut cluster, &mut address, byte)?;
        }
        let metadata = self.get_node_mut(id)?.metadata_mut();
        metadata.size = size;
        metadata.chain_head = chain_head;
        self.update_metadata(id)?;
        Ok(())
    }

    pub fn boot_record(&self) -> &BiosParameterBlock {
        &self.boot_record
    }

    pub fn open(&self, id: NodeId) -> Result<OpenFileCursor> {
        let node = self
            .pool
            .get(&id)
            .ok_or_else(|| error!(FsError::InvalidNodeId(id)))?;
        if node.is_dir() {
            return err!(FsError::CannotOpenDir {
                id,
                name: node.metadata().name().clone()
            });
        }
        Ok(OpenFileCursor::new(id, node.metadata().chain_head))
    }
}

pub struct OpenFileCursor {
    chain_head: u32,
    node: NodeId,
    cluster_stack: Vec<u32>,
    current_offset: usize,
    dirty: bool,
}

impl OpenFileCursor {
    fn new(node: NodeId, chain_head: u32) -> Self {
        Self {
            chain_head,
            node,
            cluster_stack: Vec::new(),
            current_offset: 0,
            dirty: true,
        }
    }

    /// Get the cluster where the cursor is.
    fn cluster(&self) -> u32 {
        self.cluster_stack
            .last()
            .copied()
            .unwrap_or(self.chain_head)
    }

    fn offset_in_cluster(&self, boot_record: &BiosParameterBlock) -> usize {
        self.current_offset % boot_record.bytes_per_cluster() as usize
    }

    /// Get the absolute address of the cursor.
    fn address(&self, boot_record: &BiosParameterBlock) -> usize {
        boot_record.offset(self.cluster())
            + (self.current_offset % boot_record.bytes_per_cluster() as usize)
    }

    pub fn skip_forward<T: Device>(&mut self, mut offset: usize, fs: &CachedFileSystem<T>) {
        if offset == 0 {
            return;
        }
        let offset_in_cluster = self.offset_in_cluster(&fs.boot_record);
        let to_fill_cluster = fs.boot_record.bytes_per_cluster() as usize - offset_in_cluster;
        let mut delta = offset.min(to_fill_cluster);
        while offset >= delta {
            offset -= delta;
            self.current_offset += delta;
            if self.offset_in_cluster(&fs.boot_record) == 0 {
                if let ClusterKind::Used(Some(next)) = fs.get_cluster(self.cluster()) {
                    self.cluster_stack.push(*next);
                } else {
                    panic!("Skipping further than file size.");
                }
            }
            delta = fs.boot_record.bytes_per_cluster() as usize;
        }

        self.current_offset += offset;
    }

    pub fn skip_backward(&mut self, offset: usize, boot_record: &BiosParameterBlock) {
        self.current_offset -= offset;
        self.cluster_stack
            .truncate(self.current_offset / boot_record.bytes_per_cluster() as usize);
    }

    pub fn go_to<T: Device>(&mut self, head: usize, fs: &CachedFileSystem<T>) {
        if head >= self.current_offset {
            self.skip_forward(head - self.current_offset, fs);
        } else {
            self.skip_backward(self.current_offset - head, &fs.boot_record)
        }
    }

    pub fn read<T: Device>(&mut self, fs: &mut CachedFileSystem<T>) -> Result<u8> {
        let result = fs.device.read_at(self.address(&fs.boot_record))?;
        self.skip_forward(1, fs);
        Ok(result)
    }

    pub fn write<T: Device>(&mut self, byte: u8, fs: &mut CachedFileSystem<T>) -> Result<()> {
        self.dirty = true;
        if !fs.fat[self.chain_head as usize].is_used() {
            self.chain_head = fs.allocate_cluster();
        }
        fs.device.write_at(self.address(&fs.boot_record), byte)?;
        let node = fs.get_node_mut(self.node)?;
        node.metadata_mut().chain_head = self.chain_head;
        if node.metadata().size as usize == self.current_offset {
            node.metadata_mut().size += 1;
            if node.metadata_mut().size % fs.boot_record().bytes_per_cluster() == 0 {
                self.cluster_stack.push(fs.append_cluster(self.cluster()));
            }
        }
        self.current_offset += 1;
        Ok(())
    }

    pub fn flush<T: Device>(&mut self, fs: &mut CachedFileSystem<T>) -> Result<()> {
        if self.dirty {
            fs.update_metadata(self.node)?;
        }
        self.dirty = false;
        Ok(())
    }
}
