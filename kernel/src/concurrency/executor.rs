use core::task::{Context, Poll, Waker};

use alloc::{collections::BTreeMap, sync::Arc, task::Wake};
use crossbeam_queue::ArrayQueue;
use futures_util::Future;
use x86_64::instructions::interrupts;

use super::{Task, TaskId};

const MAXIMUM_CONCURRENT_TASKS: usize = 100;

#[derive(Debug)]
pub struct Executor {
    tasks: BTreeMap<TaskId, Task>,
    task_queue: Arc<ArrayQueue<TaskId>>,
    waker_cache: BTreeMap<TaskId, Waker>,
}

impl Executor {
    pub fn new() -> Self {
        Executor {
            tasks: BTreeMap::new(),
            task_queue: Arc::new(ArrayQueue::new(MAXIMUM_CONCURRENT_TASKS)),
            waker_cache: BTreeMap::new(),
        }
    }

    pub fn spawn(&mut self, task: impl Future<Output = ()> + 'static) {
        let task = Task::new(task);
        let id = task.id;
        if self.tasks.insert(id, task).is_some() {
            panic!("HELLO what are you DOING?")
        }
        self.task_queue
            .push(id)
            .expect("Try increasing `MAXIMUM_CONCURRENT_TASKS`")
    }

    pub fn run_ready_tasks(&mut self) {
        while let Some(task_id) = self.task_queue.pop() {
            let Some(task) = self.tasks.get_mut(&task_id) else {
		continue;
	    };
            let waker = self
                .waker_cache
                .entry(task_id)
                .or_insert_with(|| TaskWaker::new_waker(task_id, self.task_queue.clone()));
            let mut context = Context::from_waker(&waker);
            if let Poll::Ready(()) = task.poll(&mut context) {
                self.tasks.remove(&task_id);
                self.waker_cache.remove(&task_id);
            }
        }
    }

    pub fn run(&mut self) -> ! {
        loop {
            self.run_ready_tasks();
            self.sleep_if_idle();
        }
    }

    fn sleep_if_idle(&mut self) {
        interrupts::disable();
        if self.task_queue.is_empty() {
            interrupts::enable_and_hlt();
        } else {
            interrupts::enable();
        }
    }
}

#[derive(Debug, Clone)]
struct TaskWaker {
    task_id: TaskId,
    task_queue: Arc<ArrayQueue<TaskId>>,
}

impl TaskWaker {
    fn new_waker(task_id: TaskId, task_queue: Arc<ArrayQueue<TaskId>>) -> Waker {
        let waker = Self {
            task_id,
            task_queue,
        };
        Waker::from(Arc::new(waker))
    }

    fn wake_task(&self) {
        self.task_queue
            .push(self.task_id)
            .expect("Try increasing `MAXIMUM_CONCURRENT_TASKS` bis")
    }
}

impl Wake for TaskWaker {
    fn wake(self: Arc<Self>) {
        self.wake_task()
    }

    fn wake_by_ref(self: &Arc<Self>) {
        self.wake_task()
    }
}
