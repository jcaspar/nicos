use core::{
    pin::Pin,
    sync::atomic::{AtomicBool, Ordering},
    task::{Context, Poll, Waker},
};

use alloc::{boxed::Box, sync::Arc, task::Wake};
use futures_util::{task::AtomicWaker, Future};

use crate::hardware;

/// [`yield_now`] will yield from the current task, giving the opportunity to other tasks to be executed. With
/// the current, no-priority execution model, this means that every other task not blocking on resources at the
/// time of the call to [`yield_now`] will be executed before the current task will be executed once again.
pub async fn yield_now() {
    Yield::new().await
}

struct Yield {
    yielded: bool,
    waker: AtomicWaker,
}

impl Yield {
    fn new() -> Self {
        Self {
            yielded: false,
            waker: AtomicWaker::new(),
        }
    }
}

impl Future for Yield {
    type Output = ();

    fn poll(mut self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Self::Output> {
        if self.yielded {
            Poll::Ready(())
        } else {
            self.waker.register(cx.waker());
            self.yielded = true;
            self.waker.wake();
            Poll::Pending
        }
    }
}

/// [`sleep`] guarantees that the current task will not be awaken before `ticks` ticks. A tick
/// is currently defined as 1μs, although this might change. Refer to
/// [`FREQUENCY`](crate::hardware::FREQUENCY).
pub async fn sleep(ticks: usize) {
    Sleep::new(ticks).await
}

struct Sleep {
    inner: Arc<Inner>,
    ticks: usize,
}

impl Sleep {
    fn new(ticks: usize) -> Self {
        Self {
            inner: Arc::new(Inner::new()),
            ticks,
        }
    }
}

impl Future for Sleep {
    type Output = ();

    fn poll(self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Self::Output> {
        if self.inner.has_awoken.load(Ordering::SeqCst) {
            Poll::Ready(())
        } else if !self.inner.has_registered.load(Ordering::SeqCst) {
            self.inner.has_registered.store(true, Ordering::SeqCst);
            self.inner.waker.register(cx.waker());
            unsafe {
                hardware::PIT.lock().schedule(self.ticks, {
                    let inner = self.inner.clone();
                    move || {
                        inner.has_awoken.store(true, Ordering::SeqCst);
                        inner.waker.wake();
                    }
                })
            }
            Poll::Pending
        } else {
            Poll::Pending
        }
    }
}

struct Inner {
    waker: AtomicWaker,
    has_registered: AtomicBool,
    has_awoken: AtomicBool,
}

impl Inner {
    fn new() -> Self {
        Self {
            waker: AtomicWaker::new(),
            has_registered: AtomicBool::new(false),
            has_awoken: AtomicBool::new(false),
        }
    }
}

pub fn spin_do<T>(future: impl Future<Output = T>) -> T {
    let mut future = Box::pin(future);
    let waker = StupidWaker::new_waker();
    loop {
        let mut context = Context::from_waker(&waker);
        if let Poll::Ready(result) = future.as_mut().poll(&mut context) {
            break result;
        }
    }
}

struct StupidWaker;

impl StupidWaker {
    fn new_waker() -> Waker {
        Waker::from(Arc::new(Self))
    }
}

impl Wake for StupidWaker {
    fn wake(self: Arc<Self>) {}

    fn wake_by_ref(self: &Arc<Self>) {}
}
