use core::{
    cell::UnsafeCell,
    ops::{Deref, DerefMut},
    pin::Pin,
    sync::atomic::{AtomicBool, Ordering},
    task::{Context, Poll},
};
use futures_util::{task::AtomicWaker, Future};

pub struct Mutex<T> {
    waker: AtomicWaker,
    locked: AtomicBool,
    inner: UnsafeCell<T>,
}

impl<T> Mutex<T> {
    pub async fn lock<'a>(&'a self) -> LockGuard<'a, T> {
        MutexGuard { mutex: self }.await
    }

    pub const fn new(inner: T) -> Self {
        Self {
            waker: AtomicWaker::new(),
            locked: AtomicBool::new(false),
            inner: UnsafeCell::new(inner),
        }
    }

    pub fn try_lock<'a>(&'a self) -> Option<LockGuard<'a, T>> {
        if self
            .locked
            .compare_exchange(false, true, Ordering::SeqCst, Ordering::SeqCst)
            .is_ok()
        {
            Some(LockGuard { mutex: self })
        } else {
            None
        }
    }
}

unsafe impl<T> Send for Mutex<T> {}
unsafe impl<T> Sync for Mutex<T> {}

struct MutexGuard<'a, T> {
    mutex: &'a Mutex<T>,
}

impl<'a, T> Future for MutexGuard<'a, T> {
    type Output = LockGuard<'a, T>;

    fn poll(self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Self::Output> {
        if let Some(lock) = self.mutex.try_lock() {
            return Poll::Ready(lock);
        }

        self.mutex.waker.register(cx.waker());
        if let Some(lock) = self.mutex.try_lock() {
            Poll::Ready(lock)
        } else {
            Poll::Pending
        }
    }
}

pub struct LockGuard<'a, T> {
    mutex: &'a Mutex<T>,
}

impl<'a, T> Drop for LockGuard<'a, T> {
    fn drop(&mut self) {
        self.mutex.locked.store(false, Ordering::SeqCst);
        self.mutex.waker.wake();
    }
}

impl<'a, T> Deref for LockGuard<'a, T> {
    type Target = T;

    fn deref(&self) -> &Self::Target {
        // SAFETY: We need to ensure that when we transform the raw pointer into a borrow, no exclusive borrow
        // to the underlying data exists, and no mutation happens.
        // This is guaranteed because the only way to access `inner` is through a `LockGuard`, of which there
        // is at most one at all time. Furthermore, we have a borrow of the `LockGuard`, which means we cannot
        // also `DerefMut` it.
        unsafe { &*self.mutex.inner.get() }
    }
}

impl<'a, T> DerefMut for LockGuard<'a, T> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        // SAFETY: See the safety block of `Deref for LockGuard`. We also need to ensure here that the pointer
        // is unaliased, which is guaranteed (again) because the borrow to `self` is exclusive and that borrow
        // is the only way to access the underlying data.
        unsafe { &mut *self.mutex.inner.get() }
    }
}
