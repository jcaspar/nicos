use core::{
    pin::Pin,
    task::{Context, Poll},
};

use crate::shared_resource::Once;
use crossbeam_queue::ArrayQueue;
use futures_util::{task::AtomicWaker, Stream};

use crate::{hardware::KeyEvent, serial_println};

static KEYCODES_BUFFER: Once<ArrayQueue<KeyEvent>> = Once::new();
static KEYCODES_WAKER: AtomicWaker = AtomicWaker::new();

pub(crate) fn add_keycode(keycode: KeyEvent) {
    if let Some(buffer) = KEYCODES_BUFFER.get() {
        if buffer.push(keycode).is_err() {
            serial_println!("Keycode buffer overflown, losing keycode {keycode:?}.")
        } else {
            // A new keycode has been read, time to wake the stream.
            KEYCODES_WAKER.wake()
        }
    } else {
        serial_println!("Keycode buffer unitialized, losing keycode {keycode:?}.")
    }
}

#[non_exhaustive]
pub struct KeycodeStream;

impl KeycodeStream {
    pub fn new() -> Self {
        KEYCODES_BUFFER
            .set(ArrayQueue::new(100))
            .expect("The keycode buffer must be initialized only once.");
        Self
    }
}

impl Stream for KeycodeStream {
    type Item = KeyEvent;

    fn poll_next(self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Option<Self::Item>> {
        // # Note
        //
        // This function must *never* return Poll::Ready(None), because this would break the loop that reads
        // events somewhere else.
        //
        // # Documentation
        //
        // See https://docs.rs/futures-util/latest/futures_util/task/struct.AtomicWaker.html#examples
        // for more detailled explanations about how wakers are used.

        let Some(buffer) = KEYCODES_BUFFER.get() else {
	    unreachable!(
		"The keycodes buffer is unitialized, and yet someone is trying to iterate over keycode events"
	    )
	};

        // If we know the resource is available, we do not bother registering to the waker to dismiss it right
        // away. However, due a condition race, we must check *again* later on (a resource might become
        // available between this check and the moment we register the waker). In this unlikely event, we won't
        // do nothing, but we will have registered the waker for nothing.
        if let Some(keycode) = buffer.pop() {
            return Poll::Ready(Some(keycode));
        }

        // This will mark this future as waiting for the keyboard interrupt to feed something. See
        // [`add_keycode'] for the waker counterpart (ie. the code that wakes this waker).
        KEYCODES_WAKER.register(cx.waker());
        if let Some(keycode) = buffer.pop() {
            // This is the unlikely path
            KEYCODES_WAKER.take();
            Poll::Ready(Some(keycode))
        } else {
            Poll::Pending
        }
    }
}
