//! https://refspecs.linuxbase.org/elf/x86_64-abi-0.99.pdf
//! https://docs.oracle.com/cd/E19683-01/816-1386/6m7qcoblh/index.html

use ::elf::{endian::LittleEndian, section::SectionHeader, *};
use core::ffi::CStr;
use x86_64::{structures::paging::*, VirtAddr};

use super::*;
use crate::{
    fs::vfs::{Path, Permissions},
    hardware::Readable,
    memory::{paging::activate_pagetable, FRAME_ALLOC, MAPPER},
};

pub struct ExecInfo {
    pub entry_point: VirtAddr,
    pub stack_pointer: VirtAddr,
    pub brk: VirtAddr,
}

enum LoadInfo {
    Program {
        entry_point: VirtAddr,
        past_the_end: VirtAddr,
    },
    Interpreter {
        phdr: VirtAddr,
        phent: u64,
        phnum: u64,
        prog_base: VirtAddr,
        entry_point: VirtAddr,
        point_the_end: VirtAddr,
    },
}

impl LoadInfo {
    fn entry_point(&self) -> VirtAddr {
        match self {
            Self::Program { entry_point, .. } => *entry_point,
            Self::Interpreter { entry_point, .. } => *entry_point,
        }
    }

    fn past_the_end(&self) -> VirtAddr {
        match self {
            Self::Program { past_the_end, .. } => *past_the_end,
            Self::Interpreter {
                point_the_end: past_the_end,
                ..
            } => *past_the_end,
        }
    }
}

struct Relocation {
    offset: u64,
    sym: u32,
    rel_type: u32,
    addend: i64,
}

impl Relocation {
    fn perform<F>(&self, base_addr: VirtAddr, rel_section: SectionHeader, get_sym_val: F)
    where
        F: FnOnce(u32, u32) -> Option<VirtAddr>,
    {
        let sym_val = get_sym_val(rel_section.sh_link, self.sym);
        let add = |addr, offset: i64| {
            if offset >= 0 {
                addr + offset as u64
            } else {
                addr - ((-offset) as u64)
            }
        };
        let value = match self.rel_type {
            // abi::R_X86_64_64
            1 => add(sym_val.unwrap(), self.addend).as_u64(),
            // abi::R_X86_64_32
            10 => (add(sym_val.unwrap(), self.addend).as_u64() << 32) >> 32,
            _ => panic!("Unknown relocation type"),
        };
        let addr = base_addr + self.offset;
        unsafe { addr.as_mut_ptr::<u64>().write_volatile(value) }
    }
}

// process data and entry point
// SAFETY: this function activate the page table passed in parameters
// the caller shall activate another page table before moving the one passed here
pub unsafe fn new_proc_from_path(
    fs: &Vfs,
    path: &Path,
    argv: *mut *const u8,
    envp: *mut *const u8,
    page_table: &mut PageTable,
) -> Result<ExecInfo> {
    // vector of strings, index of the start of the strings
    let copy_string_array = |mut tab: *mut *const u8| -> (Vec<u8>, Vec<usize>) {
        let mut v = Vec::new();
        let mut idx = Vec::new();
        let mut i = 0usize;
        while unsafe { !(*tab).is_null() } {
            idx.push(i);
            let string = CStr::from_ptr(unsafe { (*tab).cast() });
            let bytes = string.to_bytes_with_nul();
            i += bytes.len();
            v.extend(bytes);
            tab = tab.wrapping_add(1);
        }
        (v, idx)
    };

    let (argv, argv_idx) = copy_string_array(argv);
    let (envp, envp_idx) = copy_string_array(envp);

    // switch to the new virtual addressing space
    // SAFETY: the caller is responsible for not moving the page_table

    unsafe {
        activate_pagetable(page_table);
    }

    let data = {
        let mut buffer = Vec::new();
        let mut fd = fs.open(path.clone(), Permissions::READ)?;
        fd.read_to_end(&mut buffer)?;
        buffer
    };
    let elf = ElfBytes::<LittleEndian>::minimal_parse(&data).unwrap();
    let load_info = load_elf_at(VirtAddr::new(0), &page_table, &elf, &data, fs).unwrap();
    let aux = build_aux_vector(&load_info);

    // allocate process stack
    for i in 0..(STACK_SIZE / 4096) {
        let addr = VirtAddr::new((STACK_START - (i + 1) * 4096) as u64);
        let page = Page::from_start_address(addr).unwrap();
        let mut mapper = MAPPER.get();
        let mut frame_allocator = FRAME_ALLOC.get();
        unsafe {
            mapper
                .map_to(
                    page,
                    frame_allocator.allocate_frame().unwrap(),
                    PageTableFlags::PRESENT
                        | PageTableFlags::USER_ACCESSIBLE
                        | PageTableFlags::WRITABLE,
                    &mut *frame_allocator,
                )
                .unwrap()
                .flush();
        };
    }

    let mut stack_pointer = VirtAddr::new(STACK_START as u64 - 1);

    // copy argv
    stack_pointer -= argv.len() * 8;
    let argv_pointer = stack_pointer;
    unsafe {
        let slice = core::slice::from_raw_parts_mut(argv_pointer.as_mut_ptr(), argv.len());
        slice.copy_from_slice(&argv);
    }

    // copy envp
    stack_pointer -= envp.len() * 8;
    let envp_pointer = stack_pointer;
    unsafe {
        let slice = core::slice::from_raw_parts_mut(envp_pointer.as_mut_ptr(), envp.len());
        slice.copy_from_slice(&envp);
    }

    let arg_len = aux.len() + envp.len() + argv.len() + 1; // aux + envp + argv + argc

    if (stack_pointer.as_u64() as usize + arg_len * 8) & 0xF != 0 {
        stack_pointer += 8u64; // align stack pointer to 16 bytes
    }

    // push aux
    stack_pointer -= aux.len() * 8 * 2; // 2 bytes for each entry
    unsafe {
        let slice = core::slice::from_raw_parts_mut(stack_pointer.as_mut_ptr(), aux.len());
        slice.copy_from_slice(&aux);
    }

    // push envp
    stack_pointer -= envp.len() * 8;
    unsafe {
        let slice = core::slice::from_raw_parts_mut(stack_pointer.as_mut_ptr(), envp_idx.len());
        for i in 0..envp_idx.len() {
            slice[i] = envp_pointer + envp_idx[i] * 8;
        }
    }

    // push argv
    stack_pointer -= argv.len() * 8;
    unsafe {
        let slice = core::slice::from_raw_parts_mut(stack_pointer.as_mut_ptr(), argv_idx.len());
        for i in 0..argv_idx.len() {
            slice[i] = argv_pointer + argv_idx[i] * 8;
        }
    }

    // push argc
    stack_pointer -= 4u64;
    unsafe { *stack_pointer.as_mut_ptr() = argv.len() as i32 - 1 };

    Ok(ExecInfo {
        entry_point: load_info.entry_point(),
        stack_pointer,
        brk: load_info.past_the_end(),
    })
}

fn load_elf_at(
    base_address: VirtAddr,
    page_table: &PageTable,
    elf: &ElfBytes<LittleEndian>,
    raw_data: &[u8],
    fs: &Vfs,
) -> Result<LoadInfo> {
    let Some(segments) = elf.segments()
    else { return err!(ProcError::SegmentError); };

    let mut point_the_end = base_address;
    let mut interp = None;
    // load segments and find PT_INTERP and PT_PHDR
    for segment in segments {
        if let abi::PT_LOAD = segment.p_type {
            let start_address = (segment.p_paddr >> 12) << 12; // align to 4096
            let mut mapper = MAPPER.get();
            let mut frame_allocator = FRAME_ALLOC.get();
            let mut flags = PageTableFlags::PRESENT | PageTableFlags::USER_ACCESSIBLE;
            //TODO: find how to change the flags later
            flags |= PageTableFlags::WRITABLE;
            /*if segment.p_flags & abi::PF_W != 0 {
                flags |= PageTableFlags::WRITABLE;
            }
            if segment.p_flags & abi::PF_X == 0 {
                flags |= PageTableFlags::NO_EXECUTE;
            }*/

            let last_address = segment.p_paddr + segment.p_memsz;
            let real_size = last_address - start_address;
            let nb_page = real_size / 4096 + (if real_size & 0xFFF != 0 { 1 } else { 0 });
            point_the_end = point_the_end.max(VirtAddr::new(start_address + nb_page * 4096));

            // alloc memory
            for i in 0..nb_page {
                let addr = VirtAddr::new(start_address + i * 4096);
                let page = Page::from_start_address(addr).unwrap();
                unsafe {
                    mapper
                        .map_to(
                            page,
                            frame_allocator.allocate_frame().unwrap(),
                            flags,
                            &mut *frame_allocator,
                        )
                        .unwrap()
                        .flush();
                };
            }
            // copy data
            let ptr = segment.p_paddr as *mut u8;
            let src = raw_data.as_ptr().wrapping_add(segment.p_offset as usize);
            unsafe {
                core::ptr::copy_nonoverlapping(src, ptr, segment.p_filesz as usize);
            }
            // zero bss
            for i in segment.p_filesz..segment.p_memsz {
                let ptr = (start_address + i) as *mut u8;
                unsafe {
                    *ptr = 0;
                }
            }
        } else if let abi::PT_INTERP = segment.p_type {
            interp = Some(segment);
        }
    }

    // program must call an interpreter
    if let Some(interp) = interp {
        let (path_len, _) = raw_data[interp.p_offset as usize..]
            .iter()
            .enumerate()
            .find(|(_, c)| **c == 0)
            .unwrap();
        let path =
            Path::new(&raw_data[interp.p_offset as usize..interp.p_offset as usize + path_len])?;
        let interp_data = {
            let mut buffer = Vec::new();
            let mut fd = fs.open(path, Permissions::READ)?;
            fd.read_to_end(&mut buffer)?;
            buffer
        };
        let interp_elf = ElfBytes::minimal_parse(&interp_data).unwrap();
        let interp_info = load_elf_at(point_the_end, page_table, &interp_elf, &interp_data, fs)?;
        point_the_end = interp_info.past_the_end();

        // copy phdr in memory
        // it may have been copied already but it is easier to copy it again
        // that to check if it belongs to a PT_LOAD segment
        let page = Page::from_start_address(point_the_end).unwrap();
        let mut mapper = MAPPER.get();
        let mut frame_allocator = FRAME_ALLOC.get();
        unsafe {
            mapper
                .map_to(
                    page,
                    frame_allocator.allocate_frame().unwrap(),
                    PageTableFlags::USER_ACCESSIBLE
                        | PageTableFlags::PRESENT
                        | PageTableFlags::NO_EXECUTE,
                    &mut *frame_allocator,
                )
                .unwrap()
                .flush();
        };
        let phdr = point_the_end;
        point_the_end += 4096usize;
        let phdr_size = (elf.ehdr.e_phentsize * elf.ehdr.e_phnum) as usize;
        let phdr_data = &raw_data[elf.ehdr.e_phoff as usize..elf.ehdr.e_phoff as usize + phdr_size];
        unsafe {
            let slice = core::slice::from_raw_parts_mut(phdr.as_mut_ptr(), phdr_size);
            slice.copy_from_slice(phdr_data);
        }

        Ok(LoadInfo::Interpreter {
            phdr,
            phent: elf.ehdr.e_phentsize as _,
            phnum: elf.ehdr.e_phnum as _,
            prog_base: VirtAddr::new(elf.ehdr.e_entry),
            entry_point: interp_info.entry_point(),
            point_the_end,
        })
    } else {
        let get_sym_val = |sym_section: u32, sym: u32| {
            let section = elf.section_headers()?.get(sym_section as usize).ok()?;
            let (symbtab, _) = if let abi::SHT_DYNSYM = section.sh_type {
                elf.dynamic_symbol_table()
            } else if let abi::SHT_SYMTAB = section.sh_type {
                elf.symbol_table()
            } else {
                return None;
            }
            .ok()??;
            let sym = symbtab.get(sym as usize).ok()?;

            if sym.is_undefined() {
                None
            } else {
                Some(base_address + sym.st_value)
            }
        };

        let section_headers = elf.section_headers().unwrap();
        for section in section_headers {
            if let Ok(iter) = elf.section_data_as_rels(&section) {
                for rel in iter {
                    let rel = Relocation {
                        offset: rel.r_offset,
                        sym: rel.r_sym,
                        rel_type: rel.r_type,
                        addend: 0,
                    };
                    rel.perform(base_address, section, get_sym_val)
                }
            } else if let Ok(iter) = elf.section_data_as_relas(&section) {
                for rela in iter {
                    let rel = Relocation {
                        offset: rela.r_offset,
                        sym: rela.r_sym,
                        rel_type: rela.r_type,
                        addend: rela.r_addend,
                    };
                    rel.perform(base_address, section, get_sym_val)
                }
            }
        }

        Ok(LoadInfo::Program {
            entry_point: VirtAddr::new(elf.ehdr.e_entry),
            past_the_end: point_the_end,
        })
    }
}

#[allow(dead_code)]
#[repr(i32, C)]
#[derive(Clone, Copy)]
enum Aux {
    Null,
    Ignore,
    Execfd(i64),
    Phdr(VirtAddr),
    Phnum(i64),
    Phent(i64),
    Pagesz(i64),
    Base(VirtAddr),
    Flags(i64),
    Entry(VirtAddr),
    NotElf(i64),
    Uid(i64),
    EUid(i64),
    Gid(i64),
    EGid(i64),
}

fn build_aux_vector(load_info: &LoadInfo) -> Vec<Aux> {
    let mut v = Vec::new();
    v.push(Aux::Pagesz(4096));
    v.push(Aux::NotElf(0));

    // the kernel doesn't support multiple users
    v.push(Aux::Uid(0));
    v.push(Aux::EUid(0));
    v.push(Aux::Gid(0));
    v.push(Aux::EGid(0));

    if let LoadInfo::Interpreter {
        phdr,
        phent,
        phnum,
        prog_base,
        ..
    } = load_info
    {
        v.push(Aux::Phdr(*phdr));
        v.push(Aux::Phent(*phent as _));
        v.push(Aux::Phnum(*phnum as _));
        v.push(Aux::Base(*prog_base));
    }

    v.push(Aux::Null);

    v
}
