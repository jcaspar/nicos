use core::fmt::{self, Display, Formatter};

use super::PId;

#[derive(Debug)]
pub enum Error {
    SegmentError,
    NoInitProc,
    NoParent(PId),
}

impl Display for Error {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        match self {
            Error::SegmentError => write!(f, "Wtf man... what did you try to do???"),
            Error::NoInitProc => write!(f, "Have you killed init? Why... Why?"),
            Error::NoParent(pid) => write!(f, "Process {pid} has no parent???"),
        }
    }
}
