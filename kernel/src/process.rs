use core::{
    ffi::CStr,
    fmt::{self, Display, Formatter},
    sync::atomic::{AtomicU64, Ordering},
};

use alloc::{boxed::Box, vec::Vec};
use futures_util::task::AtomicWaker;
use x86_64::{
    instructions::{
        interrupts::{disable, enable},
        tlb::flush_all,
    },
    registers::segmentation::{Segment, DS, ES, FS, GS},
    structures::paging::{FrameAllocator, Mapper, Page, PageTable, PageTableFlags},
    VirtAddr,
};

use crate::{
    asm::Registers,
    fs::vfs::{Console, FileDescriptor, Path, Permissions, Vfs},
    hardware::{Readable, Writable, CMOS, SELECTORS, TSS},
    memory::{
        layout::KERNEL_STACK_SIZE,
        paging::{activate_pagetable, new_l4_pagetable, recursive_map_pagetable},
        FRAME_ALLOC, MAPPER,
    },
    parallelism::{
        mutex::Mutex,
        scheduler::{dont_reschedule, register, yield_now, Scheduler},
    },
    prelude::*,
    primitives::halt,
    serial_println,
};

use lazy_static::lazy_static;

mod elf;
mod error;

pub use error::Error as ProcError;

const STACK_START: usize = 0x7FFF_FFFF_FFFF + 1;
const STACK_SIZE: usize = 8192;

pub static mut SCHEDULING: bool = false;

#[derive(Debug)]
pub enum ProcessState {
    Waiting,
    Runnable,
    Zombie,
}

#[repr(transparent)]
#[derive(Clone, Copy, PartialEq, Eq, Hash, PartialOrd, Ord, Debug)]
pub struct PId(u64);

impl PId {
    pub fn new(pid: u64) -> Self {
        Self(pid)
    }

    pub fn next_available() -> Self {
        static CURRENT: AtomicU64 = AtomicU64::new(2);
        let next = CURRENT.fetch_add(1, Ordering::SeqCst);
        Self::new(next)
    }
}

impl Display for PId {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        write!(f, "{}", self.0)
    }
}

impl From<PId> for u64 {
    fn from(pid: PId) -> Self {
        pid.0
    }
}

#[repr(transparent)]
#[derive(Clone, Copy, PartialEq, Eq, Hash, Debug)]
pub struct ChanId(isize);

#[derive(Debug)]
pub struct ProcessControlBlock {
    pid: PId,
    ppid: PId,
    state: ProcessState,
    saved_registers: Registers,
    page_table: Box<PageTable>,
    brk: VirtAddr,
    kernel_stack: VirtAddr,
    file_descriptors: Vec<FileDescriptor>,
    waker: AtomicWaker,
}

impl ProcessControlBlock {
    #[inline]
    pub fn pid(&self) -> PId {
        self.pid
    }

    #[inline]
    pub fn parent_pid(&self) -> PId {
        self.ppid
    }
}

pub enum ChannelState {
    Unused,
    Sender(PId, i64),
    Receivers(Vec<PId>),
}

pub struct KernelState {
    // /// process 0 and 1 never die
    // processes: BTreeMap<PId, ProcessControlBlock>,
    _channels: Vec<ChannelState>,
    /// list of kernel stacks yet to be freed
    to_be_freed: Vec<VirtAddr>,
    fs: Option<Vfs>,
    scheduler: Scheduler,
}

/// Interrupts are disabled
#[no_mangle]
extern "C" fn syscall_dispatch(regs: &mut Registers) {
    serial_println!("Start dispatching syscall {}", regs.rax);
    for kernel_stack in &KERNEL.try_lock().unwrap().to_be_freed {
        let ptr = *kernel_stack - (KERNEL_STACK_SIZE as usize + 1);
        let ptr = unsafe { Box::<[u8; KERNEL_STACK_SIZE as usize]>::from_raw(ptr.as_mut_ptr()) };
        drop(ptr)
    }
    let mut kernel = KERNEL.try_lock().unwrap();
    kernel.scheduler.current_proc_mut().saved_registers = *regs;
    kernel.syscall().unwrap();
    *regs = kernel.scheduler.current_proc_mut().saved_registers;
    serial_println!("Syscall {} has been dispatched", regs.rax);
}

pub fn scheduler_do_something(regs: &mut Registers) {
    unsafe {
        let Some(kernel) = &mut KERNEL.try_lock()
        else { return; };
        kernel.scheduler.current_proc_mut().saved_registers = *regs;
        kernel.scheduler.launch_next();
        let proc = kernel.scheduler.current_proc_mut();
        *regs = proc.saved_registers;
        TSS.try_lock().unwrap().privilege_stack_table[0] = proc.kernel_stack;
        flush_all();
    }
}

lazy_static! {
    pub static ref KERNEL: Mutex<KernelState> = Mutex::new(KernelState::new());
}

impl KernelState {
    pub fn new() -> Self {
        Self {
            _channels: Vec::new(),
            to_be_freed: Vec::new(),
            scheduler: Scheduler::new(),
            fs: None,
        }
    }

    pub fn scheduler(&self) -> &Scheduler {
        &self.scheduler
    }

    pub fn launch_user_space(&mut self) -> Result<!> {
        disable();

        unsafe {
            if SCHEDULING {
                panic!("Initialized kernel twice");
            } else {
                SCHEDULING = true;
            }
        }

        self.fs = Some(Vfs::new(1)?);

        // call execve
        let path = b"/bin/init\0";
        let mut argv = [path.as_ptr(), core::ptr::null()];
        let mut envp = [core::ptr::null::<u8>()];

        let mut regs = Registers::default();
        regs.rax = 59;
        regs.rdi = path.as_ptr() as u64;
        regs.rsi = argv.as_mut_ptr() as u64;
        regs.rdx = envp.as_mut_ptr() as u64;

        let kernel_stack =
            VirtAddr::from_ptr(Box::into_raw(Box::new([0u8; 8192]))) + 8192u64 - 16u64;
        let mut file_descriptors = Vec::new();
        file_descriptors.push(FileDescriptor::Console(Console));
        file_descriptors.push(FileDescriptor::Console(Console));
        file_descriptors.push(FileDescriptor::Console(Console));
        let proc = ProcessControlBlock {
            pid: PId(1),
            ppid: PId(1),
            state: ProcessState::Runnable,
            saved_registers: regs,
            page_table: Box::new(new_l4_pagetable()),
            brk: VirtAddr::zero(),
            waker: AtomicWaker::new(),
            kernel_stack,
            file_descriptors,
        };
        self.scheduler.spawn(proc);
        let page_table = &mut self
            .scheduler
            .get_proc_mut(PId(1))
            .ok_or(error!(ProcError::NoInitProc))?
            .page_table;
        recursive_map_pagetable(page_table, &*MAPPER.get());

        self.syscall()?;

        // setup kernel stack
        TSS.lock().privilege_stack_table[0] = kernel_stack;
        let ds = SELECTORS.get().unwrap()[2];
        unsafe {
            DS::set_reg(ds);
            ES::set_reg(ds);
            FS::set_reg(ds);
            GS::set_reg(ds);
        }

        serial_println!("Entering user space");
        let proc = &self
            .scheduler
            .get_proc(PId(1))
            .ok_or(error!(ProcError::NoInitProc))?;
        let rcx = proc.saved_registers.rcx;
        let r11 = proc.saved_registers.r11;
        let rsp = proc.saved_registers.rsp;
        flush_all();

        unsafe {
            KERNEL.force_unlock();
        }

        unsafe {
            core::arch::asm!(
                "mov rsp, rax",

                "xor rax, rax",
                "xor rbx, rbx",
                "xor rdx, rdx",
                "xor rbp, rbp",
                "xor rsi, rsi",
                "xor rdi, rdi",
                "xor r8, r8",
                "xor r9, r9",
                "xor r10, r10",
                "xor r12, r12",
                "xor r13, r13",
                "xor r14, r14",
                "xor r15, r15",
                // there is no interrupt between sti and the following instruction
                "sti",
                "sysretq",
                in("rcx") rcx,
                in("r11") r11,
                in("rax") rsp,
                options(noreturn)
            )
        }
    }

    pub fn syscall(&mut self) -> Result<()> {
        let regs = self.scheduler.current_proc().saved_registers;
        // calling convention: arguments in reg.rdi, reg.rsi, reg.rdx, reg.r10, reg.r8, reg.r9
        match regs.rax {
            0 => self.read(regs.rdi, regs.rsi as usize as *mut u8, regs.rdx)?,
            1 => self.write(regs.rdi, regs.rsi as usize as *mut u8, regs.rdx)?,
            2 => self.open(regs.rdi as usize as *mut u8, regs.rsi, regs.rdx)?,
            3 => self.close(regs.rdi)?,
            4 => (),  //Stat,
            5 => (),  //FStat,
            6 => (),  //LStat,
            7 => (),  //Poll,
            8 => (),  // LSeek,
            9 => (),  // MMap,
            10 => (), //MProtect,
            11 => (), // MunMap,
            12 => self.brk(regs.rdi as isize),
            16 => (), // ioctl
            39 => self.getpid(),
            57 => self.fork(),
            59 => self.execve(regs.rdi as *const u8, regs.rsi as _, regs.rdx as _)?,
            60 => self.exit(regs.rdi)?,
            110 => self.getppid(),
            200 => self.print_time(),
            250 => self.debug(),
            251 => (), // Hint do nothing
            252 => halt(),
            253 => self.flush_vfs()?,
            300 => self.wait()?,
            _ => panic!("Unknown syscall {:?}", regs),
        }
        Ok(())
    }

    fn flush_vfs(&mut self) -> Result<()> {
        if let Some(fs) = self.fs.as_ref() {
            fs.flush()?;
        }
        Ok(())
    }

    fn read(&mut self, fd: u64, mut buffer: *mut u8, transfer_size: u64) -> Result<()> {
        let mut byte_count = 0;
        for i in 0..transfer_size {
            if let Some(b) =
                self.scheduler.current_proc_mut().file_descriptors[fd as usize].read()?
            {
                unsafe {
                    buffer.write(b);
                    buffer = buffer.add(1);
                }
            } else {
                byte_count = i;
            }
        }

        self.scheduler.current_proc_mut().saved_registers.rax = byte_count;
        Ok(())
    }

    fn write(&mut self, fd: u64, mut buffer: *mut u8, transfer_size: u64) -> Result<()> {
        for i in 0..transfer_size {
            self.scheduler.current_proc_mut().file_descriptors[fd as usize]
                .write(unsafe { *buffer })?;
            buffer = unsafe { buffer.add(1) };
            self.scheduler.current_proc_mut().saved_registers.rax = i;
        }

        Ok(())
    }

    /// option flags and mode are currently not supported
    fn open(&mut self, file_name: *mut u8, _options_flags: u64, _mode: u64) -> Result<()> {
        let file_name = unsafe { CStr::from_ptr(file_name.cast()) };
        let file_name = file_name.to_bytes();
        let file_name = Path::new(file_name).unwrap();

        let file = self
            .fs
            .as_ref()
            .unwrap()
            .open(file_name, Permissions::all())?;

        let proc = self.scheduler.current_proc_mut();
        let len = proc.file_descriptors.len();
        proc.file_descriptors.push(file);
        proc.saved_registers.rax = len as u64;
        Ok(())
    }

    fn close(&mut self, file_descriptor: u64) -> Result<()> {
        self.scheduler.current_proc_mut().file_descriptors[file_descriptor as usize].flush()?;
        Ok(())
    }

    fn debug(&mut self) {
        let proc = &self.scheduler.current_proc();
        serial_println!(
            "Debug {:?}: {:?} {:?} {:?}",
            proc.pid(),
            proc.brk,
            proc.kernel_stack,
            proc.saved_registers
        );
        println!(
            "Debug {:?}: {:?} {:?} {:?}",
            proc.pid(),
            proc.brk,
            proc.kernel_stack,
            proc.saved_registers
        );
    }

    fn brk(&mut self, increment: isize) {
        let proc = self.scheduler.current_proc_mut();
        let old_brk = proc.brk;
        if increment >= 0 {
            proc.brk += increment as usize;
        } else {
            proc.brk -= (-increment) as usize;
        }

        if old_brk < proc.brk {
            let start = old_brk.align_up(4096u64);
            let end = proc.brk.align_up(4096u64);

            for i in 0..((end - start) & 0xFFF) {
                let page = Page::from_start_address(start + i * 4096).unwrap();
                let mut mapper = MAPPER.get();
                let mut frame_allocator = FRAME_ALLOC.get();
                unsafe {
                    mapper
                        .map_to(
                            page,
                            frame_allocator.allocate_frame().unwrap(),
                            PageTableFlags::PRESENT
                                | PageTableFlags::USER_ACCESSIBLE
                                | PageTableFlags::WRITABLE,
                            &mut *frame_allocator,
                        )
                        .unwrap()
                        .flush();
                };
            }
        }

        self.scheduler.current_proc_mut().saved_registers.rax = old_brk.as_u64();
    }

    fn fork(&mut self) {
        let new_pid = PId::next_available();
        let mut registers = self.scheduler.current_proc().saved_registers;
        registers.rax = 0;

        let new_proc = ProcessControlBlock {
            pid: new_pid,
            ppid: self.scheduler.current_pid(),
            state: ProcessState::Runnable,
            saved_registers: registers,
            page_table: self.scheduler.current_proc().page_table.clone(),
            brk: self.scheduler.current_proc().brk,
            kernel_stack: VirtAddr::from_ptr(
                Box::new([0; KERNEL_STACK_SIZE as usize])
                    .as_mut_ptr()
                    .wrapping_add(KERNEL_STACK_SIZE as usize - 1),
            ),
            file_descriptors: self.scheduler.current_proc().file_descriptors.clone(),
            waker: AtomicWaker::new(),
        };
        todo!("Really clone the page table, and reallocate the associated pages");
        let curr_proc = self.scheduler.current_proc_mut();
        curr_proc.saved_registers.rax = 0;
        curr_proc.saved_registers.rdi = new_pid.0 as u64;
        self.scheduler.spawn(new_proc);
    }

    fn execve(
        &mut self,
        file_name: *const u8,
        argv: *mut *const u8,
        envp: *mut *const u8,
    ) -> Result<()> {
        let file_name = unsafe { CStr::from_ptr(file_name.cast()) };
        let file_name = file_name.to_bytes();
        let file_name = Path::new(file_name).unwrap();

        let mut new_page_table = Box::new(new_l4_pagetable());
        recursive_map_pagetable(&mut *new_page_table, &*MAPPER.get());
        // SAFETY: new_page_table does not move
        let exec_info = unsafe {
            self::elf::new_proc_from_path(
                self.fs.as_mut().unwrap(),
                &file_name,
                argv,
                envp,
                &mut new_page_table,
            )?
        };

        // doesn't need to reallocate kernel stack
        let proc = self.scheduler.current_proc_mut();
        proc.brk = exec_info.brk;
        proc.state = ProcessState::Runnable;

        // deallocate old page table
        // serial_println!("A");
        // for entry in proc.page_table.iter() {
        //     if let Ok(frame) = entry.frame() {
        //         serial_println!("x");
        //         unsafe {
        //             FRAME_ALLOC.get().deallocate_frame(frame);
        //         }
        //         serial_println!("y");
        //     }
        // }
        // serial_println!("C");

        // we don't need to remap the page table as it has not moved
        proc.page_table = new_page_table;
        self.scheduler.current_proc_mut().saved_registers = Registers {
            rsp: exec_info.stack_pointer.as_u64(),
            rcx: exec_info.entry_point.as_u64(),
            ..Default::default()
        };
        Ok(())
    }

    /// 0 <= ret_code <= 255
    fn exit(&mut self, ret_code: u64) -> Result<()> {
        let curr_pid = self.scheduler.current_pid();
        for proc in self.scheduler.processes_mut() {
            if proc.ppid == curr_pid {
                proc.ppid = PId(1);
            }
        }
        let this = self.scheduler.current_proc();
        let pid = this.pid;
        let ppid = this.ppid;
        let kernel_stack = this.kernel_stack;
        let child_regs = this.saved_registers;

        for fd in self
            .scheduler
            .current_proc_mut()
            .file_descriptors
            .iter_mut()
        {
            fd.flush()?;
        }

        if pid == ppid {
            panic!("You are your own parent!");
        }

        let parent_proc = self
            .scheduler
            .get_proc_mut(ppid)
            .ok_or_else(|| error!(ProcError::NoParent(pid)))?;
        if let ProcessState::Waiting = parent_proc.state {
            // disable interrupts : it is a problem if the stack
            // is already mark to be freed but the process is still schedulable
            disable();

            // mark the stack to be freed
            self.to_be_freed.push(kernel_stack);

            parent_proc.state = ProcessState::Runnable;
            let mut saved_registers = &mut parent_proc.saved_registers;

            parent_proc.waker.wake();

            // we pass in the virtual memory of the parent for dereferencing rax
            unsafe {
                activate_pagetable(&mut parent_proc.page_table);
            }

            let addr = saved_registers.rax as usize as *mut i32;
            unsafe {
                *addr = ((ret_code & 0xFF) << 8) as i32;
            }
            saved_registers.rdi = pid.into();
            saved_registers.rdx = child_regs.rax;
            self.scheduler.exit_current_process();

            enable();
            // wait for the scheduler to schedule a new process
            crate::halt()
        } else {
            self.scheduler.current_proc_mut().state = ProcessState::Zombie;
            Ok(())
        }
    }

    fn getpid(&mut self) {
        self.scheduler.current_proc_mut().saved_registers.rax = self.scheduler.current_pid().into();
    }

    fn getppid(&mut self) {
        let current_proc = self.scheduler.current_proc_mut();
        current_proc.saved_registers.rax = current_proc.ppid.into();
    }

    fn print_time(&mut self) {
        // this is safe because read_time is atomic, therefore
        // the worst that can happens is to read the time of the previous second
        let time = unsafe { CMOS.data_ptr().read().read_time() };
        serial_println!(
            "|{}|",
            time::OffsetDateTime::from_unix_timestamp(time.0).unwrap()
        );
        println!(
            "Current time: {}",
            time::OffsetDateTime::from_unix_timestamp(time.0).unwrap()
        );
    }

    /// wait for one child to die, and return its pid
    fn wait(&mut self) -> Result<()> {
        self.scheduler.current_proc_mut().state = ProcessState::Waiting;
        register(&self.scheduler.current_proc().waker)?;
        yield_now();
        Ok(())
    }
}
