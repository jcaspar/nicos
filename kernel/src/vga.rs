use bootloader_api::info::{FrameBuffer, FrameBufferInfo, PixelFormat};

use x86_64::instructions::interrupts::without_interrupts;

use crate::shared_resource::SharedResource;
use core::{
    fmt::{self, Arguments, Write},
    marker::PhantomData,
};

#[derive(Clone, Copy, Debug)]
pub struct Color {
    red: u8,
    green: u8,
    blue: u8,
}

impl Color {
    #![allow(unused)]

    const BLACK: Color = Color {
        red: 0,
        green: 0,
        blue: 0,
    };

    const WHITE: Color = Color {
        red: 255,
        green: 255,
        blue: 255,
    };

    const RED: Color = Color {
        red: 255,
        green: 0,
        blue: 0,
    };

    const GREEN: Color = Color {
        red: 0,
        green: 255,
        blue: 0,
    };

    const BLUE: Color = Color {
        red: 0,
        green: 0,
        blue: 255,
    };

    const fn new(red: u8, green: u8, blue: u8) -> Self {
        Self { red, green, blue }
    }
}

pub static VGA: SharedResource<Vga<'static>> = SharedResource::new();

/// Initialize the console with a framebuffer. This is required to be subsequently able
/// to print things to the console.
///
/// # Safety
///
/// This must be called before every other console operation, and the size of the screen
/// mut be at least 16 pixels.
pub unsafe fn init(frame_buffer: &'static mut FrameBuffer) {
    let vga = Vga::new(frame_buffer);
    let (width, height) = vga.window_size();
    vga.fill_rect(0, 0, width, height, Color::BLACK);
    VGA.init(vga);
}

#[macro_export]
macro_rules! print {
    ($($arg:tt)*) => {
        $crate::vga::print(::core::format_args!($($arg)*))
    };
}

#[macro_export]
macro_rules! println {
    ($($arg:tt)+) => {
	$crate::print!("{}\n", ::core::format_args!($($arg)*))
    };
    () => {
	$crate::print!("\n")
    }
}

pub fn print(args: Arguments) {
    without_interrupts(move || {
        // SAFETY: [`print`] can only be called by the kernel, which is single threaded.
        // We also **forbid** the panic handler to print anything, and we prevent any
        // interrupt from happening while writing, so we are sure nobody else is using
        // the console while we do.
        VGA.get().console().write_fmt(args).unwrap()
    });
}

#[derive(Debug)]
pub struct Vga<'a> {
    buffer_start: *mut u8,
    info: FrameBufferInfo,
    console_state: VgaConsoleState,
    // the framebuffer should not be dropped or used while the VGA hold the pointer
    phantom: PhantomData<&'a FrameBuffer>,
}

// just so we can put this into a static SharedReference
unsafe impl<'a> Send for Vga<'a> {}

impl<'a> From<&'a mut FrameBuffer> for Vga<'a> {
    fn from(frame_buffer: &'a mut FrameBuffer) -> Self {
        Self::new(frame_buffer)
    }
}

impl<'a> Vga<'a> {
    pub fn new(frame_buffer: &'a mut FrameBuffer) -> Self {
        Self {
            buffer_start: frame_buffer.buffer_mut().as_mut_ptr(),
            info: frame_buffer.info(),
            console_state: VgaConsoleState::new(),
            phantom: PhantomData,
        }
    }

    #[inline]
    pub fn get_pixel(&self, x: usize, y: usize) -> Color {
        assert!(self.is_valid(x, y));

        unsafe { self.get_pixel_unchecked(x, y) }
    }

    /// Returns the color of a pixel.
    ///
    /// # Safety
    ///
    /// The safety conditions of this function are the same of [`pixel_at_unchecked`].
    pub unsafe fn get_pixel_unchecked(&self, x: usize, y: usize) -> Color {
        let buffer = self.pixel_at_unchecked(x, y);
        match self.info.pixel_format {
            PixelFormat::Rgb => Color {
                red: buffer.offset(0).read_volatile(),
                green: buffer.offset(1).read_volatile(),
                blue: buffer.offset(2).read_volatile(),
            },
            PixelFormat::Bgr => Color {
                red: buffer.offset(2).read_volatile(),
                green: buffer.offset(1).read_volatile(),
                blue: buffer.offset(0).read_volatile(),
            },
            PixelFormat::U8 => {
                let c = buffer.read_volatile();
                Color {
                    red: c,
                    green: c,
                    blue: c,
                }
            }
            PixelFormat::Unknown {
                red_position,
                green_position,
                blue_position,
            } => Color {
                red: buffer.add(red_position as _).read_volatile(),
                green: buffer.add(green_position as _).read_volatile(),
                blue: buffer.add(blue_position as _).read_volatile(),
            },
            _ => panic!("unknown pixel format"),
        }
    }

    /// Returns an address pointing to where the pixel lie in the actual memory, without checking
    /// if the coordinates are within bounds.
    ///
    /// # Safety
    ///
    /// The caller of this function must ensure the coordinates are within the bounds of the
    /// framebuffer.
    pub unsafe fn pixel_at_unchecked(&self, x: usize, y: usize) -> *mut u8 {
        let offset = (self.info.stride * y + x) * self.info.bytes_per_pixel;
        self.buffer_start.add(offset)
    }

    pub fn pixel_at(&mut self, x: usize, y: usize) -> *mut u8 {
        assert!(self.is_valid(x, y));
        // SAFETY: We have ensured (see above) that the coordinates are within bounds.
        unsafe { self.pixel_at_unchecked(x, y) }
    }

    /// Set the color of a pixel, without checking of the coordinates are within bounds.
    ///
    /// # Safety
    ///
    /// The safety conditions of this function are the same of [`pixel_at_unchecked`].
    pub unsafe fn put_pixel_unchecked(&self, x: usize, y: usize, color: Color) {
        self.color_pixel_unchecked(self.pixel_at_unchecked(x, y), color);
    }

    /// Check is the coordinates are within the bounds of the framebuffer.
    #[inline]
    pub fn is_valid(&self, x: usize, y: usize) -> bool {
        x < self.info.width && y < self.info.height
    }

    pub fn fill_rect(&self, x: usize, y: usize, dx: usize, dy: usize, color: Color) {
        assert!(
            dx == 0 || dy == 0 || (self.is_valid(x, y) && self.is_valid(x + dx - 1, y + dy - 1))
        );
        for x in x..(x + dx) {
            for y in y..(y + dy) {
                // SAFETY: We have checked that the corners are within bounds, so the whole
                // rectangle is within bounds.
                unsafe {
                    self.color_pixel_unchecked(self.pixel_at_unchecked(x, y), color);
                }
            }
        }
    }

    #[inline(always)]
    unsafe fn color_pixel_unchecked(&self, pix: *mut u8, col: Color) {
        match self.info.pixel_format {
            PixelFormat::Rgb => {
                pix.add(0).write_volatile(col.red);
                pix.add(1).write_volatile(col.green);
                pix.add(2).write_volatile(col.blue);
            }
            PixelFormat::Bgr => {
                pix.add(0).write_volatile(col.blue);
                pix.add(1).write_volatile(col.green);
                pix.add(2).write_volatile(col.red);
            }
            PixelFormat::U8 => {
                let c = 30 * col.red as u16 + 59 * col.green as u16 + 11 * col.blue as u16;
                pix.write_volatile((c / 100) as u8);
            }
            PixelFormat::Unknown {
                red_position,
                green_position,
                blue_position,
            } => {
                pix.add(red_position as usize).write_volatile(col.red);
                pix.add(green_position as usize).write_volatile(col.green);
                pix.add(blue_position as usize).write_volatile(col.blue);
            }
            _ => panic!("unknown pixel format"),
        }
    }

    /// width, height
    pub fn window_size(&self) -> (usize, usize) {
        (self.info.width, self.info.height)
    }

    pub fn load_default_font() -> &'static [u8; 4096] {
        static FONT: &[u8; 4096] = include_bytes!("../../assets/font");
        FONT
    }

    pub fn write_char(&self, font: &[u8; 4096], chr: u8, x: usize, y: usize, fg: Color, bg: Color) {
        assert!(self.is_valid(x + 7, y + 15));
        without_interrupts(|| unsafe { self.write_char_unchecked(font, chr, x, y, fg, bg) });
    }

    /// color : 0xRRGGBB
    pub unsafe fn write_char_unchecked(
        &self,
        font: &[u8; 4096],
        char: u8,
        x: usize,
        y: usize,
        fg: Color,
        bg: Color,
    ) {
        let mask = [128, 64, 32, 16, 8, 4, 2, 1];
        let glyph = char as usize * 16;
        for cy in 0..16 {
            for (cx, msk) in mask.iter().enumerate() {
                // SAFETY: the rectangle that contains the glyph has the outermost corner within
                // bounds (see assert above), so all the pixels of the glyph are within bounds.
                unsafe {
                    self.put_pixel_unchecked(
                        x + cx,
                        y + cy - 12,
                        if font[glyph + cy] & msk != 0 { fg } else { bg },
                    )
                }
            }
        }
    }

    // line length in characters, 0 means no limit
    // handle newline
    pub fn write_bytes(
        &self,
        font: &[u8; 4096],
        text: &[u8],
        mut x: usize,
        mut y: usize,
        fg: Color,
        bg: Color,
        line_length: usize,
    ) {
        let mut char_in_line = 0;
        for c in text {
            if *c == b'\n' {
                x -= 8 * char_in_line;
                y += 16;
                char_in_line = 0;
                continue;
            }

            self.write_char(font, *c, x, y, fg, bg);
            x += 8;
            char_in_line += 1;
            if char_in_line == line_length {
                char_in_line = 0;
                y += 16;
                x -= 8 * line_length;
            }
        }
    }

    /// zeroes tab and writes the digit of the number in tab, returning a slice of it
    pub fn print_u32(mut n: u32, tab: &mut [u8; 20]) -> &[u8] {
        *tab = [0_u8; 20];
        if n == 0 {
            tab[0] = b'0';
            return &tab[0..1];
        }
        let mut i = 0;
        while n != 0 {
            tab[19 - i] = b'0' + (n % 10) as u8;
            n /= 10;
            i += 1;
        }
        &tab[20 - i..]
    }

    pub fn console(&mut self) -> VgaConsole<'_, 'a> {
        VgaConsole { vga: self }
    }
}

#[derive(Debug)]
struct VgaConsoleState {
    column: usize,
    line: usize,
    fg: Color,
    bg: Color,
    font: &'static [u8; 4096],
}

impl VgaConsoleState {
    pub fn new() -> Self {
        Self {
            column: 0,
            line: 0,
            fg: Color::WHITE,
            bg: Color::BLACK,
            font: Vga::load_default_font(),
        }
    }
}

impl Default for VgaConsoleState {
    fn default() -> Self {
        Self::new()
    }
}

pub struct VgaConsole<'a, 'b> {
    vga: &'a mut Vga<'b>,
}

impl VgaConsole<'_, '_> {
    pub fn width(&self) -> usize {
        self.vga.info.width / 8
    }

    pub fn height(&self) -> usize {
        self.vga.info.height / 16
    }

    pub fn column(&self) -> usize {
        self.vga.console_state.column
    }

    pub fn line(&self) -> usize {
        self.vga.console_state.line
    }

    /// # Safety
    ///
    /// `column` must be less than `self.width()`. For a safe counterpart, see [`set_column`]
    pub unsafe fn set_column_unchecked(&mut self, column: usize) {
        debug_assert!(column < self.width());
        self.vga.console_state.column = column;
    }

    /// Change the column of the cursor. The column must be within bounds.
    ///
    /// # Panic
    ///
    /// This will panic if the column is not within bounds.
    pub fn set_column(&mut self, column: usize) {
        assert!(column < self.width());
        // SAFETY: We have ensured above that `column < self.width - 1`.
        unsafe { self.set_column_unchecked(column) }
    }

    /// # Safety
    ///
    /// `line` must be less than `self.height - 1`
    pub unsafe fn set_line_unchecked(&mut self, line: usize) {
        debug_assert!(line < self.height() - 1);
        self.vga.console_state.line = line;
    }

    pub fn set_line(&mut self, line: usize) {
        assert!(line < self.height() - 1);
        // SAFETY: We have ensured above that `line < self.height - 1`.
        unsafe {
            self.set_line_unchecked(line);
        }
    }

    pub fn fg(&self) -> Color {
        self.vga.console_state.fg
    }

    pub fn bg(&self) -> Color {
        self.vga.console_state.bg
    }

    pub fn set_fg(&mut self, color: Color) {
        self.vga.console_state.fg = color;
    }

    pub fn set_bg(&mut self, color: Color) {
        self.vga.console_state.bg = color;
    }

    pub fn font(&self) -> &'static [u8; 4096] {
        self.vga.console_state.font
    }

    pub fn set_font(&mut self, font: &'static [u8; 4096]) {
        self.vga.console_state.font = font;
    }

    pub fn newline(&mut self) {
        self.set_column(0);

        if self.line() < self.height() - 2 {
            self.set_line(self.line() + 1);
            return;
        }

        for y in 0..self.vga.info.height - 16 {
            for x in 0..self.vga.info.width {
                // SAFETY: we know that the coordinates are within the bounds, since the bounds
                // defined above are exactly the framebuffer's.
                unsafe {
                    self.vga
                        .put_pixel_unchecked(x, y, self.vga.get_pixel_unchecked(x, y + 16));
                }
            }
        }
        for y in 0..16 {
            for x in 0..self.vga.info.width {
                // SAFETY: `x` is definitively within bounds
                // /!\ WE DONT KNOW WHETHER THE FRAME BUFFER IS BIG ENOUGH /!\
                unsafe {
                    self.vga
                        .put_pixel_unchecked(x, self.vga.info.height - 16 + y, Color::BLACK);
                }
            }
        }
    }
}

impl fmt::Write for VgaConsole<'_, '_> {
    // This implementation does not have the right to return `Err`
    fn write_str(&mut self, s: &str) -> fmt::Result {
        for &c in s.as_bytes() {
            if c == b'\n' {
                self.newline();
                continue;
            }

            // SAFETY: according to someone, the internal state of the console is ok, so we don't
            // write outside of the bounds. /!\ THIS IS FISHY /!\
            without_interrupts(|| unsafe {
                self.vga.write_char_unchecked(
                    self.font(),
                    c,
                    self.column() * 8,
                    (self.line() + 1) * 16,
                    self.fg(),
                    self.bg(),
                );
            });
            self.set_column(self.column() + 1);
            if self.column() == self.width() - 1 {
                self.newline();
            }
        }
        Ok(())
    }
}

unsafe impl Sync for Vga<'_> {}
