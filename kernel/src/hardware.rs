#![allow(dead_code)] // unused functionalities may be needed later

use crate::shared_resource::{Once, PanicMutex};
use core::fmt::Write;
pub use x86_64::instructions::{interrupts::without_interrupts, port::Port};

mod cmos;
mod device;
mod error;
mod gdt;
mod harddisk;
mod idt;
mod keyboard;
mod pic;
mod pit;
mod serial;

pub use cmos::CMOS;
pub use device::{Device, Peekable, Readable, Reader, Skippable, Writable, Writer};
pub use error::Error as DeviceError;
pub use gdt::{SELECTORS, TSS};
pub use harddisk::{HardDiskDriver, IDEDriver};
pub use keyboard::{DecodedKey, EventModifiers, KeyCode, KeyEvent, Keyboard, Layout};
pub use pic::{PICs, IRQ};
pub use pit::PIT;
pub use serial::Serial;

// offset of PIC's IRQ in the IDT
const PIC_OFFSET: usize = 0x20;

// indexes of the stack in the TSS for the following exceptions handler
const DOUBLE_FAULT: u16 = 0;
const NMI: u16 = 1;
const DEBUG: u16 = 2;
const MACHINE_CHECK: u16 = 3;

/// PIT frequency, in Hz
pub const FREQUENCY: f64 = 1e2;

// COM port
const COM1: u16 = 0x3F8;
const COM2: u16 = 0x2F8;

// IDE BAR
const BAR0: u16 = 0x1F0;
const BAR1: u16 = 0x3F6;
const BAR2: u16 = 0x170;
const BAR3: u16 = 0x376;
const BAR4: u16 = 0x00;

pub static PICS: Once<PICs> = Once::new();
pub static SERIAL: PanicMutex<Serial> = PanicMutex::new(unsafe { Serial::new(COM1) });
pub static CMOS: PanicMutex<CMOS> = PanicMutex::new(unsafe { CMOS::new() });
pub static KEYBOARD: PanicMutex<Keyboard> = PanicMutex::new(unsafe { Keyboard::new() });
pub static IDE: PanicMutex<IDEDriver> =
    PanicMutex::new(IDEDriver::new(BAR0, BAR1, BAR2, BAR3, BAR4));
pub static PIT: PanicMutex<PIT> = PanicMutex::new(PIT::new());

pub fn _serial_print(args: core::fmt::Arguments) {
    // SAFETY: think about this
    SERIAL.lock().write_fmt(args).unwrap();
}

#[macro_export]
macro_rules! serial_print {
    ($($arg:tt)*) => {
        $crate::hardware::_serial_print(format_args!($($arg)*))
    };
}

pub use serial_print;
#[macro_export]
macro_rules! serial_println {
    ($($arg:tt)*) => {
	$crate::serial_print!("{}\n", format_args!($($arg)*))
    };
}
pub use serial_println;

fn io_wait() {
    unsafe {
        Port::new(0x80).write(0u8);
    }
}

pub fn init() {
    without_interrupts(|| unsafe {
        gdt::init();
        idt::init();
        let mut pics = PICs::new(PIC_OFFSET as u8, PIC_OFFSET as u8 + 8);
        pics.init();
        PICS.set(pics).unwrap();
        PIT.lock().init(FREQUENCY);
        SERIAL.lock().init();
        CMOS.lock().init();
        KEYBOARD.lock().init();
        IDE.lock().init();
    });
    // Make sure interrupts are enabled
    x86_64::instructions::interrupts::enable();
    unsafe {
        IDE.lock().init();
    }
}
