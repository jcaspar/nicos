use bootloader_api::info::MemoryRegions;

pub mod frame_alloc;
pub mod layout;
pub mod paging;
pub mod rust_alloc;

pub use frame_alloc::FRAME_ALLOC;
pub use paging::MAPPER;

/// # Safety
/// Must only be called once, prior to any allocation or use of MAPPER.
pub unsafe fn init(regions: &MemoryRegions, recursive_index: u16) {
    frame_alloc::init(regions);
    paging::init(recursive_index);
    rust_alloc::init();
}
