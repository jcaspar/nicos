#![no_std]
#![no_main]

extern crate alloc;
use bootloader_api::{entry_point, info, BootInfo};
use core::assert;

use kernel::{hardware, macros::*, memory, process::KERNEL, vga};

entry_point!(main, config = &memory::layout::BOOTLOADER_CONFIG);
fn main(boot_info: &'static mut BootInfo) -> ! {
    // Separate borrow to let us borrow other fields of boot_info later
    let framebuffer: &'static mut info::Optional<info::FrameBuffer> = &mut boot_info.framebuffer;
    assert!(framebuffer.as_ref().unwrap().info().height >= 16);

    // SAFETY: This is called before everything else,
    unsafe {
        vga::init(framebuffer.as_mut().unwrap());
    }

    hardware::init();
    let recursive_index = boot_info.recursive_index.into_option().unwrap();
    unsafe { memory::init(&boot_info.memory_regions, recursive_index) };

    memory::paging::test();

    println!(
        r#"
  _                      ____   U  ___ u  ____     
 | \ |"|       ___    U /"___|   \/"_ \/ / __"| u  
<|  \| |>     |_"_|   \| | u     | | | |<\___ \/   
U| |\  |u      | |     | |/__.-,_| |_| | u___) |   
 |_| \_|     U/| |\u    \____|\_)-\___/  |____/>>  
 ||   \\,-.-,_|___|_,-._// \\      \\     )(  (__) 
 (_")  (_/ \_)-' '-(_/(__)(__)    (__)   (__)      

"#
    );

    let Err(error) = KERNEL.lock().launch_user_space()
    else {
	unreachable!()
    };
    panic!("{error}");
}
