#![no_std]
#![feature(const_trait_impl)]
#![feature(abi_x86_interrupt)]
#![feature(const_mut_refs)]
#![feature(never_type)]

extern crate alloc;

use core::{fmt::Write, panic};

pub mod asm;
pub mod concurrency;
pub mod error;
pub mod fs;
pub mod hardware;
pub mod memory;
pub mod parallelism;
pub mod primitives;
pub mod process;
mod serde;
pub mod shared_resource;
pub mod string;
pub mod vga;

pub mod macros {
    pub use crate::{print, println};
    pub use crate::{serial_print, serial_println};
}

/// This module can be imported in every module of this crate, as it contains items that are
/// always useful.
/// It also re-exports some stuff that is usually in `std::prelude` (such as `Vec` and `Box`).
#[allow(unused_imports)]
pub(crate) mod prelude {
    pub(crate) use crate::error::{err, error, Result};
    pub(crate) use crate::macros::*;
    pub(crate) use crate::string::AsciiString;
    pub(crate) use alloc::{boxed::Box, vec::Vec};
}

use crate::primitives::halt;
use crate::vga::VGA;

#[panic_handler]
fn panic(panic_info: &panic::PanicInfo) -> ! {
    serial_println!("=== KERNEL PANIC ===\n{panic_info}");
    unsafe {
        let mut vga = VGA.force_get();
        let mut console = vga.console();
        // SAFETY: The implementation of [`VgaConsole`] cannot return an `Err`.
        writeln!(console, "\n=== KERNEL PANIC ===\n{panic_info}").unwrap_unchecked();
    }
    halt()
}
