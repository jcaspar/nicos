use crate::{
    hardware::{Device, DeviceError, Writable},
    prelude::*,
};
use alloc::vec::Vec;

impl Device for Vec<u8> {
    fn read_at(&mut self, position: usize) -> Result<Option<u8>> {
        Ok(self.get(position).copied())
    }

    fn write_at(&mut self, position: usize, byte: u8) -> Result<()> {
        let Some(entry) = self.get_mut(position) else {
	    return err!(DeviceError::OutOfBounds);
	};
        *entry = byte;
        Ok(())
    }

    fn flush(&mut self) -> Result<()> {
        Ok(())
    }
}

pub trait Serialize {
    fn serialize(&self, writer: &mut impl Writable) -> Result<()>;
}

impl Serialize for u8 {
    fn serialize(&self, writer: &mut impl Writable) -> Result<()> {
        Ok(writer.write(*self)?)
    }
}

impl Serialize for u16 {
    fn serialize(&self, writer: &mut impl Writable) -> Result<()> {
        self.to_le_bytes().serialize(writer)
    }
}

impl Serialize for u32 {
    fn serialize(&self, writer: &mut impl Writable) -> Result<()> {
        self.to_le_bytes().serialize(writer)
    }
}

impl<T: Serialize> Serialize for [T] {
    fn serialize(&self, writer: &mut impl Writable) -> Result<()> {
        for element in self {
            element.serialize(writer)?;
        }
        Ok(())
    }
}

impl<const N: usize, T: Serialize> Serialize for [T; N] {
    fn serialize(&self, writer: &mut impl Writable) -> Result<()> {
        <[T]>::serialize(self as _, writer)
    }
}

impl Serialize for &str {
    fn serialize(&self, writer: &mut impl Writable) -> Result<()> {
        self.as_bytes().serialize(writer)
    }
}
