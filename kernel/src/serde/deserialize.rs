use crate::{error::Result, hardware::Readable};

pub trait Deserialize: Sized {
    fn deserialize(reader: &mut impl Readable) -> Result<Self>;
}

impl<T: Deserialize + Default + Copy, const N: usize> Deserialize for [T; N] {
    fn deserialize(reader: &mut impl Readable) -> Result<Self> {
        let mut result = [T::default(); N];
        for i in 0..N {
            result[i] = reader.parse()?;
        }
        Ok(result)
    }
}

impl Deserialize for u8 {
    fn deserialize(reader: &mut impl Readable) -> Result<Self> {
        Ok(reader.read()?.unwrap()) // TODO: don't unwrap
    }
}

impl Deserialize for u16 {
    fn deserialize(reader: &mut impl Readable) -> Result<Self> {
        Ok(Self::from_le_bytes(reader.parse()?))
    }
}

impl Deserialize for u32 {
    fn deserialize(reader: &mut impl Readable) -> Result<Self> {
        Ok(Self::from_le_bytes(reader.parse()?))
    }
}
