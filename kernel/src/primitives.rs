use core::arch::asm;

pub fn halt() -> ! {
    unsafe {
        asm!(
            // Clear Interrupt Flag: réduit le nombre d'interrupt pris en compte
            // (le but étant d'arrêter la machine). Cela économise des opérations de CPU
            // si l'OS est simulé, car on peut rester plus longtemps en mode `hlt`.
            "cli",
            "2:", // Bug de LLVM: si on essaye avec 0: ou 1:, ça plante
            "hlt",
            "jmp 2b", // au cas où un interrupt fasse "sortir" du `hlt`
            options(
                nomem,
                nostack,
                noreturn,
                // L'instruction `cli` modifie IF, mais ce n'est pas pris en compte par
                // Rust d'après [la référence][1]
                //
                // [1] https://doc.rust-lang.org/reference/inline-assembly.html
                preserves_flags,
            ),
        )
    }
}
