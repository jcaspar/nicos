use core::arch::global_asm;

use x86_64::structures::idt::InterruptStackFrame;

global_asm! {
    ".global system_clock_interrupt_handler",
    "system_clock_interrupt_handler:",
    // ss, rsp, rflags, cs and rip are pushed by the interrupt
    "push r15",
    "push r14",
    "push r13",
    "push r12",
    "push r11",
    "push r10",
    "push r9",
    "push r8",

    "mov r8, gs",
    "push r8",
    "mov r8, fs",
    "push r8",
    "mov r8, es",
    "push r8",
    "mov r8, ds",
    "push r8",

    "push rdi",
    "push rsi",
    "push rbp",
    "push rdx",
    "push rcx",
    "push rbx",
    "push rax",
    // arg
    "mov rdi, rsp",

    // call the handler
    "mov r15, rsp",
    "and rsp, ~15",
    "call real_system_clock_interrupt_handler",
    "mov rsp, r15",

    // retrieve registers
    "pop rax",
    "pop rbx",
    "pop rcx",
    "pop rdx",
    "pop rbp",
    "pop rsi",
    "pop rdi",

    "pop r8",
    "mov ds, r8",
    "pop r8",
    "mov es, r8",
    "pop r8",
    "mov fs, r8",
    "pop r8",
    "mov gs, r8",

    "pop r8",
    "pop r9",
    "pop r10",
    "pop r11",
    "pop r12",
    "pop r13",
    "pop r14",
    "pop r15",
    // ss, rsp, rflags, cs and rip were directly modified
    "iretq",
}

extern "x86-interrupt" {
    pub fn system_clock_interrupt_handler(stack_frame: InterruptStackFrame);
}

global_asm! {
    ".global syscall_handler",
    "syscall_handler:",
        "cli",
        // InterruptStackFrame is not used here
        "sub rsp, 40",
        // save registers
        "push r15",
        "push r14",
        "push r13",
        "push r12",
        "push r11",
        "push r10",
        "push r9",
        "push r8",

        "mov r8, gs",
        "push r8",
        "mov r8, fs",
        "push r8",
        "mov r8, es",
        "push r8",
        "mov r8, ds",
        "push r8",

        "push rdi",
        "push rsi",
        "push rbp",
        "push rdx",
        "push rcx",
        "push rbx",
        "push rax",

        // calculate the pointer to the struct
        "mov rdi, rsp",

        // set the segment selectors
        "mov ax, 16",
        "mov ds, ax",
        "mov es, ax",
        "mov fs, ax",
        "mov gs, ax",

        // dispatch the syscall
        "mov r15, rsp",
        "and rsp, ~15",
        "call syscall_dispatch",
        "mov rsp, r15",

        // retrieve registers
        "pop rax",
        "pop rbx",
        "pop rcx",
        "pop rdx",
        "pop rbp",
        "pop rsi",
        "pop rdi",

        "pop r8",
        "mov ds, r8",
        "pop r8",
        "mov es, r8",
        "pop r8",
        "mov fs, r8",
        "pop r8",
        "mov gs, r8",

        "pop r8",
        "pop r9",
        "pop r10",
        "pop r11",
        "pop r12",
        "pop r13",
        "pop r14",
        "pop r15",
        // pop the empty InterruptStackFrame
        "add rsp, 40",
        "sti",
        // return to userspace
        "sysretq",
}

extern "C" {
    pub fn syscall_handler();
}

#[repr(C)]
#[derive(Clone, Copy, Debug)]
pub struct Registers {
    pub rax: u64,
    pub rbx: u64,
    /// save rip during syscalls
    pub rcx: u64,
    pub rdx: u64,
    pub rbp: u64,
    pub rsi: u64,
    pub rdi: u64,
    pub ds: u64,
    pub es: u64,
    pub fs: u64,
    pub gs: u64,
    pub r8: u64,
    pub r9: u64,
    pub r10: u64,
    /// save rflags during syscalls
    pub r11: u64,
    pub r12: u64,
    pub r13: u64,
    pub r14: u64,
    pub r15: u64,
    // information pushed on the stack frame during interrupts
    pub rip: u64,
    pub cs: u64,
    pub rflags: u64,
    pub rsp: u64,
    pub ss: u64,
}

const DEFAULT_RFLAGS: u64 = 0b1001000000010;

impl Default for Registers {
    fn default() -> Self {
        Registers {
            rax: 0,
            rbx: 0,
            rcx: 0,
            rdx: 0,
            rbp: 0,
            rsi: 0,
            rdi: 0,
            ds: 0,
            es: 0,
            fs: 0,
            gs: 0,
            r8: 0,
            r9: 0,
            r10: 0,
            r11: DEFAULT_RFLAGS,
            r12: 0,
            r13: 0,
            r14: 0,
            r15: 0,
            rip: 0,
            cs: 0,
            rflags: 0,
            rsp: 0,
            ss: 0,
        }
    }
}
