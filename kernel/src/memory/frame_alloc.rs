use bootloader_api::info;
use x86_64::structures::paging::{frame, FrameAllocator, FrameDeallocator, PhysFrame, Size4KiB};
use x86_64::PhysAddr;

use crate::serial_println;
use crate::shared_resource::SharedResource;

pub struct DumbFrameAllocator(frame::PhysFrameRange<Size4KiB>);

impl DumbFrameAllocator {
    pub fn from_mem_regions(regions: &info::MemoryRegions) -> Self {
        // currently only uses the first usable region, ignoring the rest
        let usable_region: &info::MemoryRegion = regions
            .iter()
            .find(|region| region.kind == info::MemoryRegionKind::Usable)
            .expect("Bootloader did not return a usable memory region");
        let start_addr = PhysAddr::new(usable_region.start);
        let end_addr = PhysAddr::new(usable_region.end); // exclusive

        serial_println!("creating dumb frame allocator");
        serial_println!("start address: {start_addr:?}, end address: {end_addr:?}");
        let start = frame::PhysFrame::from_start_address(start_addr).unwrap();
        let end = frame::PhysFrame::from_start_address(end_addr).unwrap();
        Self(frame::PhysFrameRange { start, end })
    }
}

// This is the absolute dumbest allocator possible and will never deallocate a frame
unsafe impl FrameAllocator<Size4KiB> for DumbFrameAllocator {
    fn allocate_frame(&mut self) -> Option<PhysFrame<Size4KiB>> {
        let nextframe = self.0.next();
        //serial_println!("allocating a frame: {:?}", nextframe);
        nextframe
    }
}

#[derive(Debug)]
enum BuddyStatus {
    Partial,
    Full,
}

#[derive(Debug)]
struct Buddies<T> {
    status: BuddyStatus,
    left: T,
    right: T,
}

#[derive(Debug)]
enum FrameStatus {
    Free,
    Allocated,
}

trait BuddyTree {
    type FrameLocation;
    fn new() -> Self;
    fn is_full(&self) -> bool;
    fn allocate_frame(&mut self) -> Self::FrameLocation;
    fn deallocate_frame(&mut self, _: Self::FrameLocation);
    fn location_to_u64(loc: Self::FrameLocation, acc: u64) -> u64;
    fn u64_to_location(u: u64) -> (Self::FrameLocation, u64);
}

impl BuddyTree for FrameStatus {
    type FrameLocation = ();

    fn new() -> Self {
        FrameStatus::Free
    }

    fn location_to_u64(_: Self::FrameLocation, acc: u64) -> u64 {
        acc
    }
    fn u64_to_location(u: u64) -> (Self::FrameLocation, u64) {
        ((), u)
    }
    fn is_full(&self) -> bool {
        match &self {
            Self::Free => false,
            Self::Allocated => true,
        }
    }
    fn allocate_frame(&mut self) -> Self::FrameLocation {
        match &self {
            Self::Free => {
                *self = Self::Allocated;
            }
            Self::Allocated => panic!(),
        }
    }
    fn deallocate_frame(&mut self, _: Self::FrameLocation) {
        match &self {
            Self::Free => panic!(),
            Self::Allocated => {
                *self = Self::Free;
            }
        }
    }
}

impl<T: BuddyTree> BuddyTree for Buddies<T> {
    type FrameLocation = (bool, T::FrameLocation);

    fn new() -> Self {
        Self {
            status: BuddyStatus::Partial,
            left: T::new(),
            right: T::new(),
        }
    }

    fn location_to_u64((myloc, lowloc): Self::FrameLocation, acc: u64) -> u64 {
        T::location_to_u64(lowloc, (acc << 1) + myloc as u64)
    }
    fn u64_to_location(u: u64) -> (Self::FrameLocation, u64) {
        let (childloc, v) = T::u64_to_location(u);
        let loc = ((v & 1) == 1, childloc);
        (loc, v >> 1)
    }
    fn is_full(&self) -> bool {
        match &self.status {
            BuddyStatus::Partial => false,
            BuddyStatus::Full => true,
        }
    }

    fn allocate_frame(&mut self) -> Self::FrameLocation {
        match &self.status {
            BuddyStatus::Partial => {
                if self.left.is_full() {
                    let right_location = self.right.allocate_frame();
                    if self.right.is_full() {
                        self.status = BuddyStatus::Full
                    };
                    (true, right_location)
                } else {
                    (false, self.left.allocate_frame())
                }
            }
            BuddyStatus::Full => panic!(),
        }
    }

    fn deallocate_frame(&mut self, (myloc, childloc): Self::FrameLocation) {
        match myloc {
            false => self.left.deallocate_frame(childloc),
            true => self.right.deallocate_frame(childloc),
        }
        self.status = BuddyStatus::Partial;
    }
}

// 2^15, so 32768 possible frames
type BBBBuddies = Buddies<
    Buddies<
        Buddies<
            Buddies<
                Buddies<
                    Buddies<
                        Buddies<
                            Buddies<
                                Buddies<
                                    Buddies<
                                        Buddies<Buddies<Buddies<Buddies<Buddies<FrameStatus>>>>>,
                                    >,
                                >,
                            >,
                        >,
                    >,
                >,
            >,
        >,
    >,
>;

#[derive(Debug)]
pub struct BuddyAllocator {
    start_frame: PhysFrame,
    end_frame: PhysFrame, // exclusive
    bbbbuddies: BBBBuddies,
}

impl BuddyAllocator {
    pub fn from_mem_regions(regions: &info::MemoryRegions) -> Self {
        // currently only uses the first usable region, ignoring the rest
        let usable_region: &info::MemoryRegion = regions
            .iter()
            .find(|region| region.kind == info::MemoryRegionKind::Usable)
            .expect("Bootloader did not return a usable memory region");
        let start_addr = PhysAddr::new(usable_region.start);
        let end_addr = PhysAddr::new(usable_region.end); // exclusive

        serial_println!("creating buddy frame allocator");
        serial_println!("start address: {start_addr:?}, end address: {end_addr:?}");
        Self {
            start_frame: frame::PhysFrame::from_start_address(start_addr).unwrap(),
            end_frame: frame::PhysFrame::from_start_address(end_addr).unwrap(),
            bbbbuddies: BBBBuddies::new(),
        }
    }
}

unsafe impl FrameAllocator<Size4KiB> for BuddyAllocator {
    fn allocate_frame(&mut self) -> Option<PhysFrame> {
        let loc = self.bbbbuddies.allocate_frame();
        let offset = BBBBuddies::location_to_u64(loc, 0);

        let ret = self.start_frame + offset;

        if ret >= self.end_frame {
            self.bbbbuddies.deallocate_frame(loc);
            None
        } else {
            // serial_println!("allocating frame {ret:?}");
            Some(ret)
        }
    }
}

impl FrameDeallocator<Size4KiB> for BuddyAllocator {
    unsafe fn deallocate_frame(&mut self, f: PhysFrame) {
        let offset: u64 = f - self.start_frame;
        let (loc, residue) = BBBBuddies::u64_to_location(offset);
        if residue != 0 {
            panic!();
        }
        // serial_println!("deallocating frame {f:?}");
        self.bbbbuddies.deallocate_frame(loc);
    }
}

pub type FrameAllocType = BuddyAllocator;
pub static FRAME_ALLOC: SharedResource<FrameAllocType> = SharedResource::new();

pub fn init(regions: &info::MemoryRegions) {
    FRAME_ALLOC.init(FrameAllocType::from_mem_regions(regions));
}
