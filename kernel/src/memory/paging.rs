use crate::serial_println;

use super::layout;
use crate::shared_resource::{Once, SharedResource};
use alloc::boxed::Box;
use x86_64::registers::control::{Cr3, Cr3Flags};
use x86_64::structures::paging::{
    page_table::PageTableEntry, Page, PageTable, PageTableFlags as Flags, PageTableIndex,
    PhysFrame, RecursivePageTable, Size4KiB, Translate,
};
use x86_64::{PhysAddr, VirtAddr};

pub fn active_p4_virtaddr() -> VirtAddr {
    Page::from_page_table_indices(
        recursive_index(),
        recursive_index(),
        recursive_index(),
        recursive_index(),
    )
    .start_address()
}

pub static MAPPER: SharedResource<MapperType> = SharedResource::new();
static RECURSIVE_INDEX: Once<PageTableIndex> = Once::new();

pub fn recursive_index() -> PageTableIndex {
    *RECURSIVE_INDEX.get().unwrap()
}

pub fn init(index: u16) {
    RECURSIVE_INDEX.set(PageTableIndex::new(index)).unwrap();
    let p4_ptr = active_p4_virtaddr().as_u64() as *mut PageTable;
    MAPPER.init(RecursivePageTable::new(unsafe { &mut *p4_ptr }).unwrap());
}

// Copies over the top entries from the currently active page table
// !!Not yet recursively mapped!!
pub fn new_l4_pagetable() -> PageTable {
    let mut mapper = MAPPER.get();
    let current_p4 = mapper.level_4_table();
    let mut ret = PageTable::new();
    let kernel_start_index =
        Page::<Size4KiB>::from_start_address(VirtAddr::new(layout::KERNEL_MEMORY_START))
            .unwrap()
            .p4_index();
    for i in kernel_start_index.into()..512 {
        if i != recursive_index().into() {
            ret[i] = current_p4[i].clone();
        }
    }
    ret
}

pub type MapperType = RecursivePageTable<'static>;

pub fn recursive_map_pagetable(pt: &mut PageTable, mapper: &MapperType) {
    let phys_addr = mapper
        .translate_addr(VirtAddr::from_ptr(pt as *mut PageTable))
        .unwrap();
    let flags = Flags::PRESENT | Flags::WRITABLE;
    PageTableEntry::set_addr(&mut pt[recursive_index()], phys_addr, flags);
}

/// # Safety
/// The pagetable must be recursively mapped, and must not be moved nor mutated by anyone
/// while it is active, as the virtual address active_p4_virtaddr() is mutably referenced by
/// MAPPER.
pub unsafe fn activate_pagetable(pt: &mut PageTable) {
    set_cr3(pt[recursive_index()].addr());
}

/// # Safety
/// Must be the physical address of a recursively mapped page table, which must not move while it
/// is active.
unsafe fn set_cr3(addr: PhysAddr) {
    let phys_frame: PhysFrame<Size4KiB> = PhysFrame::from_start_address(addr).unwrap();
    unsafe {
        Cr3::write(phys_frame, Cr3Flags::empty());
    }
}

pub fn test() {
    walk_through_virtual_memory();

    // We need to save the physical address of the old pagetable since otherwise we cannot
    // return to it (but do we need to?)
    let old_p4_phys = super::MAPPER
        .get()
        .translate_addr(active_p4_virtaddr())
        .unwrap();

    let mut new_p4 = Box::new(new_l4_pagetable());
    recursive_map_pagetable(&mut new_p4, &super::MAPPER.get());
    unsafe { activate_pagetable(&mut new_p4) };
    serial_println!("new pagetable active");
    walk_through_virtual_memory();

    unsafe { set_cr3(old_p4_phys) };
    serial_println!("old pagetable restored");
    walk_through_virtual_memory();
}

// check which pages are mapped how
fn walk_through_virtual_memory() {
    serial_println!("walking through virtual memory");
    let mut current_range_start = None;

    let mut mapper = MAPPER.get();
    let p4 = mapper.level_4_table();
    for (p4_index, p4_entry) in p4.iter().enumerate() {
        let p4_index = PageTableIndex::new(p4_index as u16);
        if p4_entry.is_unused() {
            if let Some(page) = current_range_start {
                /*serial_println!(
                    "range started at {page:?} ended at {:?} in p4",
                    Page::from_page_table_indices(
                        p4_index,
                        PageTableIndex::new(0),
                        PageTableIndex::new(0),
                        PageTableIndex::new(0)
                    )
                );*/
                current_range_start = None;
            }
            continue;
        }
        let p3_page = Page::from_page_table_indices(
            recursive_index(),
            recursive_index(),
            recursive_index(),
            p4_index,
        );
        let p3 = unsafe { &*(p3_page.start_address().as_u64() as *const PageTable) };
        for (p3_index, p3_entry) in p3.iter().enumerate() {
            let p3_index = PageTableIndex::new(p3_index as u16);
            if p3_entry.is_unused() {
                if let Some(page) = current_range_start {
                    /*serial_println!(
                        "range started at {page:?} ended at {:?} in p3",
                        Page::from_page_table_indices(
                            p4_index,
                            p3_index,
                            PageTableIndex::new(0),
                            PageTableIndex::new(0)
                        )
                    );*/
                    current_range_start = None;
                }
                continue;
            }
            let p2_page = Page::from_page_table_indices(
                recursive_index(),
                recursive_index(),
                p4_index,
                p3_index,
            );
            let p2 = unsafe { &*(p2_page.start_address().as_u64() as *const PageTable) };
            for (p2_index, p2_entry) in p2.iter().enumerate() {
                let p2_index = PageTableIndex::new(p2_index as u16);
                if p2_entry.is_unused() {
                    if let Some(page) = current_range_start {
                        /*serial_println!(
                            "range started at {page:?} ended at {:?} in p2",
                            Page::from_page_table_indices(
                                p4_index,
                                p3_index,
                                p2_index,
                                PageTableIndex::new(0)
                            )
                        );*/
                        current_range_start = None;
                    }
                    continue;
                }
                let p1_page =
                    Page::from_page_table_indices(recursive_index(), p4_index, p3_index, p2_index);
                let p1 = unsafe { &*(p1_page.start_address().as_u64() as *const PageTable) };
                for (p1_index, p1_entry) in p1.iter().enumerate() {
                    let p1_index = PageTableIndex::new(p1_index as u16);
                    if p1_entry.is_unused() {
                        if let Some(page) = current_range_start {
                            /*serial_println!(
                                "range started at {page:?} ended at {:?} in p1",
                                Page::from_page_table_indices(
                                    p4_index, p3_index, p2_index, p1_index,
                                )
                            );*/
                            current_range_start = None;
                        }
                        continue;
                    }
                    if current_range_start.is_none() {
                        current_range_start = Some(Page::from_page_table_indices(
                            p4_index, p3_index, p2_index, p1_index,
                        ));
                    }
                }
            }
        }
    }
}
