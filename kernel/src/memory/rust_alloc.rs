use super::frame_alloc::FrameAllocType;
use super::paging::MapperType;
use crate::shared_resource::SharedResource;
use core::alloc::{GlobalAlloc, Layout};
use x86_64::structures::paging::{FrameAllocator, Mapper, Page, PageTableFlags as Flags, Size4KiB};
use x86_64::VirtAddr;

#[global_allocator]
pub static RUST_ALLOC: SharedResource<DumbAllocator> = SharedResource::new();

pub fn init() {
    RUST_ALLOC.init(DumbAllocator::new(
        super::layout::HEAP_START,
        super::layout::HEAP_END,
        &mut super::MAPPER.get(),
        &mut super::FRAME_ALLOC.get(),
    ))
}

#[derive(Debug)]
pub struct DumbAllocator {
    /// Next available address
    current: VirtAddr,

    /// Maximum allocatable address (exclusive)
    limit: VirtAddr,
}

impl DumbAllocator {
    pub fn new(
        start: u64,
        end: u64,
        mapper: &mut MapperType,
        frame_allocator: &mut FrameAllocType,
    ) -> Self {
        let current = VirtAddr::new(start);
        let limit = VirtAddr::new(end);
        let start_page: Page<Size4KiB> = Page::from_start_address(current).unwrap();
        let end_page = Page::from_start_address(limit).unwrap();
        let page_range = Page::range(start_page, end_page);
        for page in page_range {
            unsafe {
                mapper
                    .map_to(
                        page,
                        frame_allocator.allocate_frame().unwrap(),
                        Flags::PRESENT | Flags::WRITABLE,
                        frame_allocator,
                    )
                    .unwrap()
                    .flush();
            }
        }
        Self { current, limit }
    }
}

/// # Safety
/// Same requirements as [core::alloc::GlobalAlloc].
trait MutAllocator {
    fn alloc(&mut self, layout: Layout) -> *mut u8;
    fn dealloc(&mut self, ptr: *mut u8, layout: Layout);
}

impl MutAllocator for DumbAllocator {
    fn alloc(&mut self, layout: Layout) -> *mut u8 {
        // serial_println!("allocating {layout:?}");
        let aligned = self.current.align_up(layout.align() as u64);
        let top = aligned + layout.size();
        let ret = if top > self.limit {
            core::ptr::null_mut()
        } else {
            self.current = top;
            aligned.as_mut_ptr()
        };
        // serial_println!(" at address {ret:?}");
        ret
    }

    fn dealloc(&mut self, _: *mut u8, _: Layout) {
        // serial_println!("pretending to deallocate {p:?}");
    }
}

unsafe impl<T: MutAllocator + core::fmt::Debug> GlobalAlloc for SharedResource<T> {
    unsafe fn alloc(&self, layout: Layout) -> *mut u8 {
        self.get().alloc(layout)
    }
    unsafe fn dealloc(&self, ptr: *mut u8, layout: Layout) {
        self.get().dealloc(ptr, layout)
    }
}
