use bootloader_api::{config::Mapping, BootloaderConfig};

pub const KERNEL_STACK_SIZE: u64 = 0x10_0000; // 1 MiB, think
pub const KERNEL_STACK_START: u64 = 0xa000_0000_0000;
pub const BOOTINFO_START: u64 = 0xa100_0000_0000;
pub const FRAMEBUFFER_START: u64 = 0xa200_0000_0000;
pub const PAGETABLE_START: u64 = 0xa300_0000_0000;
pub const HEAP_START: u64 = 0xa400_0000_0000;
pub const HEAP_SIZE: u64 = 0x100_0000; // 1 MiB
pub const HEAP_END: u64 = HEAP_START + HEAP_SIZE;
pub const DYNAMIC_START: u64 = 0xc000_0000_0000;
pub const KERNEL_MEMORY_START: u64 = KERNEL_STACK_START;

pub static BOOTLOADER_CONFIG: BootloaderConfig = {
    let mut config = BootloaderConfig::new_default();
    config.kernel_stack_size = KERNEL_STACK_SIZE;
    config.mappings.kernel_stack = Mapping::FixedAddress(KERNEL_STACK_START);
    config.mappings.boot_info = Mapping::FixedAddress(BOOTINFO_START);
    config.mappings.framebuffer = Mapping::FixedAddress(FRAMEBUFFER_START);
    config.mappings.page_table_recursive = Some(Mapping::FixedAddress(PAGETABLE_START));
    // heap is at 0xa500_0000_0000 (set in memory.rs)
    config.mappings.dynamic_range_start = Some(DYNAMIC_START);
    config
};
