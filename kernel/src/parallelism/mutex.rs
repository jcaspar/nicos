use super::scheduler::{register, unregister, yield_now};
use core::{
    cell::UnsafeCell,
    ops::{Deref, DerefMut},
    sync::atomic::{AtomicBool, Ordering},
};
use futures_util::task::AtomicWaker;

pub struct Mutex<T> {
    waker: AtomicWaker,
    locked: AtomicBool,
    inner: UnsafeCell<T>,
}

impl<T> Mutex<T> {
    pub fn new(inner: T) -> Mutex<T> {
        Self {
            waker: AtomicWaker::new(),
            locked: AtomicBool::new(false),
            inner: UnsafeCell::new(inner),
        }
    }

    pub fn lock<'a>(&'a self) -> MutexGuard<'a, T> {
        loop {
            if let Some(lock) = self.try_lock() {
                return lock;
            }

            register(&self.waker).unwrap();
            if let Some(lock) = self.try_lock() {
                unregister(&self.waker);
                return lock;
            } else {
                yield_now();
            }
        }
    }

    pub fn try_lock<'a>(&'a self) -> Option<MutexGuard<'a, T>> {
        if self.locked.swap(true, Ordering::SeqCst) {
            None
        } else {
            Some(MutexGuard { mutex: self })
        }
    }

    pub unsafe fn force_unlock(&self) {
        self.locked.store(false, Ordering::SeqCst);
    }
}

pub struct MutexGuard<'a, T> {
    mutex: &'a Mutex<T>,
}

impl<'a, T> MutexGuard<'a, T> {
    fn new(mutex: &'a Mutex<T>) -> Self {
        Self { mutex }
    }
}

impl<'a, T> Drop for MutexGuard<'a, T> {
    fn drop(&mut self) {
        self.mutex.locked.store(false, Ordering::SeqCst);
        self.mutex.waker.wake();
    }
}

unsafe impl<T> Send for Mutex<T> {}
unsafe impl<T> Sync for Mutex<T> {}

impl<'a, T> Deref for MutexGuard<'a, T> {
    type Target = T;

    fn deref(&self) -> &Self::Target {
        // SAFETY: We need to ensure that when we transform the raw pointer into a borrow, no exclusive borrow
        // to the underlying data exists, and no mutation happens.
        // This is guaranteed because the only way to access `inner` is through a `LockGuard`, of which there
        // is at most one at all time. Furthermore, we have a borrow of the `LockGuard`, which means we cannot
        // also `DerefMut` it.
        unsafe { &*self.mutex.inner.get() }
    }
}

impl<'a, T> DerefMut for MutexGuard<'a, T> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        // SAFETY: See the safety block of `Deref for LockGuard`. We also need to ensure here that the pointer
        // is unaliased, which is guaranteed (again) because the borrow to `self` is exclusive and that borrow
        // is the only way to access the underlying data.
        unsafe { &mut *self.mutex.inner.get() }
    }
}
