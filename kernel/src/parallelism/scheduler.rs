use core::{
    sync::atomic::{AtomicBool, AtomicU64, Ordering},
    task::Waker,
};

use alloc::{collections::BTreeMap, sync::Arc, task::Wake};
use crossbeam_queue::SegQueue;
use futures_util::task::AtomicWaker;
use x86_64::instructions::{self, interrupts};

use crate::{
    error::resource,
    prelude::*,
    process::{PId, ProcessControlBlock, KERNEL},
};

static COUNT: AtomicU64 = AtomicU64::new(0);
static RESCHEDULE: AtomicBool = AtomicBool::new(true);

pub(super) fn count() -> u64 {
    COUNT.load(Ordering::SeqCst)
}

/// Fake the effect of a yield. This will effectively blocks until the scheduler interrupts
/// the task, and resumes when the scheduler reschedules the task.
///
/// This is useful if the current process is waiting for a resource to be available, it has
/// already notified that it should be reschduled when the resource is available, and now it
/// should just wait to be rescheduled.
pub(crate) fn yield_now() {
    let cnt = count();
    while count() == cnt {
        instructions::hlt();
    }
}

/// Hint the scheduler not to reschedule the current process, as it's waiting for some resource
/// to be available. The process takes the responsability of ensuring the scheduler will be
/// notified when the resource is available.
///
/// IMPORTANT: to avoid a data race, you should setup the notification *before* calling this
/// function, otherwise the scheduler might interrupt you in between, and the process will never
/// be resumed.
/// For this reason, you might want to you [`register`] instead.
pub(crate) fn dont_reschedule() {
    RESCHEDULE.store(false, Ordering::SeqCst);
}

/// Hint the scheduler to reschedule the current process. This is the default behavior, and is
/// only useful if you are trying to "undo" a resource-based rescheduling notification, as it
/// might happen that while you were informing the scheduler that a process was waiting for a
/// resource, that resource became available (see the implementation of `Mutex` as an example).
///
/// IMPORTANT: to avoid a data race, you should clear the notification *after* calling this
/// function, otherwise the scheduler might interrupt you in between, and the process will never
/// be resumed.
/// For this reason, you might want to use [`unregister`] instead.
pub(crate) fn do_reschedule() {
    RESCHEDULE.store(true, Ordering::SeqCst);
}

/// Mark the current process as waiting for a resource to be available, and register that
/// resource's `waker` to the current process, so that it will be resumed once the resource is
/// available.
///
/// Note that the process should not rely on the resource being available once it has been
/// resumed: if multiple process wait for the same resource to be available, they might all be
/// awaken when the resource becomes available, but the first to be actually executed might
/// consume the resource so it becomes unavailable again for the other processes.
///
/// Also note that this will not (yet?) suspend the current process. If you want to wait until
/// the process is actually rescheduled, you should also call [`yield_now`].
pub(crate) fn register(waker: &AtomicWaker) -> Result<()> {
    waker.register(
        KERNEL
            .try_lock()
            .ok_or_else(resource!())?
            .scheduler()
            .current_waker(),
    );
    dont_reschedule();
    Ok(())
}

/// Mark the current process as not waiting anymore for a resource to be available, thus
/// forgetting the hook that was previously setup on the resource's waker.
pub(crate) fn unregister(waker: &AtomicWaker) {
    do_reschedule();
    waker.take();
}

#[derive(Debug)]
pub struct Scheduler {
    process_queue: Arc<SegQueue<PId>>,
    processes: BTreeMap<PId, (ProcessControlBlock, Waker)>,
    curr_process: AtomicU64,
}

impl Scheduler {
    pub const TICKS_PER_PROC: usize = 1;

    pub fn new() -> Self {
        Self {
            process_queue: Arc::new(SegQueue::new()),
            processes: BTreeMap::new(),
            curr_process: AtomicU64::new(1),
        }
    }

    pub fn processes(&self) -> impl Iterator<Item = &ProcessControlBlock> {
        self.processes.values().map(|(p, _)| p)
    }

    pub fn processes_mut(&mut self) -> impl Iterator<Item = &mut ProcessControlBlock> {
        self.processes.values_mut().map(|(p, _)| p)
    }

    pub fn get_proc(&self, pid: PId) -> Option<&ProcessControlBlock> {
        self.processes.get(&pid).map(|x| &x.0)
    }

    pub fn get_proc_mut(&mut self, pid: PId) -> Option<&mut ProcessControlBlock> {
        self.processes.get_mut(&pid).map(|x| &mut x.0)
    }

    pub fn current_proc_mut(&mut self) -> &mut ProcessControlBlock {
        self.get_proc_mut(self.current_pid()).unwrap()
    }

    pub fn current_proc(&self) -> &ProcessControlBlock {
        self.get_proc(self.current_pid()).unwrap()
    }

    pub fn spawn(&mut self, proc: ProcessControlBlock) {
        let waker = ProcWaker::new_waker(proc.pid(), self.process_queue.clone());
        self.process_queue.push(proc.pid());
        self.processes.insert(proc.pid(), (proc, waker));
    }

    pub fn current_pid(&self) -> PId {
        PId::new(self.curr_process.load(Ordering::SeqCst))
    }

    pub fn current_waker(&self) -> &Waker {
        &self.processes.get(&self.current_pid()).unwrap().1
    }

    pub fn launch_next(&self) -> PId {
        COUNT.store(count() + 1, Ordering::SeqCst);

        if RESCHEDULE.load(Ordering::SeqCst) {
            self.process_queue.push(self.current_pid());
        }
        RESCHEDULE.store(true, Ordering::SeqCst);

        self.curr_process.store(
            loop {
                interrupts::disable();
                if let Some(pid) = self.process_queue.pop() {
                    break pid.into();
                }
                interrupts::enable_and_hlt();
            },
            Ordering::SeqCst,
        );
        interrupts::enable();

        self.current_pid()
    }

    pub fn exit_current_process(&mut self) {
        dont_reschedule();
        self.processes.remove(&self.current_pid());
    }
}

#[derive(Debug)]
struct ProcWaker {
    pid: PId,
    process_queue: Arc<SegQueue<PId>>,
}

impl ProcWaker {
    fn new_waker(pid: PId, process_queue: Arc<SegQueue<PId>>) -> Waker {
        let waker = Self { pid, process_queue };

        Waker::from(Arc::new(waker))
    }
}

impl Wake for ProcWaker {
    fn wake(self: Arc<Self>) {
        self.wake_by_ref()
    }

    fn wake_by_ref(self: &Arc<Self>) {
        self.process_queue.push(self.pid);
    }
}
