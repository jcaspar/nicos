use core::{
    fmt::{self, Display, Formatter},
    sync::atomic::{AtomicBool, Ordering},
};
use generic_once_cell::OnceCell;
use lock_api::{GuardSend, MappedMutexGuard, MutexGuard, RawMutex};
use x86_64::instructions::interrupts::without_interrupts;

use crate::{
    error::{Error, Here},
    prelude::*,
};

#[derive(Debug)]
pub struct SRError;

impl Display for SRError {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        write!(f, "The resource has already been acquired.")
    }
}

pub struct PanicRaw(AtomicBool);

unsafe impl RawMutex for PanicRaw {
    type GuardMarker = GuardSend;

    #[allow(clippy::declare_interior_mutable_const)]
    const INIT: Self = PanicRaw(AtomicBool::new(false));

    fn lock(&self) {
        if !self.try_lock() {
            panic!("tried to acquire locked shared resource");
        }
    }

    fn try_lock(&self) -> bool {
        self.0
            .compare_exchange(false, true, Ordering::Acquire, Ordering::Relaxed)
            .is_ok()
    }

    unsafe fn unlock(&self) {
        self.0.store(false, Ordering::Release);
    }
}

pub type PanicMutex<T> = lock_api::Mutex<PanicRaw, T>;

pub struct SharedResource<T>(PanicMutex<Option<T>>);

impl<T: core::fmt::Debug> SharedResource<T> {
    pub fn try_get(&self, here: impl FnOnce() -> Here) -> Result<MappedMutexGuard<PanicRaw, T>> {
        MutexGuard::try_map(self.0.lock(), |x| x.as_mut())
            .map_err(move |_| Error::here_with(SRError, here()))
    }

    pub fn get(&self) -> MappedMutexGuard<PanicRaw, T> {
        self.try_get(|| panic!("Resource already acquired"))
            .unwrap()
    }

    pub fn init(&self, val: T) {
        let mut content = self.0.lock();
        assert!(content.is_none(), "double init of SharedResource");
        *content = Some(val);
    }
    pub const fn new() -> Self {
        Self(PanicMutex::new(None))
    }

    pub unsafe fn force_get(&self) -> MappedMutexGuard<PanicRaw, T> {
        without_interrupts(|| {
            if self.0.is_locked() {
                self.0.force_unlock()
            }
            self.get()
        })
    }
}

pub type Once<T> = OnceCell<PanicRaw, T>;
