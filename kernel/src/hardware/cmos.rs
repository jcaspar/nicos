use x86_64::instructions::{interrupts::without_interrupts, port::Port};

use core::{
    prelude::rust_2021::*,
    sync::atomic::{AtomicI64, Ordering},
};

use super::io_wait;

/// io ports for CMOS
const CONTROL: u16 = 0x70;
const DATA: u16 = 0x71;

/// unix timestamp
#[repr(transparent)]
#[derive(Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct Time(pub i64);

pub struct CMOS {
    control: Port<u8>,
    data: Port<u8>,
    time: AtomicI64,
}

impl CMOS {
    pub(super) const unsafe fn new() -> Self {
        Self {
            control: Port::new(CONTROL),
            data: Port::new(DATA),
            time: AtomicI64::new(0),
        }
    }

    /// SAFETY : interrupts must be disabled
    pub(super) unsafe fn init(&mut self) {
        self.enable_periodic_interrupts();

        unsafe {
            self.read_register(0x0C);
        }
    }

    /// SAFETY : reg should be a valid CMOS register address,
    /// and the interrupts should be disabled
    pub(super) unsafe fn read_register(&mut self, reg: u8) -> u8 {
        // bit 1 << 7 disable NMIs
        self.control.write(reg | (1 << 7));
        io_wait();
        let ret = self.data.read();
        ret
    }

    /// SAFETY : reg should be a valid CMOS register address,
    /// and the interrupts should be disabled
    pub(super) unsafe fn write_register(&mut self, reg: u8, value: u8) {
        self.control.write(reg | (1 << 7));
        io_wait();
        self.data.write(value);
        self.control.write(0);
    }

    /// SAFETY : the interrupts should be disabled
    /// and because the RTC may be updated between the calls
    /// it is the responsability of the caller to handle this case
    /// return None if the value are inconsistent
    pub(super) unsafe fn get_current_date(&mut self) -> Option<Time> {
        fn from_bcd(x: u8) -> u8 {
            (x & 0xF) + 10 * (x >> 4)
        }
        let second = from_bcd(unsafe { self.read_register(0x00) });
        let minute = from_bcd(unsafe { self.read_register(0x02) });
        let hour = from_bcd(unsafe { self.read_register(0x04) });
        let day_of_month = from_bcd(unsafe { self.read_register(0x07) });
        let month = from_bcd(unsafe { self.read_register(0x08) });
        let year = from_bcd(unsafe { self.read_register(0x09) }) as i32;
        let century = from_bcd(unsafe { self.read_register(0x32) }) as i32;

        let month = match month {
            1 => time::Month::January,
            2 => time::Month::February,
            3 => time::Month::March,
            4 => time::Month::April,
            5 => time::Month::May,
            6 => time::Month::June,
            7 => time::Month::July,
            8 => time::Month::August,
            9 => time::Month::September,
            10 => time::Month::October,
            11 => time::Month::November,
            _ => time::Month::December,
        };

        let date =
            time::Date::from_calendar_date(year + 100 * century, month, day_of_month).ok()?;
        let time = time::Time::from_hms(hour, minute, second).ok()?;

        Some(Time(
            time::PrimitiveDateTime::new(date, time)
                .assume_utc()
                .unix_timestamp(),
        ))
    }

    pub(super) fn enable_periodic_interrupts(&mut self) {
        without_interrupts(|| unsafe {
            self.control.write(0xB | (1 << 7));
            let prev = self.data.read();
            // read reset the control register
            self.control.write(0xB | (1 << 7));
            self.data.write(prev | 0x40); // bit 6 of register B is on
        })
    }

    pub(super) fn disable_periodic_interrupts(&mut self) {
        without_interrupts(|| unsafe {
            self.control.write(0xB | (1 << 7));
            let prev = self.data.read();
            // read reset the control register
            self.control.write(0xB | (1 << 7));
            self.data.write(prev & !0x40); // bit 6 of register B is on
        })
    }

    pub fn read_time(&self) -> Time {
        Time(self.time.load(Ordering::SeqCst))
    }

    pub fn set_time(&mut self, time: Time) {
        self.time.store(time.0, Ordering::SeqCst);
    }
}
