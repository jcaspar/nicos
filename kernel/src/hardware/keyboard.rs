use core::fmt::{self, Display, Formatter};

use x86_64::instructions::port::Port;

// io ports
const DATA: u16 = 0x60;
const CONTROL: u16 = 0x64;

/// the names are the ones of my keyboard (azerty)
#[repr(u8)]
#[derive(Debug, Copy, Clone)]
pub enum KeyCode {
    Escape = 1,
    One,
    Two,
    Three,
    Four,
    Five,
    Six,
    Seven,
    Eight,
    Nine,
    Zero,
    Degree,
    Equal,
    Backspace,
    Tab,
    A,
    Z,
    E,
    R,
    T,
    Y,
    U,
    I,
    O,
    P,
    Circumflex,
    Dollar,
    Enter,
    LControl,
    Q,
    S,
    D,
    F,
    G,
    H,
    J,
    K,
    L,
    M,
    Percent,
    PowerTwo,
    LShift,
    Asterisk,
    W,
    X,
    C,
    V,
    B,
    N,
    Comma,
    Dot,
    Slash,
    Exclamation,
    RShift,
    KeypadAsterisk,
    Alt,
    Space,
    CapsLock,
    F1,
    F2,
    F3,
    F4,
    F5,
    F6,
    F7,
    F8,
    F9,
    F10,
    NumberLock,
    ScrollLock,
    KeypadSeven,
    KeypadEight,
    KeypadNine,
    KeypadMinus,
    KeypadFour,
    KeypadFive,
    KeypadSix,
    KeypadPlus,
    KeypadOne,
    KeypadTwo,
    KeypadThree,
    KeypadZero,
    KeypadDot,
    // skip some values
    Lt = 0x56,
    F11,
    F12,
    // extended keymap (second scancode | 0x80)
    KeypadEnter = 0x1C | 0x80,
    RControl = 0x1D | 0x80,
    KeypadSlash = 0x35 | 0x80,
    AltGr = 0x38 | 0x80,
    Home = 0x47 | 0x80,
    PageUp,
    CursorLeft = 0x4b | 0x80,
    CursorRight = 0x4d | 0x80,
    EndPressed = 0x4f | 0x80,
    CursordDown,
    PageDown,
    Insert,
    Delete,
}

struct KeyState {
    pub code: KeyCode,
    pub pressed: bool,
}

#[derive(Clone, Copy)]
struct Modifiers {
    lshift: bool,
    rshift: bool,
    lcontrol: bool,
    rcontrol: bool,
    caps_lock: bool,
    alt: bool,
    alt_gr: bool,
}

#[derive(Debug, Copy, Clone)]
pub enum DecodedKey {
    SpecialKey(KeyCode),
    Decoded(u8), // ascii char
}

#[derive(Debug, Clone, Copy)]
pub struct EventModifiers {
    pub shift: bool,
    pub control: bool,
    pub caps_lock: bool,
    pub alt: bool,
    pub alt_gr: bool,
}

impl From<Modifiers> for EventModifiers {
    fn from(m: Modifiers) -> Self {
        EventModifiers {
            shift: m.lshift | m.rshift,
            control: m.lcontrol | m.rcontrol,
            caps_lock: m.caps_lock,
            alt: m.alt,
            alt_gr: m.alt_gr,
        }
    }
}

#[derive(Debug, Clone, Copy)]
pub struct KeyEvent {
    pub key: DecodedKey,
    pub is_pressed: bool,
    pub modifiers: EventModifiers,
}

impl Display for KeyEvent {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        if !self.is_pressed {
            write!(f, "^")?;
        }
        if self.modifiers.control {
            write!(f, "C-")?;
        }
        if self.modifiers.alt {
            write!(f, "M-")?;
        }
        if self.modifiers.shift {
            write!(f, "S-")?;
        }
        match self.key {
            DecodedKey::Decoded(c) => write!(f, "{}", c as char)?,
            DecodedKey::SpecialKey(key) => write!(f, "{key:?}")?,
        }

        Ok(())
    }
}

#[derive(Clone, Copy)]
enum ScanCodeState {
    Initial,
    ExtendedByte,
}

pub enum Layout {
    Azerty,
}

pub struct Keyboard {
    data: Port<u8>,
    control: Port<u8>,
    modifiers: Modifiers,
    state: ScanCodeState,
    layout: Layout,
    buffer: [u8; 32],
}

impl Keyboard {
    pub const unsafe fn new() -> Self {
        Keyboard {
            data: Port::new(DATA),
            control: Port::new(CONTROL),
            modifiers: Modifiers {
                lshift: false,
                rshift: false,
                lcontrol: false,
                rcontrol: false,
                caps_lock: false,
                alt: false,
                alt_gr: false,
            },
            state: ScanCodeState::Initial,
            layout: Layout::Azerty,
            buffer: [0; 32],
        }
    }

    fn set_led(&mut self) {}

    pub unsafe fn read(&mut self) -> Option<KeyEvent> {
        let scan_code = self.data.read();
        if scan_code == 0xe0 {
            self.state = ScanCodeState::ExtendedByte;
            None
        } else {
            Self::translate_scan_code_to_key_code(scan_code, self.state)
                .and_then(|key_state| self.translate_key_code_to_event(key_state))
        }
    }

    pub unsafe fn init(&mut self) {}

    pub fn set_layout(&mut self, layout: Layout) {
        self.layout = layout;
    }

    fn translate_scan_code_to_key_code(scan_code: u8, extended: ScanCodeState) -> Option<KeyState> {
        let pressed = scan_code & 0x80 == 0;
        let scan_code = scan_code & 0x7F;
        let code = if let ScanCodeState::Initial = extended {
            if ((KeyCode::KeypadDot as u8 + 1)..(KeyCode::Lt as u8)).contains(&scan_code) {
                panic!("Unknown scan code {}", scan_code);
            }
            unsafe { (&scan_code as *const u8 as *const KeyCode).read() }
        } else {
            if [
                0x1C, 0x1D, 0x35, 0x38, 0x47, 0x48, 0x4b, 0x4d, 0x4f, 0x50, 0x51, 0x52, 0x53,
            ]
            .contains(&scan_code)
            {
                unsafe { (&(scan_code | 0x80) as *const u8 as *const KeyCode).read() }
            } else {
                return None;
            }
        };
        Some(KeyState { code, pressed })
    }

    fn translate_key_code_to_event(&mut self, key_state: KeyState) -> Option<KeyEvent> {
        match self.layout {
            Layout::Azerty => {
                let is_pressed = key_state.pressed;
                let maj =
                    (self.modifiers.lshift | self.modifiers.rshift) ^ self.modifiers.caps_lock;
                let alt_gr = self.modifiers.alt_gr;
                let key = match key_state.code {
                    KeyCode::LShift => {
                        self.modifiers.lshift = is_pressed;
                        DecodedKey::SpecialKey(key_state.code)
                    }
                    KeyCode::RShift => {
                        self.modifiers.rshift = is_pressed;
                        DecodedKey::SpecialKey(key_state.code)
                    }
                    KeyCode::LControl => {
                        self.modifiers.lcontrol = is_pressed;
                        DecodedKey::SpecialKey(key_state.code)
                    }
                    KeyCode::RControl => {
                        self.modifiers.rcontrol = is_pressed;
                        DecodedKey::SpecialKey(key_state.code)
                    }
                    KeyCode::Alt => {
                        self.modifiers.alt = is_pressed;
                        DecodedKey::SpecialKey(key_state.code)
                    }
                    KeyCode::AltGr => {
                        self.modifiers.alt_gr = is_pressed;
                        DecodedKey::SpecialKey(key_state.code)
                    }
                    KeyCode::ScrollLock | KeyCode::NumberLock | KeyCode::CapsLock => {
                        self.modifiers.caps_lock = !self.modifiers.caps_lock;
                        self.set_led();
                        DecodedKey::SpecialKey(key_state.code)
                    }
                    KeyCode::Escape
                    | KeyCode::Backspace
                    | KeyCode::F1
                    | KeyCode::F2
                    | KeyCode::F3
                    | KeyCode::F4
                    | KeyCode::F5
                    | KeyCode::F6
                    | KeyCode::F7
                    | KeyCode::F8
                    | KeyCode::F9
                    | KeyCode::F10
                    | KeyCode::F11
                    | KeyCode::F12
                    | KeyCode::Home
                    | KeyCode::PageUp
                    | KeyCode::CursorLeft
                    | KeyCode::CursorRight
                    | KeyCode::EndPressed
                    | KeyCode::CursordDown
                    | KeyCode::PageDown
                    | KeyCode::Insert
                    | KeyCode::Delete => DecodedKey::SpecialKey(key_state.code),
                    KeyCode::One => {
                        if maj {
                            DecodedKey::Decoded('1' as u8)
                        } else {
                            DecodedKey::Decoded('&' as u8)
                        }
                    }
                    KeyCode::Two => {
                        if alt_gr {
                            DecodedKey::Decoded('~' as u8)
                        } else if maj {
                            DecodedKey::Decoded('2' as u8)
                        } else {
                            return None;
                        }
                    }
                    KeyCode::Three => {
                        if alt_gr {
                            DecodedKey::Decoded('#' as u8)
                        } else if maj {
                            DecodedKey::Decoded('3' as u8)
                        } else {
                            DecodedKey::Decoded('"' as u8)
                        }
                    }
                    KeyCode::Four => {
                        if alt_gr {
                            DecodedKey::Decoded('{' as u8)
                        } else if maj {
                            DecodedKey::Decoded('4' as u8)
                        } else {
                            DecodedKey::Decoded('\'' as u8)
                        }
                    }
                    KeyCode::Five => {
                        if alt_gr {
                            DecodedKey::Decoded('[' as u8)
                        } else if maj {
                            DecodedKey::Decoded('5' as u8)
                        } else {
                            DecodedKey::Decoded('(' as u8)
                        }
                    }
                    KeyCode::Six => {
                        if alt_gr {
                            DecodedKey::Decoded('|' as u8)
                        } else if maj {
                            DecodedKey::Decoded('6' as u8)
                        } else {
                            DecodedKey::Decoded('-' as u8)
                        }
                    }
                    KeyCode::Seven => {
                        if alt_gr {
                            DecodedKey::Decoded('`' as u8)
                        } else if maj {
                            DecodedKey::Decoded('7' as u8)
                        } else {
                            DecodedKey::Decoded('è' as u8)
                        }
                    }
                    KeyCode::Eight => {
                        if alt_gr {
                            DecodedKey::Decoded('\\' as u8)
                        } else if maj {
                            DecodedKey::Decoded('8' as u8)
                        } else {
                            DecodedKey::Decoded('_' as u8)
                        }
                    }
                    KeyCode::Nine => {
                        if alt_gr {
                            DecodedKey::Decoded('^' as u8)
                        } else if maj {
                            DecodedKey::Decoded('9' as u8)
                        } else {
                            DecodedKey::Decoded('ç' as u8)
                        }
                    }
                    KeyCode::Zero => {
                        if alt_gr {
                            DecodedKey::Decoded('@' as u8)
                        } else if maj {
                            DecodedKey::Decoded('0' as u8)
                        } else {
                            DecodedKey::Decoded('à' as u8)
                        }
                    }
                    KeyCode::Degree => {
                        if alt_gr {
                            DecodedKey::Decoded(']' as u8)
                        } else if maj {
                            DecodedKey::Decoded('°' as u8)
                        } else {
                            DecodedKey::Decoded(')' as u8)
                        }
                    }
                    KeyCode::Equal => {
                        if alt_gr {
                            DecodedKey::Decoded('}' as u8)
                        } else if maj {
                            DecodedKey::Decoded('+' as u8)
                        } else {
                            DecodedKey::Decoded('=' as u8)
                        }
                    }
                    KeyCode::Tab => DecodedKey::Decoded('\t' as u8),
                    KeyCode::A => {
                        if maj {
                            DecodedKey::Decoded('A' as u8)
                        } else {
                            DecodedKey::Decoded('a' as u8)
                        }
                    }
                    KeyCode::Z => {
                        if maj {
                            DecodedKey::Decoded('Z' as u8)
                        } else {
                            DecodedKey::Decoded('z' as u8)
                        }
                    }
                    KeyCode::E => {
                        if alt_gr {
                            DecodedKey::Decoded('€' as u8)
                        } else if maj {
                            DecodedKey::Decoded('E' as u8)
                        } else {
                            DecodedKey::Decoded('e' as u8)
                        }
                    }
                    KeyCode::R => {
                        if maj {
                            DecodedKey::Decoded('R' as u8)
                        } else {
                            DecodedKey::Decoded('r' as u8)
                        }
                    }
                    KeyCode::T => {
                        if maj {
                            DecodedKey::Decoded('T' as u8)
                        } else {
                            DecodedKey::Decoded('t' as u8)
                        }
                    }
                    KeyCode::Y => {
                        if maj {
                            DecodedKey::Decoded('Y' as u8)
                        } else {
                            DecodedKey::Decoded('y' as u8)
                        }
                    }
                    KeyCode::U => {
                        if maj {
                            DecodedKey::Decoded('U' as u8)
                        } else {
                            DecodedKey::Decoded('u' as u8)
                        }
                    }
                    KeyCode::I => {
                        if maj {
                            DecodedKey::Decoded('I' as u8)
                        } else {
                            DecodedKey::Decoded('i' as u8)
                        }
                    }
                    KeyCode::O => {
                        if maj {
                            DecodedKey::Decoded('O' as u8)
                        } else {
                            DecodedKey::Decoded('o' as u8)
                        }
                    }
                    KeyCode::P => {
                        if maj {
                            DecodedKey::Decoded('P' as u8)
                        } else {
                            DecodedKey::Decoded('p' as u8)
                        }
                    }
                    KeyCode::Circumflex => {
                        if maj {
                            DecodedKey::Decoded('^' as u8)
                        } else {
                            DecodedKey::Decoded('¨' as u8)
                        }
                    }
                    KeyCode::Dollar => {
                        if alt_gr {
                            DecodedKey::Decoded('£' as u8)
                        } else if maj {
                            DecodedKey::Decoded('$' as u8)
                        } else {
                            DecodedKey::Decoded('¤' as u8)
                        }
                    }
                    KeyCode::Enter => DecodedKey::Decoded('\n' as u8),
                    KeyCode::Q => {
                        if maj {
                            DecodedKey::Decoded('Q' as u8)
                        } else {
                            DecodedKey::Decoded('q' as u8)
                        }
                    }
                    KeyCode::S => {
                        if maj {
                            DecodedKey::Decoded('S' as u8)
                        } else {
                            DecodedKey::Decoded('s' as u8)
                        }
                    }
                    KeyCode::D => {
                        if maj {
                            DecodedKey::Decoded('D' as u8)
                        } else {
                            DecodedKey::Decoded('d' as u8)
                        }
                    }
                    KeyCode::F => {
                        if maj {
                            DecodedKey::Decoded('F' as u8)
                        } else {
                            DecodedKey::Decoded('f' as u8)
                        }
                    }
                    KeyCode::G => {
                        if maj {
                            DecodedKey::Decoded('G' as u8)
                        } else {
                            DecodedKey::Decoded('g' as u8)
                        }
                    }
                    KeyCode::H => {
                        if maj {
                            DecodedKey::Decoded('H' as u8)
                        } else {
                            DecodedKey::Decoded('h' as u8)
                        }
                    }
                    KeyCode::J => {
                        if maj {
                            DecodedKey::Decoded('J' as u8)
                        } else {
                            DecodedKey::Decoded('j' as u8)
                        }
                    }
                    KeyCode::K => {
                        if maj {
                            DecodedKey::Decoded('K' as u8)
                        } else {
                            DecodedKey::Decoded('k' as u8)
                        }
                    }
                    KeyCode::L => {
                        if maj {
                            DecodedKey::Decoded('L' as u8)
                        } else {
                            DecodedKey::Decoded('l' as u8)
                        }
                    }
                    KeyCode::M => {
                        if maj {
                            DecodedKey::Decoded('M' as u8)
                        } else {
                            DecodedKey::Decoded('m' as u8)
                        }
                    }
                    KeyCode::Percent => {
                        if maj {
                            DecodedKey::Decoded('%' as u8)
                        } else {
                            DecodedKey::Decoded('ù' as u8)
                        }
                    }
                    KeyCode::PowerTwo => DecodedKey::Decoded('²' as u8),
                    KeyCode::Asterisk => {
                        if maj {
                            DecodedKey::Decoded('µ' as u8)
                        } else {
                            DecodedKey::Decoded('*' as u8)
                        }
                    }
                    KeyCode::W => {
                        if maj {
                            DecodedKey::Decoded('W' as u8)
                        } else {
                            DecodedKey::Decoded('w' as u8)
                        }
                    }
                    KeyCode::X => {
                        if maj {
                            DecodedKey::Decoded('X' as u8)
                        } else {
                            DecodedKey::Decoded('x' as u8)
                        }
                    }
                    KeyCode::C => {
                        if maj {
                            DecodedKey::Decoded('C' as u8)
                        } else {
                            DecodedKey::Decoded('c' as u8)
                        }
                    }
                    KeyCode::V => {
                        if maj {
                            DecodedKey::Decoded('V' as u8)
                        } else {
                            DecodedKey::Decoded('v' as u8)
                        }
                    }
                    KeyCode::B => {
                        if maj {
                            DecodedKey::Decoded('B' as u8)
                        } else {
                            DecodedKey::Decoded('b' as u8)
                        }
                    }
                    KeyCode::N => {
                        if maj {
                            DecodedKey::Decoded('N' as u8)
                        } else {
                            DecodedKey::Decoded('n' as u8)
                        }
                    }
                    KeyCode::Comma => {
                        if maj {
                            DecodedKey::Decoded('?' as u8)
                        } else {
                            DecodedKey::Decoded(',' as u8)
                        }
                    }
                    KeyCode::Dot => {
                        if maj {
                            DecodedKey::Decoded('.' as u8)
                        } else {
                            DecodedKey::Decoded(';' as u8)
                        }
                    }
                    KeyCode::Slash => {
                        if maj {
                            DecodedKey::Decoded('/' as u8)
                        } else {
                            DecodedKey::Decoded(':' as u8)
                        }
                    }
                    KeyCode::Exclamation => {
                        if maj {
                            DecodedKey::Decoded('§' as u8)
                        } else {
                            DecodedKey::Decoded('!' as u8)
                        }
                    }
                    KeyCode::Space => DecodedKey::Decoded(' ' as u8),
                    KeyCode::Lt => {
                        if maj {
                            DecodedKey::Decoded('>' as u8)
                        } else {
                            DecodedKey::Decoded('<' as u8)
                        }
                    }
                    KeyCode::KeypadAsterisk => DecodedKey::Decoded('*' as u8),
                    KeyCode::KeypadEnter => DecodedKey::Decoded('\n' as u8),
                    KeyCode::KeypadDot => DecodedKey::Decoded('.' as u8),
                    KeyCode::KeypadSeven => DecodedKey::Decoded('7' as u8),
                    KeyCode::KeypadEight => DecodedKey::Decoded('8' as u8),
                    KeyCode::KeypadNine => DecodedKey::Decoded('9' as u8),
                    KeyCode::KeypadMinus => DecodedKey::Decoded('-' as u8),
                    KeyCode::KeypadFour => DecodedKey::Decoded('4' as u8),
                    KeyCode::KeypadFive => DecodedKey::Decoded('5' as u8),
                    KeyCode::KeypadSix => DecodedKey::Decoded('6' as u8),
                    KeyCode::KeypadPlus => DecodedKey::Decoded('+' as u8),
                    KeyCode::KeypadOne => DecodedKey::Decoded('1' as u8),
                    KeyCode::KeypadTwo => DecodedKey::Decoded('2' as u8),
                    KeyCode::KeypadThree => DecodedKey::Decoded('3' as u8),
                    KeyCode::KeypadZero => DecodedKey::Decoded('0' as u8),
                    KeyCode::KeypadSlash => DecodedKey::Decoded('\\' as u8),
                };

                let modifiers = self.modifiers.into();
                Some(KeyEvent {
                    key,
                    is_pressed,
                    modifiers,
                })
            }
        }
    }
}
