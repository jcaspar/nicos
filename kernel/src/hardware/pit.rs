use x86_64::instructions::port::Port;

use alloc::{boxed::Box, collections::BinaryHeap};

const CHAN0: Port<u8> = Port::new(0x40);
const CHAN1: Port<u8> = Port::new(0x41);
const CHAN2: Port<u8> = Port::new(0x42);
const COMMAND: Port<u8> = Port::new(0x43);

const BASE: f64 = 1.193181666e6;
const MIN_FREQUENCY: f64 = BASE / (u16::MAX as f64 + 1.0);

pub struct PIT {
    channel_0: Port<u8>,
    channel_1: Port<u8>,
    channel_2: Port<u8>,
    command: Port<u8>,
    divider: u16,
    /// won't work after 5849424173.55072 year
    clock: usize,
    /// option because the new method is not const
    callbacks: Option<BinaryHeap<DateCallback>>,
}

impl PIT {
    pub const fn new() -> Self {
        Self {
            channel_0: CHAN0,
            channel_1: CHAN1,
            channel_2: CHAN2,
            command: COMMAND,
            divider: 1,
            clock: 0,
            callbacks: None,
        }
    }

    /// frequency, in Hz
    /// SAFETY: interrupts should be disabled
    pub(super) unsafe fn init(&mut self, frequency: f64) {
        // doesn't allocate, so we can call it even if the
        // memory is not initialized
        self.callbacks = Some(BinaryHeap::new());

        if frequency > BASE {
            panic!("Frequency too high, it should be at most {BASE} Hz");
        } else if frequency < MIN_FREQUENCY {
            panic!("Frequency too low, it should be at least {MIN_FREQUENCY} Hz");
        }
        let ratio = BASE / frequency;
        let divider = if ratio > u16::MAX as f64 + 0.5 {
            0
        } else {
            unsafe { ratio.to_int_unchecked::<u16>() }
        };
        self.divider = divider;

        // channel 0, lobyte/hibyte, mode 2
        let cmd = 0b00110100;
        self.command.write(cmd);

        // write divider
        let data = (self.divider & 0xFF) as u8;
        self.channel_0.write(data);

        let data = (self.divider >> 8) as u8;
        self.channel_0.write(data);
    }

    /// should not be called by users
    pub(super) unsafe fn incr(&mut self) {
        self.clock += 1;
    }

    pub fn real_frequency(&self) -> f64 {
        BASE / (self.divider as f64)
    }

    pub fn period(&self) -> f64 {
        1.0 / self.real_frequency()
    }

    /// delay in ticks
    /// clock + delay must not overflow
    /// SAFETY: this method allocates
    pub unsafe fn schedule(&mut self, delay: usize, callback: impl FnOnce() + Send + 'static) {
        if let Some(ref mut callbacks) = self.callbacks {
            callbacks.push(DateCallback(self.clock + delay, Box::new(callback)))
        }
    }

    /// should not be called by users
    pub(super) fn wake(&mut self) {
        if let Some(ref mut callbacks) = self.callbacks {
            loop {
                if callbacks
                    .peek()
                    .map(|DateCallback(date, _)| *date < self.clock)
                    .unwrap_or(false)
                {
                    let DateCallback(_, callback) = unsafe { callbacks.pop().unwrap_unchecked() };
                    callback()
                } else {
                    break;
                }
            }
        }
    }
}

pub type Callback = Box<dyn FnOnce() + Send>;

/// date, callback, ordered by date
/// we reverse because BinaryHeap is a max-heap
struct DateCallback(usize, Callback);

impl PartialEq for DateCallback {
    fn eq(&self, other: &Self) -> bool {
        self.0 == other.0
    }
}

impl Eq for DateCallback {}

impl PartialOrd for DateCallback {
    fn partial_cmp(&self, other: &Self) -> Option<core::cmp::Ordering> {
        self.0
            .partial_cmp(&other.0)
            .map(core::cmp::Ordering::reverse)
    }
}

impl Ord for DateCallback {
    fn cmp(&self, other: &Self) -> core::cmp::Ordering {
        self.0.cmp(&other.0).reverse()
    }
}
