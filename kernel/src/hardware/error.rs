use core::fmt::{self, Display, Formatter};

#[derive(Debug)]
pub enum Error {
    OutOfBounds,
    DeviceNotInitialised(u8),
}

impl Display for Error {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        match self {
            Self::OutOfBounds => write!(f, "Access out of bounds of device."),
            Self::DeviceNotInitialised(drive) => write!(f, "Device {drive} not initialized."),
        }
    }
}
