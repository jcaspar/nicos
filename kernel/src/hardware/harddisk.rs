//! https://wiki.osdev.org/ATA_PIO_Mode
//! https://wiki.osdev.org/IDE

use x86_64::instructions::port::{Port, PortRead};

use core::{
    arch::asm,
    ptr::{read_volatile, write_volatile},
};

use super::{io_wait, Device, DeviceError, IDE};
use crate::error::ErrorKind;
use crate::prelude::*;

// Status
const ATA_SR_BSY: u8 = 0x80; // Busy
const ATA_SR_DRDY: u8 = 0x40; // Drive ready
const ATA_SR_DF: u8 = 0x20; // Drive write fault
const ATA_SR_DSC: u8 = 0x10; // Drive seek complete
const ATA_SR_DRQ: u8 = 0x08; // Data request ready
const ATA_SR_CORR: u8 = 0x04; // Corrected data
const ATA_SR_IDX: u8 = 0x02; // Index
const ATA_SR_ERR: u8 = 0x01; // Error

// Errors
const ATA_ER_BBK: u8 = 0x80; // Bad block
const ATA_ER_UNC: u8 = 0x40; // Uncorrectable data
const ATA_ER_MC: u8 = 0x20; // Media changed
const ATA_ER_IDNF: u8 = 0x10; // ID mark not found
const ATA_ER_MCR: u8 = 0x08; // Media change request
const ATA_ER_ABRT: u8 = 0x04; // Command aborted
const ATA_ER_TK0NF: u8 = 0x02; // Track 0 not found
const ATA_ER_AMNF: u8 = 0x01; // No address mark

// Commands
const ATA_CMD_READ_PIO: u8 = 0x20;
const ATA_CMD_READ_PIO_EXT: u8 = 0x24;
const ATA_CMD_READ_DMA: u8 = 0xC8;
const ATA_CMD_READ_DMA_EXT: u8 = 0x25;
const ATA_CMD_WRITE_PIO: u8 = 0x30;
const ATA_CMD_WRITE_PIO_EXT: u8 = 0x34;
const ATA_CMD_WRITE_DMA: u8 = 0xCA;
const ATA_CMD_WRITE_DMA_EXT: u8 = 0x35;
const ATA_CMD_CACHE_FLUSH: u8 = 0xE7;
const ATA_CMD_CACHE_FLUSH_EXT: u8 = 0xEA;
const ATA_CMD_PACKET: u8 = 0xA0;
const ATA_CMD_IDENTIFY_PACKET: u8 = 0xA1;
const ATA_CMD_IDENTIFY: u8 = 0xEC;

const ATA_IDENT_DEVICETYPE: u8 = 0;
const ATA_IDENT_CYLINDERS: u8 = 2;
const ATA_IDENT_HEADS: u8 = 6;
const ATA_IDENT_SECTORS: u8 = 12;
const ATA_IDENT_SERIAL: u8 = 20;
const ATA_IDENT_MODEL: u8 = 54;
const ATA_IDENT_CAPABILITIES: u8 = 98;
const ATA_IDENT_FIELDVALID: u8 = 106;
const ATA_IDENT_MAX_LBA: u8 = 120;
const ATA_IDENT_COMMANDSETS: u8 = 164;
const ATA_IDENT_MAX_LBA_EXT: u8 = 200;

const IDE_ATA: u8 = 0x00;
const IDE_ATAPI: u8 = 0x01;

const ATA_MASTER: u8 = 0x00;
const ATA_SLAVE: u8 = 0x01;

const ATA_REG_DATA: u8 = 0x00;
const ATA_REG_ERROR: u8 = 0x01;
const ATA_REG_FEATURES: u8 = 0x01;
const ATA_REG_SECCOUNT0: u8 = 0x02;
const ATA_REG_LBA0: u8 = 0x03;
const ATA_REG_LBA1: u8 = 0x04;
const ATA_REG_LBA2: u8 = 0x05;
const ATA_REG_HDDEVSEL: u8 = 0x06;
const ATA_REG_COMMAND: u8 = 0x07;
const ATA_REG_STATUS: u8 = 0x07;
const ATA_REG_SECCOUNT1: u8 = 0x08;
const ATA_REG_LBA3: u8 = 0x09;
const ATA_REG_LBA4: u8 = 0x0A;
const ATA_REG_LBA5: u8 = 0x0B;
const ATA_REG_CONTROL: u8 = 0x0C;
const ATA_REG_ALTSTATUS: u8 = 0x0C;
const ATA_REG_DEVADDRESS: u8 = 0x0D;

// Channels:
const ATA_PRIMARY: u8 = 0x00;
const ATA_SECONDARY: u8 = 0x01;

// Directions:
const ATA_READ: u8 = 0x00;
const ATA_WRITE: u8 = 0x01;

// Harddisk Specs:
const SECTOR_SIZE: usize = 512;

pub struct IDEDriver {
    registers: [IDEChannel; 2],
    buffer: [u8; 2048],
    harddisks: [Option<HardDisk>; 4],
    /// VolatileStack
    irq_invoked: bool,
}

struct IDEChannel {
    base: u16,
    control: u16,
    bmide: u16, // bus master IDE
    nien: u8,   // nIEN : no interrupt
}

/// On primary channel (0), master drive (0), ATA (0)
struct HardDisk {
    channel: u8,
    drive: u8,
    disk_type: u8,
    signature: u16,
    capabilities: u16,
    command_sets: u32,
    size: u32,
    model: [u8; 40],
}

impl IDEDriver {
    pub const fn new(bar0: u16, bar1: u16, bar2: u16, bar3: u16, bar4: u16) -> Self {
        Self {
            registers: [
                IDEChannel {
                    base: bar0,
                    control: bar1,
                    bmide: bar4,
                    nien: 0x00,
                },
                IDEChannel {
                    base: bar2,
                    control: bar3,
                    bmide: bar4 as u16 + 8,
                    nien: 0x00,
                },
            ],
            buffer: [0; 2048],
            harddisks: [None, None, None, None],
            irq_invoked: false,
        }
    }

    pub unsafe fn init(&mut self) {
        for i in 0..2 {
            if self.read_reg::<u8>(i, ATA_REG_STATUS) == 0xFF {
                // No device
                continue;
            }

            for j in 0..2 {
                self.write_to_reg(i, ATA_REG_HDDEVSEL, 0xA0 | (j << 4));
                self.poll(i);

                self.write_to_reg(i, ATA_REG_CONTROL, 2);

                self.write_to_reg(i, ATA_REG_COMMAND, ATA_CMD_IDENTIFY);

                if self.read_reg::<u8>(i, ATA_REG_STATUS) == 0 {
                    // No device
                    continue;
                }

                let mut status: u8;
                let err = loop {
                    status = self.read_reg(i, ATA_REG_STATUS);
                    if status & ATA_SR_ERR != 0 {
                        break true;
                    }
                    if !(status & ATA_SR_BSY != 0) && (status & ATA_SR_DRQ != 0) {
                        break false;
                    }
                };

                let disk_type = if err {
                    let cl: u8 = self.read_reg(i, ATA_REG_LBA1);
                    let ch: u8 = self.read_reg(i, ATA_REG_LBA2);

                    if (cl == 0x14 && ch == 0xEB) || (cl == 0x69 && ch == 0x96) {
                    } else {
                        serial_println!("Unknown device {i} {j}, continuing");
                        continue;
                    }

                    self.write_to_reg(i, ATA_REG_COMMAND, ATA_CMD_IDENTIFY_PACKET);
                    IDE_ATAPI
                } else {
                    IDE_ATA
                };

                self.read_buffer(i, ATA_REG_DATA);

                let signature = unsafe {
                    *self
                        .buffer
                        .as_ptr()
                        .add(ATA_IDENT_DEVICETYPE as usize)
                        .cast::<u16>()
                };
                let capabilities = unsafe {
                    *self
                        .buffer
                        .as_ptr()
                        .add(ATA_IDENT_CAPABILITIES as usize)
                        .cast::<u16>()
                };
                let command_sets = unsafe {
                    *self
                        .buffer
                        .as_ptr()
                        .add(ATA_IDENT_COMMANDSETS as usize)
                        .cast::<u32>()
                };
                let size = if (command_sets & (1 << 26)) != 0 {
                    unsafe {
                        *self
                            .buffer
                            .as_ptr()
                            .add(ATA_IDENT_MAX_LBA_EXT as usize)
                            .cast::<u32>()
                    }
                } else {
                    unsafe {
                        *self
                            .buffer
                            .as_ptr()
                            .add(ATA_IDENT_MAX_LBA as usize)
                            .cast::<u32>()
                    }
                };

                let mut model = [0; 40];
                for k in 0..20 {
                    model[2 * k] = self.buffer[ATA_IDENT_MODEL as usize + 2 * k + 1];
                    model[2 * k + 1] = self.buffer[ATA_IDENT_MODEL as usize + 2 * k];
                }

                let harddisk = HardDisk {
                    channel: i,
                    drive: j,
                    disk_type,
                    signature,
                    capabilities,
                    command_sets,
                    size,
                    model,
                };
                self.harddisks[(i * 2 + j) as usize] = Some(harddisk);

                serial_println!(
                    "Found {} hard disk {} of size {}MB",
                    ["ATA", "ATAPI"][disk_type as usize],
                    unsafe { core::str::from_utf8_unchecked(&model).trim() },
                    size / 1024 / 2,
                )
            }
        }
    }

    unsafe fn read_reg<T: PortRead>(&mut self, channel: u8, reg: u8) -> T {
        let reg = reg as u16;
        if let 0x08..=0x0B = reg {
            self.write_to_reg(
                channel,
                ATA_REG_CONTROL,
                0x80 | self.registers[channel as usize].nien,
            );
        }
        let res = match reg {
            ..=0x07 => Port::new(self.registers[channel as usize].base + reg - 0x00).read(),
            0x08..=0x0B => Port::new(self.registers[channel as usize].base + reg - 0x06).read(),
            0x0C..=0x0D => Port::new(self.registers[channel as usize].control + reg - 0x0A).read(),
            _ => panic!("Read from unknown hard drive register {}", reg),
        };
        if let 0x08..=0x0B = reg {
            self.write_to_reg(
                channel,
                ATA_REG_CONTROL,
                self.registers[channel as usize].nien,
            );
        }
        io_wait();
        res
    }

    unsafe fn write_to_reg(&mut self, channel: u8, reg: u8, data: u8) {
        let reg = reg as u16;
        if let 0x08..=0x0B = reg {
            self.write_to_reg(
                channel,
                ATA_REG_CONTROL,
                0x80 | self.registers[channel as usize].nien,
            );
        }
        match reg {
            ..=0x07 => Port::new(self.registers[channel as usize].base + reg - 0x00).write(data),
            0x08..=0x0B => {
                Port::new(self.registers[channel as usize].base + reg - 0x06).write(data)
            }
            0x0C..=0x0D => {
                Port::new(self.registers[channel as usize].control + reg - 0x0C).write(data)
            }
            _ => panic!("Write to unknown hard drive register {}", reg),
        }
        if let 0x08..=0x0B = reg {
            self.write_to_reg(
                channel,
                ATA_REG_CONTROL,
                self.registers[channel as usize].nien,
            );
        }
        io_wait();
    }

    fn read_buffer(&mut self, channel: u8, reg: u8) {
        let ptr = self.buffer.as_mut_ptr().cast::<u32>();
        let len = self.buffer.len();
        for i in 0..len >> 2 {
            unsafe { *ptr.add(i) = self.read_reg(channel, reg) };
        }
    }

    fn poll(&mut self, channel: u8) {
        for _ in 0..15 {
            unsafe { self.read_reg::<u8>(channel, ATA_REG_ALTSTATUS) }; // 30ns
        }

        loop {
            if unsafe { self.read_reg::<u8>(channel, ATA_REG_STATUS) } & ATA_SR_BSY == 0 {
                break;
            }
        }
    }

    pub fn ata_access(
        &mut self,
        write: bool,
        drive: u8,
        lba_address: u32,
        numsects: u8,
        mut buffer: *mut [u8; SECTOR_SIZE],
    ) {
        let Some(harddisk) = &self.harddisks[drive as usize] else {
	    panic!("Access to inexistant harddisk {drive}")
	};
        let channel = harddisk.channel;
        let slavebit = harddisk.drive;
        let bus = self.registers[channel as usize].base;
        let words: u32 = 256;

        // disable IRQs
        unsafe { write_volatile(&mut self.irq_invoked, false) };
        self.registers[channel as usize].nien =
            unsafe { read_volatile(&self.irq_invoked) as u8 } + 2;
        unsafe {
            self.write_to_reg(
                channel,
                ATA_REG_CONTROL,
                self.registers[channel as usize].nien,
            )
        };

        // select LBA24 mode
        let lba_io = [
            ((lba_address & 0x0000FF) >> 0) as u8,
            ((lba_address & 0x00FF00) >> 8) as u8,
            ((lba_address & 0xFF0000) >> 16) as u8,
            0,
            0,
            0,
        ];
        let head = ((lba_address/*& 0xF000000*/) >> 24) as u8;

        // wait
        while unsafe { self.read_reg::<u8>(channel, ATA_REG_STATUS) } & ATA_SR_BSY != 0 {}

        unsafe {
            self.write_to_reg(channel, ATA_REG_HDDEVSEL, 0xE0 | (slavebit << 4) | head);
            self.write_to_reg(channel, ATA_REG_SECCOUNT0, numsects);
            self.write_to_reg(channel, ATA_REG_LBA0, lba_io[0]);
            self.write_to_reg(channel, ATA_REG_LBA1, lba_io[1]);
            self.write_to_reg(channel, ATA_REG_LBA2, lba_io[2]);
        }

        let cmd = if write {
            ATA_CMD_WRITE_PIO
        } else {
            ATA_CMD_READ_PIO
        };

        unsafe {
            self.write_to_reg(channel, ATA_REG_COMMAND, cmd);
        }

        if write {
            for _ in 0..numsects {
                self.poll(channel);
                for _ in 0..words {
                    unsafe {
                        asm!(
                            "outsw",
                            in("dx") bus,
                            inout("rsi") buffer
                        );
                    }
                    io_wait();
                }
                unsafe {
                    self.write_to_reg(channel, ATA_REG_COMMAND, ATA_CMD_CACHE_FLUSH);
                }
                self.poll(channel);
            }
        } else {
            for _ in 0..numsects {
                self.poll(channel);
                unsafe {
                    asm!(
                        "rep insw",
                        in("ecx") words,
                        in("dx") bus,
                        inout("rdi") buffer,
                    );
                }
            }
        }
    }
}

#[derive(Debug)]
pub struct HardDiskDriver {
    drive: u8,
    sector: u32,
    size: u32,
    buffer: [u8; SECTOR_SIZE],
    init: bool,
    dirty: bool,
}

impl HardDiskDriver {
    pub const fn new(drive: u8) -> Self {
        Self {
            drive,
            sector: 0,
            size: 0,
            buffer: [0; SECTOR_SIZE],
            init: false,
            dirty: false,
        }
    }

    /// SAFETY : IDE has already been initialized
    pub unsafe fn init(&mut self) {
        if let Some(Some(harddisk)) = IDE.lock().harddisks.get(self.drive as usize) {
            self.init = true;
            self.size = harddisk.size;
        } else {
            panic!("Harddisk {} is not present", { self.drive });
        }
        IDE.lock()
            .ata_access(false, self.drive, self.sector, 1, &mut self.buffer);
    }

    pub fn size(&self) -> u32 {
        self.size
    }

    pub fn flush_cache(&mut self) {
        if self.dirty {
            IDE.lock()
                .ata_access(true, self.drive, self.sector, 1, &mut self.buffer);
            self.dirty = false;
        }
    }

    fn go_to(&mut self, address: usize) -> Result<()> {
        if !self.init {
            return err!(DeviceError::DeviceNotInitialised(self.drive));
        }

        let new_sector = address / SECTOR_SIZE;
        if new_sector >= self.size as usize {
            return err!(DeviceError::OutOfBounds);
        }

        if new_sector == self.sector as usize {
            return Ok(());
        }
        self.flush_cache();
        self.sector = new_sector as _;
        if self.sector == self.size {
            return err!(DeviceError::OutOfBounds);
        }
        IDE.lock()
            .ata_access(false, self.drive, self.sector, 1, &mut self.buffer);
        Ok(())
    }
}

impl Device for HardDiskDriver {
    fn read_at(&mut self, head: usize) -> Result<Option<u8>> {
        if !self.init {
            return err!(DeviceError::DeviceNotInitialised(self.drive));
        }

        if let Err(error) = self.go_to(head) {
            if let ErrorKind::Device(DeviceError::OutOfBounds) = *error.kind {
                return Ok(None);
            } else {
                return Err(error);
            }
        }

        let inside_sector = head % SECTOR_SIZE;

        let res = unsafe { self.buffer.as_ptr().add(inside_sector).read_volatile() };
        Ok(Some(res))
    }

    fn write_at(&mut self, head: usize, data: u8) -> Result<()> {
        if !self.init {
            return err!(DeviceError::DeviceNotInitialised(self.drive));
        }

        self.go_to(head)?;
        let inside_sector = head % SECTOR_SIZE;
        self.dirty = true;
        unsafe {
            self.buffer
                .as_mut_ptr()
                .add(inside_sector)
                .write_volatile(data)
        };
        Ok(())
    }

    fn flush(&mut self) -> Result<()> {
        self.flush_cache();
        Ok(())
    }
}
