//! https://wiki.osdev.org/Global_Descriptor_Table and
//! https://wiki.osdev.org/GDT_Tutorial#Filling_the_Table
//! https://wiki.osdev.org/Task_State_Segment
//! Intel Software Developer Manual, Volume 3-A, Chapter 3

use core::ops::Deref;
use x86_64::{
    instructions::{
        segmentation::{Segment, CS, DS, ES, FS, GS, SS},
        tables::load_tss,
    },
    registers::model_specific::{Efer, EferFlags, LStar, Star},
    structures::{
        gdt::{Descriptor, GlobalDescriptorTable, SegmentSelector},
        tss::TaskStateSegment,
    },
    VirtAddr,
};

use super::*;
use crate::{asm::syscall_handler, shared_resource::Once};

/// this is a macro and not a function, as each stack must be different
macro_rules! tss_stack {
    ($x:ident) => {
        static mut $x: [u8; STACK_SIZE as usize] = [0; STACK_SIZE as usize];
    };
}

const STACK_SIZE: u64 = 4096 * 5;
tss_stack!(DOUBLE_FAULT_STACK);
tss_stack!(NMI_STACK);
tss_stack!(DEBUG_STACK);
tss_stack!(MACHINE_CHECK_STACK);

pub static TSS: PanicMutex<TaskStateSegment> = PanicMutex::new(TaskStateSegment::new());
pub static SELECTORS: Once<[SegmentSelector; 5]> = Once::new();
static GDT: Once<GlobalDescriptorTable> = Once::new();

pub fn init() {
    let mut tss = TSS.lock();
    unsafe {
        // double fault exception
        tss.interrupt_stack_table[DOUBLE_FAULT as usize] =
            VirtAddr::from_ptr(&DOUBLE_FAULT_STACK) + STACK_SIZE;
        // non-maskable interrupts
        tss.interrupt_stack_table[NMI as usize] = VirtAddr::from_ptr(&NMI_STACK) + STACK_SIZE;
        // debug interrupts
        tss.interrupt_stack_table[DEBUG as usize] = VirtAddr::from_ptr(&DEBUG_STACK) + STACK_SIZE;
        // machine check exception
        tss.interrupt_stack_table[MACHINE_CHECK as usize] =
            VirtAddr::from_ptr(&MACHINE_CHECK_STACK) + STACK_SIZE;
    }

    let mut gdt = GlobalDescriptorTable::new();
    let kernel_code_selector = gdt.add_entry(Descriptor::kernel_code_segment());
    let kernel_data_selector = gdt.add_entry(Descriptor::kernel_data_segment());
    let user_data_selector = gdt.add_entry(Descriptor::user_data_segment());
    let user_code_selector = gdt.add_entry(Descriptor::user_code_segment());
    let tss_selector = gdt.add_entry(Descriptor::tss_segment(
        // SAFETY: it is clear that the requirement for the borrow to be 'static is wrong,
        // as the tss can (and should) be updated after it has been loaded in the gdt
        unsafe { core::mem::transmute(tss.deref()) },
    ));
    SELECTORS
        .set([
            kernel_code_selector,
            kernel_data_selector,
            user_data_selector,
            user_code_selector,
            tss_selector,
        ])
        .unwrap();

    GDT.set(gdt).unwrap();
    GDT.get().unwrap().load();

    unsafe {
        CS::set_reg(kernel_code_selector);
        DS::set_reg(kernel_data_selector);
        ES::set_reg(kernel_data_selector);
        FS::set_reg(kernel_data_selector);
        GS::set_reg(kernel_data_selector);
        SS::set_reg(kernel_data_selector);
        load_tss(tss_selector);
    }
    // syscall/sysret
    unsafe {
        Efer::update(|flags| *flags = *flags | EferFlags::SYSTEM_CALL_EXTENSIONS);
        Star::write(
            user_code_selector,
            user_data_selector,
            kernel_code_selector,
            kernel_data_selector,
        )
        .unwrap();
    }
    LStar::write(VirtAddr::new(syscall_handler as u64));
}
