use x86_64::instructions::port::Port;

use core::{fmt, num::NonZeroU16};

/// Internal invariants :
/// * base should be the io address of a COM port, and there
///   should not be any write to these ports outside of this struct
/// * DLAB is set to 0
/// * the port should be the ones given in Self::new()
pub struct Serial {
    base: u16,
    data: Port<u8>,
    interrupt_enable: Port<u8>,
    interrupt_identification: Port<u8>,
    line_control: Port<u8>,
    modem_control: Port<u8>,
    line_status: Port<u8>,
    modem_status: Port<u8>,
    scratch: Port<u8>,
}

impl Serial {
    /// SAFETY: base should be the io address of a COM port,
    /// and there should only be one Serial per port
    pub const unsafe fn new(base: u16) -> Self {
        Self {
            base,
            data: Port::new(base),
            interrupt_enable: Port::new(base + 1),
            interrupt_identification: Port::new(base + 2),
            line_control: Port::new(base + 3),
            modem_control: Port::new(base + 4),
            line_status: Port::new(base + 5),
            modem_status: Port::new(base + 6),
            scratch: Port::new(base + 7),
        }
    }

    /// SAFETY: interrupts should be disabled
    pub unsafe fn init(&mut self) {
        // safe because the interrupts are disabled
        self.set_baud(
            // safe because 1 is nonzero
            unsafe { NonZeroU16::new_unchecked(1) },
        );

        let mut line_control = 0;
        line_control |= 0b10; // characters need 7 bits
        line_control |= 0b0 << 2; // one stop bit
        line_control |= 0b000 << 3; // no parity bit
        self.line_control.write(line_control);

        let mut interrupt_enable = 0;
        interrupt_enable |= 0; // deactivate data available interrupts
        interrupt_enable |= 1 << 1; // activate transmit empty interrupts
        interrupt_enable |= 0 << 2; // deactivate break/error interrupts
        interrupt_enable |= 0 << 3; // deactivate status change interrupts
        self.interrupt_enable.write(interrupt_enable);

        self.modem_control.write(0); // unused
    }

    /// SAFETY: interrupts should be disabled
    unsafe fn set_baud(&mut self, value: NonZeroU16) {
        let line_control = self.line_control.read();
        // set DLAB to 1
        self.line_control.write(line_control | 0b10000000);
        // set baud
        self.data.write((value.get() & 0xFF) as u8);
        self.interrupt_enable.write((value.get() >> 8) as u8);
        // reset DLAB
        self.line_control.write(line_control);
    }

    pub fn can_transmit(&mut self) -> bool {
        ((unsafe { self.line_status.read() } >> 5) & 1) != 0
    }

    pub fn can_read(&mut self) -> bool {
        (unsafe { self.line_status.read() } & 1) != 0
    }

    pub fn send(&mut self, bytes: &[u8]) {
        for b in bytes {
            while !self.can_transmit() {}
            unsafe { self.data.write(*b) }
        }
    }

    pub fn recv(&mut self, bytes: &mut [u8]) {
        for b in bytes {
            while !self.can_read() {}
            unsafe {
                *b = self.data.read();
            }
        }
    }
}

impl fmt::Write for Serial {
    fn write_str(&mut self, s: &str) -> fmt::Result {
        Ok(self.send(s.as_bytes()))
    }
}
