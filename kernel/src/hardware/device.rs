use alloc::vec::Vec;

use crate::{
    error::Result,
    serde::{Deserialize, Serialize},
    string::AsciiString,
};

pub trait Device: Sized {
    fn read_at(&mut self, n: usize) -> Result<Option<u8>>;
    fn reader_at<'a>(&'a mut self, n: usize) -> Reader<'a, Self> {
        Reader::new(self, n)
    }
    fn flush(&mut self) -> Result<()>;
    fn write_at(&mut self, n: usize, byte: u8) -> Result<()>;
    fn writer_at<'a>(&'a mut self, n: usize) -> Writer<'a, Self> {
        Writer::new(self, n)
    }
}

pub trait Skippable: Sized {
    fn skip_forward(&mut self, offset: usize);
}

pub trait Peekable: Skippable {
    fn go_to(&mut self, head: usize);
    fn skip_backward(&mut self, offset: usize);
}

pub trait Writable: Skippable {
    fn write(&mut self, byte: u8) -> Result<()>;
    fn flush(&mut self) -> Result<()>;
    fn encode<U: Serialize + ?Sized>(&mut self, obj: &U) -> Result<()> {
        obj.serialize(self)
    }
    fn write_all(&mut self, bytes: &[u8]) -> Result<()> {
        for byte in bytes {
            self.write(*byte)?;
        }
        Ok(())
    }
}

pub struct Writer<'a, T: Device> {
    head: usize,
    device: &'a mut T,
}

impl<'a, T: Device> Drop for Writer<'a, T> {
    fn drop(&mut self) {
        drop(self.device.flush());
    }
}

impl<'a, T: Device> Writer<'a, T> {
    fn new(device: &'a mut T, head: usize) -> Self {
        Self { head, device }
    }
}

impl<'a, T: Device> Skippable for Writer<'a, T> {
    fn skip_forward(&mut self, offset: usize) {
        self.head += offset;
    }
}

impl<'a, T: Device> Peekable for Writer<'a, T> {
    fn go_to(&mut self, head: usize) {
        self.head = head;
    }

    fn skip_backward(&mut self, offset: usize) {
        self.head -= offset;
    }
}

impl<'a, T: Device> Writable for Writer<'a, T> {
    fn write(&mut self, byte: u8) -> Result<()> {
        self.device.write_at(self.head, byte)?;
        self.head += 1;
        Ok(())
    }

    fn flush(&mut self) -> Result<()> {
        self.device.flush()
    }
}

pub trait Readable: Skippable {
    fn read(&mut self) -> Result<Option<u8>>;

    fn read_to_end(&mut self, buffer: &mut Vec<u8>) -> Result<()> {
        while let Some(byte) = self.read()? {
            buffer.push(byte);
        }
        Ok(())
    }

    fn read_to_string(&mut self, string: &mut AsciiString) -> Result<()> {
        self.read_to_end(&mut *string)
    }

    fn parse<U: Deserialize>(&mut self) -> Result<U> {
        U::deserialize(self)
    }
}

pub struct Reader<'a, T> {
    head: usize,
    device: &'a mut T,
}

impl<'a, T: Device> Reader<'a, T> {
    fn new(device: &'a mut T, head: usize) -> Self {
        Self { head, device }
    }
}

impl<'a, T: Device> Readable for Reader<'a, T> {
    fn read(&mut self) -> Result<Option<u8>> {
        self.head += 1;
        self.device.read_at(self.head - 1)
    }
}

impl<'a, T: Device> Skippable for Reader<'a, T> {
    fn skip_forward(&mut self, offset: usize) {
        self.head += offset;
    }
}

impl<'a, T: Device> Peekable for Reader<'a, T> {
    fn go_to(&mut self, head: usize) {
        self.head = head;
    }

    fn skip_backward(&mut self, offset: usize) {
        self.head -= offset;
    }
}

impl<'a> Reader<'a, Vec<u8>> {
    pub fn is_saturated(&self) -> bool {
        self.head == self.device.len()
    }
}
