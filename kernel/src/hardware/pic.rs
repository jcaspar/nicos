use core::cell::UnsafeCell;

use x86_64::instructions::{interrupts::without_interrupts, port::Port};

use super::io_wait;

/// IO port for master PIC
const PIC1: u16 = 0x20;
/// IO port for slave PIC
const PIC2: u16 = 0xA0;

/// End of interrupt
const PIC_EOI: u8 = 0x20;
/// Deactivate PIC
const PIC_DEACTIVATE: u8 = 0xFF;

/// ICW4 (not) needed
const ICW1_ICW4: u8 = 0x01;
/// Single (cascade) mode
const ICW1_SINGLE: u8 = 0x02;
/// Call address interval 4 (8)
const ICW1_INTERVAL4: u8 = 0x04;
/// Level triggered (edge) mode
const ICW1_LEVEL: u8 = 0x08;
/// Initialization - required!
const ICW1_INIT: u8 = 0x10;

///8086/88 (MCS-80/85) mode
const ICW4_8086: u8 = 0x01;
/// Auto (normal) EOI
const ICW4_AUTO: u8 = 0x02;
/// Buffered mode/slave
const ICW4_BUF_SLAVE: u8 = 0x08;
/// Buffered mode/master
const ICW4_BUF_MASTER: u8 = 0x0C;
/// Special fully nested (not)
const ICW4_SFNM: u8 = 0x10;

/// OCW3 queries
const PIC_READ_IRR: u8 = 0x0A;
const PIC_READ_ISR: u8 = 0x0B;

#[repr(u8)]
#[derive(Clone, Copy)]
pub enum IRQ {
    SystemClock = 0,
    Keyboard = 1,
    COM2 = 3,
    COM1 = 4,
    RealTimeClock = 8,
    PS2 = 12,
}

/// Internal invariants :
/// * the offsets should be used by another IRQ in the IDT
/// * the port should be the ones given in Self::new()
#[derive(Debug)]
pub struct PICs {
    master_offset: u8,
    slave_offset: u8,
    pic1_cmd: UnsafeCell<Port<u8>>,
    pic1_data: Port<u8>,
    pic2_cmd: UnsafeCell<Port<u8>>,
    pic2_data: Port<u8>,
}

impl PICs {
    /// offset to which the IRQ's should be remaped
    /// SAFETY: the offsets should not be used by another IRQ in the IDT
    pub const unsafe fn new(master_offset: u8, slave_offset: u8) -> Self {
        Self {
            master_offset,
            slave_offset,
            pic1_cmd: UnsafeCell::new(Port::new(PIC1)),
            pic1_data: Port::new(PIC1 + 1),
            pic2_cmd: UnsafeCell::new(Port::new(PIC2)),
            pic2_data: Port::new(PIC2 + 1),
        }
    }

    fn write1(&self, data: u8) {
        // SAFETY (for `&mut *...`): `write` is an atomic operation, so the lifetime of the
        // exclusive reborrow lasts exactly as long as a single operation, which can't be
        // interleaved, and thus aliased.
        // SAFETY (for `.write(...)`): the port we are writing to is valid, because Jean said so.
        unsafe { (&mut *self.pic1_cmd.get()).write(data) }
    }

    fn write2(&self, data: u8) {
        // SAFETY: same as for `write1`.
        unsafe { (&mut *self.pic2_cmd.get()).write(data) }
    }

    fn read1(&self) -> u8 {
        // SAFETY: same as for `write1`
        unsafe { (&mut *self.pic1_cmd.get()).read() }
    }

    fn read2(&self) -> u8 {
        // SAFETY: same as for `write1`
        unsafe { (&mut *self.pic2_cmd.get()).read() }
    }

    /// SAFETY: interrupts should be disabled
    pub unsafe fn init(&mut self) {
        let mask1 = self.pic1_data.read();
        let mask2 = self.pic2_data.read();

        self.write1(ICW1_INIT | ICW1_ICW4);
        io_wait();
        self.write2(ICW1_INIT | ICW1_ICW4);
        io_wait();

        self.pic1_data.write(self.master_offset);
        io_wait();
        self.pic2_data.write(self.slave_offset);
        io_wait();

        // The master should receive the slave's interrupts at port 4
        self.pic1_data.write(4);
        io_wait();
        // The slave should send its interrupts on the port 2
        self.pic2_data.write(2);
        io_wait();

        self.pic1_data.write(ICW4_8086);
        io_wait();
        self.pic2_data.write(ICW4_8086);
        io_wait();

        self.pic1_data.write(mask1);
        self.pic2_data.write(mask2);
    }

    pub fn end_of_interrupt(&self, irq: IRQ) {
        if irq as u8 >= 8 {
            self.write2(PIC_EOI);
        }
        self.write1(PIC_EOI);
    }

    /// Return the combine value of both PIC's for the asked register
    /// SAFETY: should not be used directly
    unsafe fn pic_get_irq_reg(&self, ocw3: u8) -> u16 {
        without_interrupts(|| {
            self.write1(ocw3);
            self.write2(ocw3);

            ((self.read2() as u16) << 8) | self.read1() as u16
        })
    }

    pub fn pic_get_irr(&self) -> u16 {
        unsafe { self.pic_get_irq_reg(PIC_READ_IRR) }
    }

    pub fn pic_get_isr(&self) -> u16 {
        unsafe { self.pic_get_irq_reg(PIC_READ_ISR) }
    }
}
