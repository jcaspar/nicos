use x86_64::{
    registers::control::Cr2,
    structures::idt::{InterruptDescriptorTable, InterruptStackFrame, PageFaultErrorCode},
};

use crate::{
    asm::{system_clock_interrupt_handler, Registers},
    fs::vfs::KEYCODES,
    process::{scheduler_do_something, SCHEDULING},
    shared_resource::Once,
};

use super::*;

macro_rules! exception_handler {
    ($idt:expr => $($id:ident,$id_handler:ident $(, $idx:expr)?);*) => {
        $(
            pub extern "x86-interrupt" fn $id_handler(stack_frame: InterruptStackFrame) {
                panic!(concat!("EXCEPTION: ", stringify!($id), "\n{:#?}"), stack_frame);
            }
            let _entry_options = $idt.$id.set_handler_fn($id_handler);
            $(
                unsafe {
                    _entry_options.set_stack_index($idx);
                }
            )?
        )*
    }
}

pub fn init() {
    let mut idt = InterruptDescriptorTable::new();
    exception_handler! {idt =>
        divide_error, divide_error_handler;
        non_maskable_interrupt, non_maskable_interrupt_handler, NMI;
        breakpoint, breakpoint_handler;
        overflow, overflow_handler;
        bound_range_exceeded, bound_range_exceeded_handler;
        invalid_opcode, invalid_opcode_handler;
        device_not_available, device_not_available_handler
    }
    unsafe {
        idt.debug
            .set_handler_fn(debug_handler)
            .set_stack_index(DEBUG);
        idt.double_fault
            .set_handler_fn(double_fault_handler)
            .set_stack_index(DOUBLE_FAULT);
    }
    idt.page_fault.set_handler_fn(page_fault_handler);
    idt[PIC_OFFSET + IRQ::SystemClock as usize].set_handler_fn(unsafe {
        core::mem::transmute(
            system_clock_interrupt_handler as unsafe extern "x86-interrupt" fn(InterruptStackFrame),
        )
    });
    idt[PIC_OFFSET + IRQ::Keyboard as usize].set_handler_fn(keyboard_interrupt_handler);
    idt[PIC_OFFSET + IRQ::RealTimeClock as usize].set_handler_fn(real_time_clock_interrupt_handler);
    idt.invalid_tss.set_handler_fn(invalid_tss_handler);
    idt.segment_not_present
        .set_handler_fn(segment_not_present_handler);
    idt.stack_segment_fault
        .set_handler_fn(stack_segment_fault_handler);
    idt.general_protection_fault
        .set_handler_fn(general_protection_fault_handler);
    IDT.set(idt).unwrap();
    IDT.get().unwrap().load();
}

static IDT: Once<InterruptDescriptorTable> = Once::new();

pub extern "x86-interrupt" fn debug_handler(_: InterruptStackFrame) {}

pub extern "x86-interrupt" fn double_fault_handler(
    _stack_frame: InterruptStackFrame,
    error_code: u64,
) -> ! {
    panic!("ERROR: double fault on error {}", error_code);
}

#[no_mangle]
pub extern "C" fn real_system_clock_interrupt_handler(regs: &mut Registers) {
    let mut pit = PIT.lock();
    unsafe {
        pit.incr();
    }
    drop(pit);
    unsafe {
        if SCHEDULING {
            scheduler_do_something(regs);
        }
    }

    // pit.wake();

    PICS.get().unwrap().end_of_interrupt(IRQ::SystemClock);
}

extern "x86-interrupt" fn keyboard_interrupt_handler(_stack_frame: InterruptStackFrame) {
    let key_event = unsafe { KEYBOARD.lock().read() };
    if let Some(key_event) = key_event {
        KEYCODES.push(key_event);
    }
    PICS.get().unwrap().end_of_interrupt(IRQ::Keyboard);
}

extern "x86-interrupt" fn real_time_clock_interrupt_handler(_stack_frame: InterruptStackFrame) {
    if let Some(mut cmos) = CMOS.try_lock() {
        let reason = unsafe { cmos.read_register(0x0C) };

        if (reason >> 4) & 1 == 1 {
            // The loop here should not be executed more than twice, else there is a problem
            let time = loop {
                if let Some(time) = without_interrupts(|| unsafe { cmos.get_current_date() }) {
                    break time;
                }
            };

            cmos.set_time(time);
        }
    }

    PICS.get().unwrap().end_of_interrupt(IRQ::RealTimeClock);
}

extern "x86-interrupt" fn page_fault_handler(
    stack_frame: InterruptStackFrame,
    error_code: PageFaultErrorCode,
) {
    let addr = Cr2::read();
    crate::serial_println!("page fault: {:?}", error_code);
    panic!(
        "page fault while accessing {:?} : {:#?}, error: {:?}",
        addr,
        stack_frame,
        error_code.bits()
    )
}

extern "x86-interrupt" fn invalid_tss_handler(_stack_frame: InterruptStackFrame, error_code: u64) {
    crate::serial_println!("invalid tss: {:?}", error_code);
    panic!("invalid tss")
}

extern "x86-interrupt" fn segment_not_present_handler(
    _stack_frame: InterruptStackFrame,
    error_code: u64,
) {
    crate::serial_println!("segment not present: {:?}", error_code);
    panic!("segment not present")
}

extern "x86-interrupt" fn stack_segment_fault_handler(
    _stack_frame: InterruptStackFrame,
    error_code: u64,
) {
    crate::serial_println!("stack segment fault: {:?}", error_code);
    panic!("stack segment fault")
}

extern "x86-interrupt" fn general_protection_fault_handler(
    stack_frame: InterruptStackFrame,
    error_code: u64,
) {
    crate::serial_println!(
        "general protection fault: {:#?}, error: {:?}",
        stack_frame,
        error_code
    );
    panic!("general protection fault")
}
