use alloc::{fmt, vec::Vec};
use core::{
    fmt::{Debug, Display, Formatter},
    ops::{Deref, DerefMut},
};

pub struct AsciiString(Vec<u8>);

impl AsciiString {
    pub fn new() -> Self {
        Self(Vec::new())
    }

    pub fn push(&mut self, chr: u8) {
        self.0.push(chr)
    }

    pub fn len(&self) -> usize {
        self.0.len()
    }
}

impl Deref for AsciiString {
    type Target = Vec<u8>;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl DerefMut for AsciiString {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}

impl Debug for AsciiString {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        write!(f, "\"{self}\"")
    }
}

impl Display for AsciiString {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        for byte in &self.0 {
            write!(f, "{}", *byte as char)?;
        }
        Ok(())
    }
}

impl From<&[u8]> for AsciiString {
    fn from(bytes: &[u8]) -> Self {
        Self(bytes.into())
    }
}

pub fn to_lower(byte: u8) -> u8 {
    (byte as char).to_lowercase().next().unwrap() as u8
}
