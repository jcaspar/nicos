use crate::{fs::FsError, hardware::DeviceError, process::ProcError, shared_resource::SRError};
use alloc::boxed::Box;
use core::{
    fmt::{self, Display, Formatter},
    result,
};

pub type Result<T> = result::Result<T, Error>;

macro_rules! err {
    ($error:expr) => {
        Err($crate::error::error!($error))
    };
}

macro_rules! error {
    ($error:expr) => {
        $crate::error::Error::new($error, file!(), line!(), column!())
    };
}

macro_rules! here {
    () => {
        || $crate::error::Here::new(file!(), line!(), column!())
    };
}

macro_rules! resource {
    () => {
        || {
            $crate::error::Error::here_with(
                $crate::shared_resource::SRError,
                $crate::error::here!()(),
            )
        }
    };
}

// This trick is to namespace the macros in the `error` module.
#[allow(unused_imports)]
pub(crate) use {err, error, here, resource};

macro_rules! make_err {
    ($name:ident {$(
	$desc:literal => $disc:ident($payload:ty)
    ),* $(,)?}) => {
	#[derive(Debug)]
	pub enum $name {$(
	    $disc($payload)
	),*}

	impl Display for $name {
	    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
		match self {$(
		    Self::$disc(error) => write!(f, "{}: {error}", $desc)
		),*}
	    }
	}

	$(impl From<$payload> for $name {
	    fn from(error: $payload) -> Self {
		Self::$disc(error)
	    }
	})*
    };
}

pub struct Here {
    file: &'static str,
    line: u32,
    column: u32,
}

impl Here {
    pub fn new(file: &'static str, line: u32, column: u32) -> Self {
        Self { file, line, column }
    }
}

#[derive(Debug)]
pub struct Error {
    pub kind: Box<ErrorKind>,
    pub file: &'static str,
    pub line: u32,
    pub column: u32,
}

impl Error {
    pub fn new(error: impl Into<ErrorKind>, file: &'static str, line: u32, column: u32) -> Self {
        Self {
            kind: Box::new(error.into()),
            file,
            line,
            column,
        }
    }

    pub fn here_with(error: impl Into<ErrorKind>, here: Here) -> Self {
        Self::new(error, here.file, here.line, here.column)
    }
}

impl Display for Error {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "Error ({}:{}:{}): {}",
            self.file, self.line, self.column, self.kind
        )
    }
}

make_err! { ErrorKind {
    "filesyste" => Filesystem(FsError),
    "device" => Device(DeviceError),
    "process" => Process(ProcError),
    "shared resource" => SharedResource(SRError),
}}
