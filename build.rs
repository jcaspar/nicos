use bootloader::{BiosBoot, UefiBoot};
use std::env::var_os;
use std::path::PathBuf;

fn main() {
    println!("out_dir");
    let out_dir = PathBuf::from(var_os("OUT_DIR").expect("OUT_DIR not found"));
    println!("kernel");
    let kernel = PathBuf::from(var_os("CARGO_BIN_FILE_KERNEL_kernel").unwrap());
    println!("done");
    let uefi_path = out_dir.join("nicos-uefi.img");
    UefiBoot::new(&kernel)
        .create_disk_image(&uefi_path)
        .unwrap();
    let bios_path = out_dir.join("nicos-bios.img");
    BiosBoot::new(&kernel)
        .create_disk_image(&bios_path)
        .unwrap();

    println!("cargo:rustc-env=UEFI_PATH={}", uefi_path.display());
    println!("cargo:rustc-env=BIOS_PATH={}", bios_path.display());

    println!("test kernel");
    let test_kernel = PathBuf::from(var_os("CARGO_BIN_FILE_KERNEL_kernel_test").unwrap());
    println!("done");
    let test_uefi_path = out_dir.join("test-nicos-uefi.img");
    UefiBoot::new(&test_kernel)
        .create_disk_image(&test_uefi_path)
        .unwrap();
    let test_bios_path = out_dir.join("test-nicos-bios.img");
    BiosBoot::new(&test_kernel)
        .create_disk_image(&test_bios_path)
        .unwrap();

    println!(
        "cargo:rustc-env=TEST_UEFI_PATH={}",
        test_uefi_path.display()
    );
    println!(
        "cargo:rustc-env=TEST_BIOS_PATH={}",
        test_bios_path.display()
    );
}
